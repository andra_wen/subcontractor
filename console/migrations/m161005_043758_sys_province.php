<?php

use yii\db\Migration;
use yii\app;

class m161005_043758_sys_province extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('sys_province',
        [
            'id' => $this->primaryKey(),
            'country_id'=> $this->integer(11)->notNull(),
            'name'=>$this->string(50)->notNull()->unique(),
            'iso_code'=> $this->string(50)->unique(),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-province-country-id',
            'sys_province',
            'country_id',
            'sys_country',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-province-createdby-id',
            'sys_province',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-province-lastmodifby-id',
            'sys_province',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->execute($this->insertIndonesiaProvince());
    }

    private function insertIndonesiaProvince()
    {
        $current_time = time();
        $super_user = 1;

        $this->batchInsert('sys_province',
        [
            'id','name','country_id','createdby', 'createdon', 'lastmodifby','lastmodif', 'status'
        ],
        [
            [1, 'Aceh','95',$super_user, $current_time, $super_user, $current_time,10],
            [2, 'Sumatera Utara','95',$super_user, $current_time, $super_user, $current_time,10],
            [3, 'Sumatera Barat','95',$super_user, $current_time, $super_user, $current_time,10],
            [4, 'Riau','95',$super_user, $current_time, $super_user, $current_time,10],
            [5, 'Jambi','95',$super_user, $current_time, $super_user, $current_time,10],
            [6, 'Sumatera Selatan','95',$super_user, $current_time, $super_user, $current_time,10],
            [7, 'Bengkulu','95',$super_user, $current_time, $super_user, $current_time,10],
            [8, 'Lampung','95',$super_user, $current_time, $super_user, $current_time,10],
            [9, 'Kepulauan Bangka Belitung','95',$super_user, $current_time, $super_user, $current_time,10],
            [10, 'Kepulauan Riau','95',$super_user, $current_time, $super_user, $current_time,10],
            [11, 'DKI Jakarta','95',$super_user, $current_time, $super_user, $current_time,10],
            [12, 'Jawa Barat','95',$super_user, $current_time, $super_user, $current_time,10],
            [13, 'Jawa Tengah','95',$super_user, $current_time, $super_user, $current_time,10],
            [14, 'Daerah Istimewa Yogyakarta','95',$super_user, $current_time, $super_user, $current_time,10],
            [15, 'Jawa Timur','95',$super_user, $current_time, $super_user, $current_time,10],
            [16, 'Banten','95',$super_user, $current_time, $super_user, $current_time,10],
            [17, 'Bali','95',$super_user, $current_time, $super_user, $current_time,10],
            [18, 'Nusa Tenggara Barat','95',$super_user, $current_time, $super_user, $current_time,10],
            [19, 'Nusa Tenggara Timur','95',$super_user, $current_time, $super_user, $current_time,10],
            [20, 'Kalimantan Barat','95',$super_user, $current_time, $super_user, $current_time,10],
            [21, 'Kalimantan Tengah','95',$super_user, $current_time, $super_user, $current_time,10],
            [22, 'Kalimantan Selatan','95',$super_user, $current_time, $super_user, $current_time,10],
            [23, 'Kalimantan Timur','95',$super_user, $current_time, $super_user, $current_time,10],
            [24, 'Sulawesi Utara','95',$super_user, $current_time, $super_user, $current_time,10],
            [25, 'Sulawesi Tengah','95',$super_user, $current_time, $super_user, $current_time,10],
            [26, 'Sulawesi Selatan','95',$super_user, $current_time, $super_user, $current_time,10],
            [27, 'Sulawesi Tenggara','95',$super_user, $current_time, $super_user, $current_time,10],
            [28, 'Gorontalo','95',$super_user, $current_time, $super_user, $current_time,10],
            [29, 'Sulawesi Barat','95',$super_user, $current_time, $super_user, $current_time,10],
            [30, 'Maluku','95',$super_user, $current_time, $super_user, $current_time,10],
            [31, 'Maluku Utara','95',$super_user, $current_time, $super_user, $current_time,10],
            [32, 'Papua','95',$super_user, $current_time, $super_user, $current_time,10],
            [33, 'Papua Barat','95',$super_user, $current_time, $super_user, $current_time,10],
        ]);
    }

    public function down()
    {
        echo "m161005_043758_sys_province cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
