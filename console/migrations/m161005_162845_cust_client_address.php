<?php

use yii\db\Migration;

class m161005_162845_cust_client_address extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('cust_client_address',
        [
            'id' => $this->primaryKey(),
            'cust_client_id'=> $this->integer(11)->notNull(),
            'address_name'=>$this->string(50),
            'address_line1'=> $this->string(200)->notNull(),
            'address_line2'=> $this->string(200),
            'address_country_id'=>$this->integer(11)->notNull(),
            'address_province_id'=>$this->integer(11)->notNull(),
            'address_countytown_id'=>$this->integer(11),
            'address_districts_id'=>$this->integer(11),
            'address_postalzip'=>$this->integer(5),
            'address_remarks'=>$this->string(200),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-address-client-id',
            'cust_client_address',
            'cust_client_id',
            'cust_clients',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-clientaddress-country-id',
            'cust_client_address',
            'address_country_id',
            'sys_country',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-clientaddress-province-id',
            'cust_client_address',
            'address_province_id',
            'sys_province',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-clientaddress-countytown-id',
            'cust_client_address',
            'address_countytown_id',
            'sys_countytown',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-clientaddress-district-id',
            'cust_client_address',
            'address_districts_id',
            'sys_districts',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161005_162845_cust_client_address cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
