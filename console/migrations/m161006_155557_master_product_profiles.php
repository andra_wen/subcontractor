<?php

use yii\db\Migration;

class m161006_155557_master_product_profiles extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('master_product_profiles',
        [
            'id' => $this->primaryKey(),
            'product_id'=>$this->integer(11)->notNull(),
            'barcode'=>$this->string(50)->unique(),
            'uom_id'=>$this->integer(11),
            'alias'=> $this->integer(11),
            'manufacturing_date'=>$this->date(),
            'shelflife'=>$this->integer(),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-profile-product-id',
            'master_product_profiles',
            'product_id',
            'master_products',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-uom-product-id',
            'master_product_profiles',
            'uom_id',
            'sys_uom',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-alias-product-id',
            'master_product_profiles',
            'alias',
            'master_products',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-productprofiles-createdby-id',
            'master_product_profiles',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-productprofiles-lastmodif-id',
            'master_product_profiles',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161006_155557_master_product_profiles cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
