<?php

use yii\db\Migration;

class m161008_040710_master_events extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('master_events',
        [
            'id' => $this->primaryKey(),
            'customer_id'=>$this->integer(11)->notNull(),
            'page_id'=>$this->integer(11),
            'event_name'=>$this->string(100),
            'event_title'=>$this->string(100),
            'event_content'=>$this->string(500),
            'event_pict'=>$this->string(100),
            'event_alt'=>$this->string(100),
            'isactive'=>$this->integer(1)->defaultValue(1),
            'sort_no'=>$this->integer(11)->defaultValue(10),
            'event_date'=>$this->date(),
            'effective_from'=>$this->dateTime(),
            'effective_till'=>$this->dateTime(),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-event-customer-id',
            'master_events',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-event-page-id',
            'master_events',
            'page_id',
            'master_pages',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-event-createdby-id',
            'master_events',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-event-lastmodif-id',
            'master_events',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161008_040710_master_events cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
