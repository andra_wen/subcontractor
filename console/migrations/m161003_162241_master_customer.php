<?php

use yii\db\Migration;

class m161003_162241_master_customer extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('master_customer', [
            'id' => $this->primaryKey(),
            'customer_name'=>$this->string(100)->notNull(),
            'customer_desc'=> $this->string(300),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        /*
        $this->addForeignKey(
            'fk-customer-createdby-id',
            'master_customer',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-customer-lastmodifby-id',
            'master_customer',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
        */

        $this->execute($this->insertGeneralCustomer());
    }

    private function insertGeneralCustomer()
    {
        $current_time = time();
        $super_user = 1;

        $this->batchInsert('master_customer',
        [
            'id','customer_name','createdby','createdon','lastmodifby','lastmodif'
        ],
        [
            ['1','General', $super_user, $current_time, $super_user, $current_time],
        ]);
    }

    public function down()
    {
        echo "m161003_162641_master_customer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
