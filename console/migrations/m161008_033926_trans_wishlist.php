<?php

use yii\db\Migration;

class m161008_033926_trans_wishlist extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('trans_wishlist',
        [
            'id' => $this->primaryKey(),
            'customer_id'=>$this->integer(11)->notNull(),
            'cust_client_id'=>$this->integer(11)->notNull(),
            'wishlist_cat'=>$this->string(100),
            'product_id'=>$this->integer(11)->notNull(),
            'product_varians_id'=> $this->integer(11),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-wishlist-customer-id',
            'trans_wishlist',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-wishlist-client-id',
            'trans_wishlist',
            'cust_client_id',
            'cust_clients',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-wishlist-product-id',
            'trans_wishlist',
            'product_id',
            'master_products',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-wishlist-varians-id',
            'trans_wishlist',
            'product_varians_id',
            'master_product_varians',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-wishlist-createdby-id',
            'trans_wishlist',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-wishlist-lastmodif-id',
            'trans_wishlist',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161008_033926_trans_wishlist cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
