<?php

use yii\db\Migration;

class m161005_144205_cust_address extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('cust_address',
        [
            'id' => $this->primaryKey(),
            'cust_user_id'=> $this->integer(11)->notNull(),
            'address_name'=>$this->string(50),
            'address_line1'=> $this->string(200)->notNull(),
            'address_line2'=> $this->string(200),
            'address_country_id'=>$this->integer(11)->notNull(),
            'address_province_id'=>$this->integer(11)->notNull(),
            'address_countytown_id'=>$this->integer(11),
            'address_districts_id'=>$this->integer(11),
            'address_postalzip'=>$this->integer(5),
            'address_remarks'=>$this->string(200),
            'address_default'=>$this->integer(2)->defaultValue(10),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-address-user-id',
            'cust_address',
            'cust_user_id',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-address-country-id',
            'cust_address',
            'address_country_id',
            'sys_country',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-address-province-id',
            'cust_address',
            'address_province_id',
            'sys_province',
            'id',
            'CASCADE'
        );


        $this->addForeignKey(
            'fk-address-countytown-id',
            'cust_address',
            'address_countytown_id',
            'sys_countytown',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-address-districts-id',
            'cust_address',
            'address_districts_id',
            'sys_districts',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-address-createdby-id',
            'cust_address',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-address-lastmodif-id',
            'cust_address',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161005_144205_cust_address cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
