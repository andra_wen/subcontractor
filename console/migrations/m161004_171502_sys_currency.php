<?php

use yii\db\Migration;

class m161004_171502_sys_currency extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('sys_currency',
        [
            'id' => $this->primaryKey(),
            'code'=>$this->string(10)->notNull(),
            'iso_code'=> $this->string(50)->unique(),
            'name'=>$this->string(100)->unique(),
            'convert_nomimal' =>$this->float(0),
            'convert_rate'=>$this->float(0),
            'intl_formatting'=>$this->integer(1),
            'min_fraction_digits'=>$this->integer(),
            'max_fraction_digits'=>$this->integer(),
            'dec_point'=>$this->string(2),
            'thousands_sep'=>$this->string(2),
            'format_string'=>$this->string(100),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-currency-createdby-id',
            'sys_currency',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-currency-lastmodifby-id',
            'sys_currency',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->execute($this->insertBasicCurrency());
    }

    private function insertBasicCurrency()
    {
        $current_time = time();
        $super_user = 1;

        $this->batchInsert('sys_currency',
            [
                'id','code','iso_code', 'name', 'convert_nomimal', 'convert_rate', 'intl_formatting',
                'min_fraction_digits', 'max_fraction_digits', 'dec_point', 'thousands_sep',
                'format_string', 'createdby', 'createdon', 'lastmodifby', 'lastmodif',
                'status'
            ],
            [
                //id  code   iso    name    c  c  i  m  m  d  t
                ['1','RUB', 'RUB', 'Ruble', 1, 1, 0, 0, 2,'.','','# руб.',$super_user,$current_time,$super_user,$current_time,10],
                ['2','USD', 'USD', 'US Dollar', 1, 62.8353, 1, 1, 0,',','.','$ #',$super_user,$current_time,$super_user,$current_time,10],
                ['3','EUR', 'EUR', 'Euro', 1, 71.3243, 1, 0, 2,'.','','&euro; #',$super_user,$current_time,$super_user,$current_time,10],
                ['4','IDR', 'IDR', 'RP', 1, 1, 1, 0, 0,',','.','',$super_user,$current_time,$super_user,$current_time,10],
            ]);
    }

    public function down()
    {
        echo "m161004_171502_sys_currency cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
