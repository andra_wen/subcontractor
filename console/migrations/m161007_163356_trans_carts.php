<?php

use yii\db\Migration;

class m161007_163356_trans_carts extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('trans_carts',
        [
            'id' => $this->primaryKey(),
            'customer_id'=>$this->integer(11)->notNull(),
            'cust_client_id'=>$this->integer(11)->notNull(),
            'product_id'=>$this->integer(11)->notNull(),
            'product_varians_id'=> $this->integer(11),
            'product_quantity'=> $this->integer(11)->notNull()->defaultValue(1),
            'provider_id'=>$this->integer(11),
            'shippingcost_id'=>$this->integer(11),
            'client_address_id'=>$this->integer(11),
            'cart_remarks'=>$this->string(200),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-cart-customer-id',
            'trans_carts',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-cart-client-id',
            'trans_carts',
            'cust_client_id',
            'cust_clients',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-cart-product-id',
            'trans_carts',
            'product_id',
            'master_products',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-cart-varians-id',
            'trans_carts',
            'product_varians_id',
            'master_product_varians',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-cart-provider-id',
            'trans_carts',
            'provider_id',
            'trans_shippingprovider',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-cart-shippingcost-id',
            'trans_carts',
            'shippingcost_id',
            'trans_shippingcost',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-cart-address-id',
            'trans_carts',
            'client_address_id',
            'cust_client_address',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-cart-createdby-id',
            'trans_carts',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-cart-lastmodif-id',
            'trans_carts',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161007_163356_trans_carts cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
