<?php

use yii\db\Migration;

class m161006_150252_master_products extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('master_products',
        [
            'id' => $this->primaryKey(),
            'customer_id'=>$this->integer(11)->notNull(),
            'product_category'=>$this->integer(11),
            'product_code'=>$this->string(50)->unique(),
            'product_name'=> $this->string(150)->notNull(),
            'product_desc'=>$this->string(200),
            'product_price'=>$this->integer(11),
            'product_weight'=>$this->integer(11),
            'product_height'=>$this->integer(11),
            'product_volume'=>$this->decimal(8.2),
            'product_remarks'=>$this->string(200),
            'custome'=>$this->integer(2)->defaultValue(10),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-product-customer-id',
            'master_products',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-product-category-id',
            'master_products',
            'product_category',
            'master_category',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-product-createdby-id',
            'master_products',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-product-lastmodif-id',
            'master_products',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161006_150252_master_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
