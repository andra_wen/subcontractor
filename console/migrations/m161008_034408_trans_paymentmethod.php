<?php

use yii\db\Migration;

class m161008_034408_trans_paymentmethod extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('trans_paymentmethod',
        [
            'id' => $this->primaryKey(),
            'customer_id'=>$this->integer(11)->notNull(),
            'payment_group'=>$this->string(100),
            'payment_title'=>$this->string(100)->notNull(),
            'payment_name'=> $this->string(100),
            'payment_pict'=> $this->string(100),
            'isactive'=>$this->integer(1)->defaultValue(1),
            'payment_notes'=>$this->string(300),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-paymentmethod-customer-id',
            'trans_paymentmethod',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-paymentmethod-createdby-id',
            'trans_paymentmethod',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-paymentmethod-lastmodif-id',
            'trans_paymentmethod',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->execute($this->insertStandardPaymentMethod());
    }

    private function insertStandardPaymentMethod()
    {
        $current_time = time();
        $super_user = 1;

        $this->batchInsert('trans_paymentmethod',
            [
                'id','customer_id','payment_title','payment_name','createdby', 'createdon', 'lastmodifby','lastmodif', 'status'
            ],
            [
                [1, 1,'Cash','Cash',$super_user, $current_time, $super_user, $current_time,10],
                [2, 1,'Bank Transfer','Bank Transfer',$super_user, $current_time, $super_user, $current_time,10],
            ]);
    }

    public function down()
    {
        echo "m161008_034408_trans_paymentmethod cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
