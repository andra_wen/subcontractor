<?php

use yii\db\Migration;

class m161007_162247_trans_adjustments extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('trans_adjustments',
        [
            'id' => $this->primaryKey(),
            'product_id'=>$this->integer(11)->notNull(),
            'customer_id'=>$this->integer(11)->notNull(),
            'adj_quantity'=>$this->integer(11),
            'adj_reason'=>$this->string(100),
            'adj_remarks'=>$this->string(100),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-adjustment-product-id',
            'trans_adjustments',
            'product_id',
            'master_products',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-adjustment-customer-id',
            'trans_adjustments',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-adjustment-createdby-id',
            'trans_adjustments',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-adjustment-lastmodif-id',
            'trans_adjustments',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161007_162247_trans_adjustments cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
