<?php

use yii\db\Migration;

class m161008_045236_trans_so extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('trans_so',
        [
            'id' => $this->primaryKey(),
            'customer_id'=>$this->integer(11)->notNull(),
            'cust_user_id'=>$this->integer(11)->notNull(),
            'so_no'=>$this->string(50)->unique()->notNull(),
            'paymentmethod_id'=> $this->integer(11),
            'cust_bank_account_id'=> $this->integer(11),
            'shipping_totalcost'=>$this->string(300),
            'product_totalfee'=>$this->string(100),
            'so_total'=>$this->integer(11),
            'so_statuscode'=>$this->integer(11)->notNull(),//references dari trans_statuscode column trans_value
            'effective_from'=>$this->dateTime(),
            'effective_till'=>$this->dateTime(),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-so-customer-id',
            'trans_so',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-so-user-id',
            'trans_so',
            'cust_user_id',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-so-paymentmethod-id',
            'trans_so',
            'paymentmethod_id',
            'trans_paymentmethod',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-so-bankaccount-id',
            'trans_so',
            'cust_bank_account_id',
            'cust_bank_accounts',
            'id',
            'CASCADE'
        );

        /*$this->addForeignKey(
            'fk-so-statuscode-id',
            'trans_so',
            'so_statuscode',
            'trans_statuscode',
            'trans_value',
            'CASCADE'
        );
        */


        $this->createTable('trans_so_details',
        [
            'id' => $this->primaryKey(),
            'so_id'=>$this->integer(11)->notNull(),
            'product_id'=>$this->integer(11)->notNull(),
            'product_name'=>$this->string(200),
            'product_varians_id'=>$this->integer(11),
            'product_varians_name'=>$this->string(200),
            'product_price'=>$this->integer(11)->notNull(),
            'product_quantity'=> $this->integer(11)->notNull()->defaultValue(1),
            'provider_id'=>$this->integer(11),
            'provider_name'=>$this->string(200),
            'shippingcost_id'=>$this->integer(11),
            'shippingcost'=>$this->integer(11),
            'user_address_id'=>$this->integer(11),
            'address_name'=>$this->string(50),
            'address_line1'=> $this->string(200),
            'address_line2'=> $this->string(200),
            'address_country'=>$this->string(200),
            'address_province'=>$this->string(200),
            'address_countytown'=>$this->string(200),
            'address_districts'=>$this->string(200),
            'address_postalzip'=>$this->integer(5),
            'cart_remarks'=>$this->string(200),
            'details_total'=>$this->integer(11),
            'custome'=>$this->string(2)->defaultValue('10'),
            'details_statuscode'=>$this->integer(11),//jika status per detail, references dari trans_statuscode column trans_value
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-details-so-id',
            'trans_so_details',
            'so_id',
            'trans_so',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-sodetails-product-id',
            'trans_so_details',
            'product_id',
            'master_products',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-sodetails-varians-id',
            'trans_so_details',
            'product_varians_id',
            'master_product_varians',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-sodetails-provider-id',
            'trans_so_details',
            'provider_id',
            'trans_shippingprovider',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-sodetails-shippingcost-id',
            'trans_so_details',
            'shippingcost_id',
            'trans_shippingcost',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-sodetails-address-id',
            'trans_so_details',
            'user_address_id',
            'cust_users_address',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-sodetails-createdby-id',
            'trans_so_details',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-sodetails-lastmodif-id',
            'trans_so_details',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161008_045236_trans_so cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
