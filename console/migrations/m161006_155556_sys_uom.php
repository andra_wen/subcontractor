<?php

use yii\db\Migration;

class m161006_155556_sys_uom extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('sys_uom',
        [
            'id' => $this->primaryKey(),
            'code'=>$this->string(10)->notNull()->unique(),
            'uom_name'=>$this->string(30)->unique(),
            'uom_description'=>$this->string(50),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-uom-createdby-id',
            'sys_uom',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-uom-lastmodif-id',
            'sys_uom',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->execute($this->insertStandardUOM());
    }

    private function insertStandardUOM()
    {
        $current_time = time();
        $super_user = 1;

        $this->batchInsert('sys_uom',
            [
                'id','code','uom_name','uom_description','createdby', 'createdon', 'lastmodifby','lastmodif', 'status'
            ],
            [
                [1,'μL','Microliter','Microliter',$super_user, $current_time, $super_user, $current_time,10],
                [2,'μM','Micrometer','Micrometer',$super_user, $current_time, $super_user, $current_time,10],
                [3,'ACR','Acre','Acre',$super_user, $current_time, $super_user, $current_time,10],
                [4,'A','Ampere','Ampere',$super_user, $current_time, $super_user, $current_time,10],
                [5,'YR','Years','Years',$super_user, $current_time, $super_user, $current_time,10],
                [6,'ARP','Arpent','Arpent',$super_user, $current_time, $super_user, $current_time,10],
                [7,'BE','Bundle','Bundle',$super_user, $current_time, $super_user, $current_time,10],
                [8,'BFT','Board Foot','Board Foot',$super_user, $current_time, $super_user, $current_time,10],
                [9,'BAG','Bag','Bag',$super_user, $current_time, $super_user, $current_time,10],
                [10,'BCK','Bucket','Bucket',$super_user, $current_time, $super_user, $current_time,10],
                [11,'BK','Book','Book',$super_user, $current_time, $super_user, $current_time,10],
                [12,'BL','Bale','Bale',$super_user, $current_time, $super_user, $current_time,10],
                [13,'BBL','Barrel','Barrel',$super_user, $current_time, $super_user, $current_time,10],
                [14,'BOT','Bottle','Bottle',$super_user, $current_time, $super_user, $current_time,10],
                [15,'BU','Bushel','Bushel',$super_user, $current_time, $super_user, $current_time,10],
                [16,'BOX','Box','Box',$super_user, $current_time, $super_user, $current_time,10],
                [17,'NAM','Nanometer','Nanometer',$super_user, $current_time, $super_user, $current_time,10],
                [18,'CAN','Can','Can',$super_user, $current_time, $super_user, $current_time,10],
                [19,'CEN','Hundred','Hundred',$super_user, $current_time, $super_user, $current_time,10],
                [20,'CG','Card','Card',$super_user, $current_time, $super_user, $current_time,10],
                [21,'MC2','Square centimeter','Square centimeter',$super_user, $current_time, $super_user, $current_time,10],
                [22,'CM3','Cubic centimeter','Cubic centimeter',$super_user, $current_time, $super_user, $current_time,10],
                [23,'CM','Centimeter','Centimeter',$super_user, $current_time, $super_user, $current_time,10],
                [24,'CRT','Crate','Crate',$super_user, $current_time, $super_user, $current_time,10],
                [25,'CE','Case','Case',$super_user, $current_time, $super_user, $current_time,10],
                [26,'CAR','Carton','Carton',$super_user, $current_time, $super_user, $current_time,10],
                [27,'DAY','Day','Day',$super_user, $current_time, $super_user, $current_time,10],
                [28,'DE','Deal','Deal',$super_user, $current_time, $super_user, $current_time,10],
                [29,'DM3','Cubic decimeter','Cubic decimeter',$super_user, $current_time, $super_user, $current_time,10],
                [30,'DM','Decimeter','Decimeter',$super_user, $current_time, $super_user, $current_time,10],
                [31,'DRM','Drum','Drum',$super_user, $current_time, $super_user, $current_time,10],
                [32,'DS','Display','Display',$super_user, $current_time, $super_user, $current_time,10],
                [33,'DZN','Dozen','Dozen',$super_user, $current_time, $super_user, $current_time,10],
                [34,'JOB','Job','Job',$super_user, $current_time, $super_user, $current_time,10],
                [35,'EA','Each','Each',$super_user, $current_time, $super_user, $current_time,10],
                [36,'FT','Foot','Foot',$super_user, $current_time, $super_user, $current_time,10],
                [37,'FPM','Foot Per Minute','Foot Per Minute',$super_user, $current_time, $super_user, $current_time,10],
                [38,'FT2','Square foot','Square foot',$super_user, $current_time, $super_user, $current_time,10],
                [39,'FT3','Cubic foot','Cubic foot',$super_user, $current_time, $super_user, $current_time,10],
                [40,'GAL','US gallon','US gallon',$super_user, $current_time, $super_user, $current_time,10],
                [41,'G','Gram','Gram',$super_user, $current_time, $super_user, $current_time,10],
                [42,'GRO','Gross','Gross',$super_user, $current_time, $super_user, $current_time,10],
                [43,'HA','Hectare','Hectare',$super_user, $current_time, $super_user, $current_time,10],
                [44,'H','Hour','Hour',$super_user, $current_time, $super_user, $current_time,10],
                [45,'IN','Inch','Inch',$super_user, $current_time, $super_user, $current_time,10],
                [46,'IN2','Square inch','Square inch',$super_user, $current_time, $super_user, $current_time,10],
                [47,'IN3','Cubic inch','Cubic inch',$super_user, $current_time, $super_user, $current_time,10],
                [48,'KG','Kilogram','Kilogram',$super_user, $current_time, $super_user, $current_time,10],
                [49,'KM2','Square kilometer','Square kilometer',$super_user, $current_time, $super_user, $current_time,10],
                [50,'KM','Kilometer','Kilometer',$super_user, $current_time, $super_user, $current_time,10],
                [51,'KIT','Kit','Kit',$super_user, $current_time, $super_user, $current_time,10],
                [52,'LB','Pound','Pound',$super_user, $current_time, $super_user, $current_time,10],
                [53,'LF','Linear Foot','Linear Foot',$super_user, $current_time, $super_user, $current_time,10],
                [54,'LOT','Lot','Lot',$super_user, $current_time, $super_user, $current_time,10],
                [55,'L','Liter','Liter',$super_user, $current_time, $super_user, $current_time,10],
                [56,'MBF','1000 Board Feet','1000 Board Feet',$super_user, $current_time, $super_user, $current_time,10],
                [57,'MCF','1000 Cubic Feet','1000 Cubic Feet',$super_user, $current_time, $super_user, $current_time,10],
                [58,'MG','Milligram','Milligram',$super_user, $current_time, $super_user, $current_time,10],
                [59,'MI2','Square mile','Square mile',$super_user, $current_time, $super_user, $current_time,10],
                [60,'TH','Thousand','Thousand',$super_user, $current_time, $super_user, $current_time,10],
                [61,'MIN','Minute','Minute',$super_user, $current_time, $super_user, $current_time,10],
                [62,'ML','Milliliter','Milliliter',$super_user, $current_time, $super_user, $current_time,10],
                [63,'MM2','Square millimeter','Square millimeter',$super_user, $current_time, $super_user, $current_time,10],
                [64,'MM','Millimeter','Millimeter',$super_user, $current_time, $super_user, $current_time,10],
                [65,'MON','Month','Month',$super_user, $current_time, $super_user, $current_time,10],
                [66,'M2','Square meter','Square meter',$super_user, $current_time, $super_user, $current_time,10],
                [67,'M3','Cubic meter','Cubic meter',$super_user, $current_time, $super_user, $current_time,10],
                [68,'M','Meter','Meter',$super_user, $current_time, $super_user, $current_time,10],
                [69,'OZ','Ounce','Ounce',$super_user, $current_time, $super_user, $current_time,10],
                [70,'FOZ','Fluid Ounce US','Fluid Ounce US',$super_user, $current_time, $super_user, $current_time,10],
                [71,'000','Group proportion','Group proportion',$super_user, $current_time, $super_user, $current_time,10],
                [72,'PAD','Pad','Pad',$super_user, $current_time, $super_user, $current_time,10],
                [73,'PAL','Pallet','Pallet',$super_user, $current_time, $super_user, $current_time,10],
                [74,'PAC','Pack','Pack',$super_user, $current_time, $super_user, $current_time,10],
                [75,'PR','Pair','Pair',$super_user, $current_time, $super_user, $current_time,10],
                [76,'PT','Pint, US liquid','Pint, US liquid',$super_user, $current_time, $super_user, $current_time,10],
                [77,'QT','Quart, US liquid','Quart, US liquid',$super_user, $current_time, $super_user, $current_time,10],
                [78,'RM','Ream','Ream',$super_user, $current_time, $super_user, $current_time,10],
                [79,'ROL','Roll','Roll',$super_user, $current_time, $super_user, $current_time,10],
                [80,'SET','Set','Set',$super_user, $current_time, $super_user, $current_time,10],
                [81,'MI','Mile','Mile',$super_user, $current_time, $super_user, $current_time,10],
                [82,'SQ','Square','Square',$super_user, $current_time, $super_user, $current_time,10],
                [83,'ST','Sheet','Sheet',$super_user, $current_time, $super_user, $current_time,10],
                [84,'TON','Ton, short (2000 lb)','Ton, short (2000 lb)',$super_user, $current_time, $super_user, $current_time,10],
                [85,'T','Tonne (metric ton,1000)','Tonne (metric ton,1000)',$super_user, $current_time, $super_user, $current_time,10],
                [86,'TU','Tube','Tube',$super_user, $current_time, $super_user, $current_time,10],
                [87,'VIA','Vial','Vial',$super_user, $current_time, $super_user, $current_time,10],
                [88,'CDS','Cord','Cord',$super_user, $current_time, $super_user, $current_time,10],
                [89,'WK','Weeks','Weeks',$super_user, $current_time, $super_user, $current_time,10],
                [90,'W','Watt','Watt',$super_user, $current_time, $super_user, $current_time,10],
                [91,'YD2','Square yard','Square yard',$super_user, $current_time, $super_user, $current_time,10],
                [92,'YD3','Cubic yard','Cubic yard',$super_user, $current_time, $super_user, $current_time,10],
                [93,'YD','Yard','Yard',$super_user, $current_time, $super_user, $current_time,10],
                [94,'LUG','Lug','Lug',$super_user, $current_time, $super_user, $current_time,10],
                [95,'LE','Deliverable line item','Deliverable line item',$super_user, $current_time, $super_user, $current_time,10],
            ]);
    }

    public function down()
    {
        echo "m161006_155826_master_uom cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
