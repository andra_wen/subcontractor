<?php

use yii\db\Migration;

class m161005_151425_cust_bank_accounts extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('cust_bank_accounts',
        [
            'id' => $this->primaryKey(),
            'bank_id'=> $this->integer(11)->notNull(),
            'bank_accountname'=>$this->string(100)->notNull(),
            'bank_accountno'=> $this->string(100)->notNull()->unique(),
            'bank_info'=> $this->string(200),
            'bank_remarks'=>$this->string(200),
            'isactive'=>$this->integer(1)->defaultValue(1)->notNull(),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-accounts-bank-id',
            'cust_bank_accounts',
            'bank_id',
            'cust_banks',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-accounts-createdby-id',
            'cust_bank_accounts',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-accounts-lastmodif-id',
            'cust_bank_accounts',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161005_151425_cust_bank_accounts cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
