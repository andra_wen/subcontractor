<?php

use yii\db\Migration;

class m161007_152829_master_sliders extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('master_sliders',
        [
            'id' => $this->primaryKey(),
            'customer_id'=>$this->integer(11)->notNull(),
            'page_id'=>$this->integer(11),
            'slider_name'=>$this->string(100),
            'slider_title'=>$this->string(100),
            'slider_pict'=>$this->string(100),
            'slider_alt'=>$this->string(100),
            'isactive'=>$this->integer(1)->defaultValue(1),
            'sort_no'=>$this->integer(11)->defaultValue(10),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-slider-customer-id',
            'master_sliders',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-slider-page-id',
            'master_sliders',
            'page_id',
            'master_pages',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-slider-createdby-id',
            'master_sliders',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-slider-lastmodif-id',
            'master_sliders',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161007_152829_master_sliders cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
