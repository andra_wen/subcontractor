<?php

use yii\db\Migration;

class m161007_163252_trans_shippingcost extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('trans_shippingcost',
        [
            'id' => $this->primaryKey(),
            'customer_id'=>$this->integer(11)->notNull(),
            'shippingprovider_id'=>$this->integer(11)->notNull(),
            'shipping_country'=>$this->integer(11)->notNull(),
            'shipping_province'=>$this->integer(11)->notNull(),
            'shipping_countytown'=> $this->integer(11)->notNull(),
            'shipping_districts'=> $this->integer(11)->notNull(),
            'shipping_note'=>$this->string(100),
            'shipping_prices'=>$this->integer(11),
            'shipping_minweight'=>$this->integer(11),
            'shipping_minweightprices'=>$this->integer(),
            'shipping_minwegithnextprice'=>$this->integer(),
            'shipping_maxweight'=>$this->integer(),
            'isactive'=>$this->integer(1)->defaultValue(1),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-shippingcost-customer-id',
            'trans_shippingcost',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-shippingcost-provider-id',
            'trans_shippingcost',
            'shippingprovider_id',
            'trans_shippingprovider',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-shippingcost-country-id',
            'trans_shippingcost',
            'shipping_country',
            'sys_country',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-shippingcost-province-id',
            'trans_shippingcost',
            'shipping_province',
            'sys_province',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-shippingcost-countytown-id',
            'trans_shippingcost',
            'shipping_countytown',
            'sys_countytown',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-shippingcost-district-id',
            'trans_shippingcost',
            'shipping_districts',
            'sys_districts',
            'id',
            'CASCADE'
        );


    }

    public function down()
    {
        echo "m161007_163252_trans_shippingcost cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
