<?php

use yii\db\Migration;

class m170520_163944_master_files extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('master_files',
            [
                'id' => $this->primaryKey(),
                'customer_id'=>$this->integer(11)->notNull(),
                'page_id'=>$this->integer(11),
                'file_name'=>$this->string(100),
                'file_title'=>$this->string(100),
                'file_desc'=>$this->string(500),
                'file_pict'=>$this->string(100),
                'the_files'=>$this->string(100),
                'file_alt'=>$this->string(100),
                'isactive'=>$this->integer(1)->defaultValue(1),
                'sort_no'=>$this->integer(11)->defaultValue(10),
                'file_date'=>$this->date(),
                'effective_from'=>$this->dateTime(),
                'effective_till'=>$this->dateTime(),
                'createdby'=> $this->integer(11)->notNull(),
                'createdon'=> $this->dateTime()->notNull(),
                'lastmodifby'=> $this->integer(11)->notNull(),
                'lastmodif'=> $this->dateTime()->notNull(),
                'status'=> $this->smallInteger()->notNull()->defaultValue(10),
            ]);

        $this->addForeignKey(
            'fk-file-customer-id',
            'master_files',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-file-page-id',
            'master_files',
            'page_id',
            'master_pages',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-file-createdby-id',
            'master_files',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-file-lastmodif-id',
            'master_files',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m170520_163944_master_files cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
