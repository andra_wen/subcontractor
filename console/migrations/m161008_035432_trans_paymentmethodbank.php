<?php

use yii\db\Migration;

class m161008_035432_trans_paymentmethodbank extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('trans_paymentmethodbank',
        [
            'id' => $this->primaryKey(),
            'customer_id'=>$this->integer(11)->notNull(),
            'paymentmethod_id'=>$this->integer(11)->notNull(),
            'cust_bank_account_id'=> $this->integer(11)->notNull(),
            'isactive'=>$this->integer(1)->defaultValue(1),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-paymentmethodbank-customer-id',
            'trans_paymentmethodbank',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-paymentmethodbank-method-id',
            'trans_paymentmethodbank',
            'paymentmethod_id',
            'trans_paymentmethod',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-paymentmethodbank-bankaccount-id',
            'trans_paymentmethodbank',
            'cust_bank_account_id',
            'cust_bank_accounts',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-paymentmethodbank-createdby-id',
            'trans_paymentmethodbank',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-paymentmethodbank-lastmodif-id',
            'trans_paymentmethodbank',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161008_035432_trans_paymentmethodbank cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
