<?php

use yii\db\Migration;

class m161005_154440_cust_client_profiles extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('cust_client_profiles',
        [
            'id' => $this->primaryKey(),
            'cust_client_id'=> $this->integer(11)->notNull(),
            'user_firstname'=> $this->string(150)->notNull(),
            'user_middlename'=> $this->string(150)->notNull(),
            'user_lastname'=> $this->string(150)->notNull(),
            'placeofbirth'=> $this->string(150),
            'dateofbirth'=> $this->date(),
            'gender'=>$this->string(1)->defaultValue('M')->notNull(),
            'marriagestatus'=>$this->string(15)->defaultValue('single'),
            'language'=> $this->integer(11)->notNull(),
            'user_picture'=>$this->string(200),
        ]);

        $this->addForeignKey(
            'fk-clientprofiles-client-id',
            'cust_client_profiles',
            'cust_client_id',
            'cust_clients',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161005_154440_cust_client_profiles cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
