<?php

use yii\db\Migration;

class m161004_164806_sys_language extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('sys_language',
        [
            'id' => $this->primaryKey(),
            'code'=>$this->string(10)->notNull()->unique(),
            'short_name'=> $this->string(100)->notNull()->unique(),
            'long_name'=> $this->string(200),
            'iso_code'=> $this->string(50)->unique(),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-language-createdby-id',
            'sys_language',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-language-lastmodifby-id',
            'sys_language',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->execute($this->insertBasicLanguage());
    }

    private function insertBasicLanguage()
    {
        $current_time = time();
        $super_user = 1;

        $this->batchInsert('sys_language',
            [
                'code','short_name','long_name','iso_code','createdby', 'createdon', 'lastmodifby',
                'lastmodif', 'status'
            ],
            [
                ['1','Bahasa', 'Bahasa Indonesia', 'ID', $super_user,$current_time, $super_user, $current_time,10],
                ['2','English', 'English', 'EN', $super_user,$current_time, $super_user, $current_time,10],
            ]);
    }

    public function down()
    {
        echo "m161004_164806_sys_language cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
