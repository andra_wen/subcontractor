<?php

use yii\db\Migration;

class m170119_074353_trans_so_confirmation extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('trans_so_confirmation',
        [
            'id' => $this->primaryKey(),
            'so_id'=>$this->integer(11)->notNull(),
            'so_no'=>$this->string(50)->unique()->notNull(),
            'paymentmethod_id'=> $this->integer(11),
            'paymentmethod_name'=>$this->string(50),
            'account_no'=>$this->string(50),
            'account_holder'=>$this->string(50),
            'phone_number'=>$this->string(30),
            'total_amount'=>$this->integer(11),
            'payment_date'=>$this->date(),
            'confirm_remarks'=>$this->string(300),
            'confirm_attachment'=>$this->string(200),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-confirm-so-id',
            'trans_so_confirmation',
            'so_id',
            'trans_so',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-confirm-paymentmethod-id',
            'trans_so_confirmation',
            'paymentmethod_id',
            'trans_paymentmethod',
            'id',
            'CASCADE'
        );

    }

    public function down()
    {
        echo "m170119_074353_trans_so_confirmation cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
