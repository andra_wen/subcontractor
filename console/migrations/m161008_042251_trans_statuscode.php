<?php

use yii\db\Migration;

class m161008_042251_trans_statuscode extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('trans_statuscode',
        [
            'id' => $this->integer(11),
            'customer_id'=>$this->integer(11)->notNull(),
            'trans_code'=>$this->string(50)->notNull(),
            'trans_name'=> $this->string(50)->notNull(),
            'trans_description'=> $this->string(300),
            'trans_value'=>$this->integer(11)->defaultValue(1)->notNull(),
            'isactive'=>$this->integer(1)->defaultValue(1),
            'sort_no'=>$this->integer(11)->defaultValue(10),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
            'PRIMARY KEY(id,trans_value)'
        ]);

        $this->addForeignKey(
            'fk-transstatus-customer-id',
            'trans_statuscode',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-transsatatus-createdby-id',
            'trans_statuscode',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-transstatus-lastmodif-id',
            'trans_statuscode',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->execute($this->insertDefaultStatusCode());
    }

    private function insertDefaultStatusCode()
    {
        $current_time = time();
        $super_user = 1;

        $this->batchInsert('trans_statuscode',
        [
            'id','customer_id','trans_code','trans_name','trans_description','trans_value','sort_no',
            'createdby', 'createdon', 'lastmodifby','lastmodif', 'status'
        ],
        [
            [1, 1,'OPEN','Open','Invoice baru, Customer belum konfirmasi pembayaran',1,1,$super_user, $current_time, $super_user, $current_time,10],
            [2, 1,'VLDN','Validation','Invoice baru, Customer sudah konfirmasi pembayaran',2,2,$super_user, $current_time, $super_user, $current_time,10],
            [3, 1,'VLDD','Validated','Setelah di validasi Admin, Barang akan disiapin admin',3,3,$super_user, $current_time, $super_user, $current_time,10],
            [4, 1,'PCKG','Packing','Barang sedang di siapin oleh Admin',4,4,$super_user, $current_time, $super_user, $current_time,10],
            [5, 1,'PRPG','Preparing','Barang sudah di siapin, barang sedang tunggu dikirim atau di pickup',5,5,$super_user, $current_time, $super_user, $current_time,10],
            [6, 1,'SNDG','Sending','Barang sudah di pickup, dalam proses pengiriman atau perjalanan ke Customer',6,6,$super_user, $current_time, $super_user, $current_time,10],
            [7, 1,'RCVD','Received','Barang sudah tiba di tempat Customer',7,7,$super_user, $current_time, $super_user, $current_time,10],
            [8, 1,'RCPT','Receipt','Barang sudah tiba di tempat Customer dan sudah di Confirm oleh Customer',8,8,$super_user, $current_time, $super_user, $current_time,10],
            [9, 1,'CNCL','Cancelled','Barang di batalkan oleh pihak Customer',9,9,$super_user, $current_time, $super_user, $current_time,10],
            [10, 1,'ACNC','Admin Cancelled','Barang di batalkan oleh pihak Admin atau Penjual',0,10,$super_user, $current_time, $super_user, $current_time,10],
            ]);
    }

    public function down()
    {
        echo "m161008_042251_trans_statuscode cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
