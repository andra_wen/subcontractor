<?php

use yii\db\Migration;

class m161008_041037_master_product_events extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('master_product_events',
        [
            'id' => $this->primaryKey(),
            'product_id'=>$this->integer(11)->notNull(),
            'event_name'=> $this->string(50)->notNull(),//promotion, new arrival, sales
            'event_caption'=>$this->string(100),//special promotion, etc
            'event_label'=>$this->string(100),//information, like number value for discount, reduce the product price, etc
            'event_stringvalue'=>$this->string(100),
            'event_numbervalue'=>$this->integer(11)->defaultValue(0),
            'effective_from'=>$this->dateTime(),
            'effective_till'=>$this->dateTime(),
            'isactive'=>$this->integer(1)->defaultValue(1),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-productevent-product-id',
            'master_product_events',
            'product_id',
            'master_products',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-productevent-createdby-id',
            'master_product_events',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-productevent-lastmodif-id',
            'master_product_events',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161008_041037_master_product_events cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
