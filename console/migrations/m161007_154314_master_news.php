<?php

use yii\db\Migration;

class m161007_154314_master_news extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('master_news',
        [
            'id' => $this->primaryKey(),
            'customer_id'=>$this->integer(11)->notNull(),
            'page_id'=>$this->integer(11),
            'news_name'=>$this->string(100),
            'news_title'=>$this->string(100),
            'news_content'=>$this->string(500),
            'news_pict'=>$this->string(100),
            'news_alt'=>$this->string(100),
            'isactive'=>$this->integer(1)->defaultValue(1),
            'sort_no'=>$this->integer(11)->defaultValue(10),
            'effective_from'=>$this->dateTime(),
            'effective_till'=>$this->dateTime(),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-news-customer-id',
            'master_news',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-news-page-id',
            'master_news',
            'page_id',
            'master_pages',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-news-createdby-id',
            'master_news',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-news-lastmodif-id',
            'master_news',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161007_154314_master_news cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
