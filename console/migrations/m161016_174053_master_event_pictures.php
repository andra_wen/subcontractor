<?php

use yii\db\Migration;

class m161016_174053_master_event_pictures extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('master_events_pictures',
            [
                'id' => $this->primaryKey(),
                'event_id'=>$this->integer(11)->notNull(),
                'picture_name'=>$this->string(50),
                'picture_path'=>$this->string(100)->notNull(),
                'picture_path_md'=>$this->string(100),
                'picture_path_sm'=>$this->string(100),
                'picture_path_xs'=>$this->string(100),
                'picture_path_thumbnail'=>$this->string(100)->notNull(),
                'picture_alt'=> $this->string(50),
                'sort_no'=>$this->integer(11)->defaultValue(10),
                'createdby'=> $this->integer(11)->notNull(),
                'createdon'=> $this->dateTime()->notNull(),
                'lastmodifby'=> $this->integer(11)->notNull(),
                'lastmodif'=> $this->dateTime()->notNull(),
                'status'=> $this->smallInteger()->notNull()->defaultValue(10),
            ]);

        $this->addForeignKey(
            'fk-picture-event-id',
            'master_events_pictures',
            'event_id',
            'master_events',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-pictureevent-createdby-id',
            'master_events_pictures',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-pictureevent-lastmodif-id',
            'master_events_pictures',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161016_174053_master_event_pictures cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
