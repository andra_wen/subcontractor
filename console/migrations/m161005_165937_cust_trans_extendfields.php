<?php

use yii\db\Migration;

class m161005_165937_cust_trans_extendfields extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('cust_trans_extendfields',
        [
            'id' => $this->primaryKey(),
            'table_name'=> $this->string(50)->notNull(),
            'table_fields'=>$this->string(50),
            'table_fields_ref'=>$this->integer(11),
            'field_label'=> $this->string(100)->notNull(),
            'field_value'=> $this->string(200),
            'field_language'=>$this->integer(11)->notNull(),
        ]);

        $this->addForeignKey(
            'fk-custextend-language-id',
            'cust_trans_extendfields',
            'field_language',
            'sys_language',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161005_165937_cust_trans_extendfields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
