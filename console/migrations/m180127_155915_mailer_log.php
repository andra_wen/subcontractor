<?php

use yii\db\Migration;

class m180127_155915_mailer_log extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('mailer_log',
        [
            'id' => $this->primaryKey(),
            'customer_id'=>$this->integer(11)->notNull(),
            'mail_to_address'=>$this->string(500),
            'mail_to_name'=>$this->string(500),
            'mail_cc_address'=>$this->string(500),
            'mail_cc_name'=>$this->string(500),
            'mail_subject'=>$this->string(300),
            'mail_to_message'=>$this->string(2500),
            'isactive'=>$this->integer(1)->defaultValue(1),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-mailer-customer-id',
            'mailer_log',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-mailer-createdby-id',
            'mailer_log',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m180127_155915_mailer_log cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
