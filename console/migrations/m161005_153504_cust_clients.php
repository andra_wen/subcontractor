<?php

use yii\db\Migration;

class m161005_153504_cust_clients extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('cust_clients',
        [
            'id' => $this->primaryKey(),
            'customer_id'=> $this->integer(11)->notNull(),
            'username'=> $this->string(100)->unique()->notNull(),
            'auth_key'=> $this->string(50)->notNull(),
            'password_hash'=> $this->string()->notNull(),
            'password_reset_token'=> $this->string(300),
            'email'=>$this->string(300)->unique(),
            'effective_from'=>$this->dateTime(),
            'effective_till'=>$this->dateTime(),
            'createdon'=> $this->dateTime(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-clients-customer-id',
            'cust_clients',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161005_153504_cust_clients cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
