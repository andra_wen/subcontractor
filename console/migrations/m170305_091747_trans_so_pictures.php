<?php

use yii\db\Migration;

class m170305_091747_trans_so_pictures extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('trans_so_pictures',
        [
            'id' => $this->primaryKey(),
            'id_so_details'=>$this->integer(11)->notNull(),
            'picture_name'=>$this->string(50),
            'picture_path'=>$this->string(100)->notNull(),
        ]);

        $this->addForeignKey(
            'fk-picture-so-id',
            'trans_so_pictures',
            'id_so_details',
            'trans_so_details',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m170305_091747_so_pictures cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
