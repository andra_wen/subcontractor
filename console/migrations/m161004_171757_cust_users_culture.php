<?php

use yii\db\Migration;

class m161004_171757_cust_users_culture extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('cust_users_culture',
        [
            'id' => $this->primaryKey(),
            'cust_user_id'=>$this->integer(11)->notNull(),
            'themes_color'=> $this->string(100)->notNull(),
            'language'=> $this->string(5)->notNull(),
            'country'=> $this->integer(11)->notNull()->defaultValue('95'),
            'dateFormat'=> $this->string(30),
            'user_currency'=> $this->integer(11),
            'user_picture'=>$this->string(300),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-userculture-user-id',
            'cust_users_culture',
            'cust_user_id',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-userculture-createdby-id',
            'cust_users_culture',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-userculture-lastmodif-id',
            'cust_users_culture',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161003_171757_cust_users_culture cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
