<?php

use yii\db\Migration;

class m161004_023011_cust_users_profile extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('cust_users_profile',
        [
            'id' => $this->primaryKey(),
            'cust_user_id'=>$this->integer(11)->notNull(),
            'user_firstname'=> $this->string(150)->notNull(),
            'user_middlename'=> $this->string(150)->notNull(),
            'user_lastname'=> $this->string(150)->notNull(),
            'user_picture'=> $this->string(100),
            'placeofbirth'=> $this->string(150),
            'dateofbirth'=> $this->date(),
            'gender'=>$this->string(1)->defaultValue('M')->notNull(),
            'marriagestatus'=>$this->string(15)->defaultValue('single'),
            'bloodtype'=>$this->string(2),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-userprofile-user-id',
            'cust_users_profile',
            'cust_user_id',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-userprofile-createdby-id',
            'cust_users_profile',
            'createdby',
            'cust_users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-userprofile-lastmodif-id',
            'cust_users_profile',
            'lastmodifby',
            'cust_users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m161004_023011_cust_users_profile cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
