<?php

use yii\db\Migration;

class m161003_162323_cust_users extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('cust_users',
        [
            'id' => $this->primaryKey(),
            'customer_id'=>$this->integer(11)->notNull(),
            'username'=> $this->string(100)->unique()->notNull(),
            'auth_key'=> $this->string(50)->notNull(),
            'password_hash'=> $this->string()->notNull(),
            'password_reset_token'=> $this->string(300),
            'email'=>$this->string(300)->unique(),
            'effective_from'=>$this->dateTime(),
            'effective_till'=>$this->dateTime(),
            'createdby'=> $this->integer(11)->notNull(),
            'createdon'=> $this->dateTime()->notNull(),
            'lastmodifby'=> $this->integer(11)->notNull(),
            'lastmodif'=> $this->dateTime()->notNull(),
            'status'=> $this->smallInteger()->notNull()->defaultValue(10),
        ]);

        $this->addForeignKey(
            'fk-users-customer-id',
            'cust_users',
            'customer_id',
            'master_customer',
            'id',
            'CASCADE'
        );

        $this->execute($this->insertSuperAdmin());
    }

    private function insertSuperAdmin()
    {
        $current_time = time();
        $auth_key = Yii::$app->security->generateRandomString();
        $pass_hash = Yii::$app->security->generatePasswordHash('admin12345');

        $this->batchInsert('cust_users',
        [
            'customer_id','username','auth_key','password_hash','password_reset_token','email',
            'effective_from', 'effective_till','createdon', 'status'
        ],
        [
            ['1','superAdmin', $auth_key, $pass_hash,$auth_key,'admin@customer.com',$current_time, $current_time,$current_time,10],
        ]);
    }

    public function down()
    {
        echo "m161003_162323_cust_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
