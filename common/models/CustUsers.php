<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "cust_users".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $effective_from
 * @property string $effective_till
 * @property string $createdon
 * @property string $lastmodif
 * @property integer $status
 * @property integer $roles_name
 *
 * @property CustAddress[] $custAddresses
 * @property CustAddress[] $custAddresses0
 * @property CustAddress[] $custAddresses1
 * @property CustBankAccounts[] $custBankAccounts
 * @property CustBankAccounts[] $custBankAccounts0
 * @property CustBanks[] $custBanks
 * @property CustBanks[] $custBanks0
 * @property CustLanguage[] $custLanguages
 * @property CustLanguage[] $custLanguages0
 * @property CustRoles[] $custRoles
 * @property CustRoles[] $custRoles0
 * @property MasterCustomer $customer
 * @property CustUsersCulture[] $custUsersCultures
 * @property CustUsersCulture[] $custUsersCultures0
 * @property CustUsersCulture[] $custUsersCultures1
 * @property CustUsersProfile[] $custUsersProfiles
 * @property CustUsersProfile[] $custUsersProfiles0
 * @property CustUsersProfile[] $custUsersProfiles1
 * @property MasterCategory[] $masterCategories
 * @property MasterCategory[] $masterCategories0
 * @property MasterCustomerProfile[] $masterCustomerProfiles
 * @property MasterCustomerProfile[] $masterCustomerProfiles0
 * @property MasterEvents[] $masterEvents
 * @property MasterEvents[] $masterEvents0
 * @property MasterEventsPictures[] $masterEventsPictures
 * @property MasterEventsPictures[] $masterEventsPictures0
 * @property MasterNews[] $masterNews
 * @property MasterNews[] $masterNews0
 * @property MasterNewsPictures[] $masterNewsPictures
 * @property MasterNewsPictures[] $masterNewsPictures0
 * @property MasterPages[] $masterPages
 * @property MasterPages[] $masterPages0
 * @property MasterProductEvents[] $masterProductEvents
 * @property MasterProductEvents[] $masterProductEvents0
 * @property MasterProductPictures[] $masterProductPictures
 * @property MasterProductPictures[] $masterProductPictures0
 * @property MasterProductProfiles[] $masterProductProfiles
 * @property MasterProductProfiles[] $masterProductProfiles0
 * @property MasterProductVarians[] $masterProductVarians
 * @property MasterProductVarians[] $masterProductVarians0
 * @property MasterProducts[] $masterProducts
 * @property MasterProducts[] $masterProducts0
 * @property MasterSliders[] $masterSliders
 * @property MasterSliders[] $masterSliders0
 * @property SysCountry[] $sysCountries
 * @property SysCountry[] $sysCountries0
 * @property SysCountytown[] $sysCountytowns
 * @property SysCountytown[] $sysCountytowns0
 * @property SysCurrency[] $sysCurrencies
 * @property SysCurrency[] $sysCurrencies0
 * @property SysDistricts[] $sysDistricts
 * @property SysDistricts[] $sysDistricts0
 * @property SysLanguage[] $sysLanguages
 * @property SysLanguage[] $sysLanguages0
 * @property SysProvince[] $sysProvinces
 * @property SysProvince[] $sysProvinces0
 * @property SysSeqPattern[] $sysSeqPatterns
 * @property SysSeqPattern[] $sysSeqPatterns0
 * @property SysUom[] $sysUoms
 * @property SysUom[] $sysUoms0
 * @property TransAdjustments[] $transAdjustments
 * @property TransAdjustments[] $transAdjustments0
 * @property TransCarts[] $transCarts
 * @property TransCarts[] $transCarts0
 * @property TransPaymentmethod[] $transPaymentmethods
 * @property TransPaymentmethod[] $transPaymentmethods0
 * @property TransPaymentmethodbank[] $transPaymentmethodbanks
 * @property TransPaymentmethodbank[] $transPaymentmethodbanks0
 * @property TransShippingprovider[] $transShippingproviders
 * @property TransShippingprovider[] $transShippingproviders0
 * @property TransSoDetails[] $transSoDetails
 * @property TransSoDetails[] $transSoDetails0
 * @property TransStatuscode[] $transStatuscodes
 * @property TransStatuscode[] $transStatuscodes0
 * @property TransWishlist[] $transWishlists
 * @property TransWishlist[] $transWishlists0
 */
class CustUsers extends \yii\db\ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cust_users';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'username'], 'required'],
            [['customer_id', 'status', 'roles_name'], 'integer'],
            [['effective_from', 'effective_till', 'createdon'], 'safe'],
            [['username'], 'string', 'max' => 100],
            [['auth_key'], 'string', 'max' => 50],
            [['password_hash'], 'string', 'max' => 255],
            [['password_reset_token', 'email'], 'string', 'max' => 300],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_id' => Yii::t('language', 'Customer ID'),
            'username' => Yii::t('language', 'Username'),
            'auth_key' => Yii::t('language', 'Auth Key'),
            'password_hash' => Yii::t('language', 'Password Hash'),
            'password_reset_token' => Yii::t('language', 'Password Reset Token'),
            'email' => Yii::t('language', 'Email'),
            'effective_from' => Yii::t('language', 'Effective From'),
            'effective_till' => Yii::t('language', 'Effective Till'),
            'createdon' => Yii::t('language', 'Createdon'),
            'status' => Yii::t('language', 'Status'),
            'roles_name' => Yii::t('language', 'Roles'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'createdby' => Yii::t('language', 'Createdby'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
        ];
    }

    //before insert User, set password and authkey
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->setPassword($this->password_hash);
                $this->generateAuthKey();
                $this->generatePasswordResetToken();
            }elseif(Yii::$app->controller->action->id == 'resetpassword'){
                $this->setPassword($this->password_hash);
            }elseif(Yii::$app->controller->action->id == 'change-password'){
                $this->setPassword($this->password_hash);
            }else{
                unset($this->password_hash);
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustAddresses()
    {
        return $this->hasMany(CustAddress::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getCustUsersAddresses()
    {
        return $this->hasMany(CustUsersAddress::className(), ['cust_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustAddresses0()
    {
        return $this->hasMany(CustAddress::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustAddresses1()
    {
        return $this->hasMany(CustAddress::className(), ['cust_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustBankAccounts()
    {
        return $this->hasMany(CustBankAccounts::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustBankAccounts0()
    {
        return $this->hasMany(CustBankAccounts::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustBanks()
    {
        return $this->hasMany(CustBanks::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustBanks0()
    {
        return $this->hasMany(CustBanks::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustLanguages()
    {
        return $this->hasMany(CustLanguage::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustLanguages0()
    {
        return $this->hasMany(CustLanguage::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustRoles()
    {
        return $this->hasMany(CustRoles::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustRoles0()
    {
        return $this->hasMany(CustRoles::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustUsersCultures()
    {
        return $this->hasMany(CustUsersCulture::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustUsersCultures0()
    {
        return $this->hasMany(CustUsersCulture::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustUsersCultures1()
    {
        return $this->hasMany(CustUsersCulture::className(), ['cust_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustUsersProfiles()
    {
        return $this->hasMany(CustUsersProfile::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustUsersProfiles0()
    {
        return $this->hasMany(CustUsersProfile::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustUsersProfiles1()
    {
        return $this->hasMany(CustUsersProfile::className(), ['cust_user_id' => 'id']);
    }
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getCustUsersProducts()
    {
        return $this->hasMany(CustUsersProduct::className(), ['cust_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterCategories()
    {
        return $this->hasMany(MasterCategory::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterCategories0()
    {
        return $this->hasMany(MasterCategory::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterCustomerProfiles()
    {
        return $this->hasMany(MasterCustomerProfile::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterCustomerProfiles0()
    {
        return $this->hasMany(MasterCustomerProfile::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterEvents()
    {
        return $this->hasMany(MasterEvents::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterEvents0()
    {
        return $this->hasMany(MasterEvents::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterEventsPictures()
    {
        return $this->hasMany(MasterEventsPictures::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterEventsPictures0()
    {
        return $this->hasMany(MasterEventsPictures::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterNews()
    {
        return $this->hasMany(MasterNews::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterNews0()
    {
        return $this->hasMany(MasterNews::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterNewsPictures()
    {
        return $this->hasMany(MasterNewsPictures::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterNewsPictures0()
    {
        return $this->hasMany(MasterNewsPictures::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterPages()
    {
        return $this->hasMany(MasterPages::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterPages0()
    {
        return $this->hasMany(MasterPages::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProductEvents()
    {
        return $this->hasMany(MasterProductEvents::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProductEvents0()
    {
        return $this->hasMany(MasterProductEvents::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProductPictures()
    {
        return $this->hasMany(MasterProductPictures::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProductPictures0()
    {
        return $this->hasMany(MasterProductPictures::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProductProfiles()
    {
        return $this->hasMany(MasterProductProfiles::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProductProfiles0()
    {
        return $this->hasMany(MasterProductProfiles::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProductVarians()
    {
        return $this->hasMany(MasterProductVarians::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProductVarians0()
    {
        return $this->hasMany(MasterProductVarians::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProducts()
    {
        return $this->hasMany(MasterProducts::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProducts0()
    {
        return $this->hasMany(MasterProducts::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterSliders()
    {
        return $this->hasMany(MasterSliders::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterSliders0()
    {
        return $this->hasMany(MasterSliders::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysCountries()
    {
        return $this->hasMany(SysCountry::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysCountries0()
    {
        return $this->hasMany(SysCountry::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysCountytowns()
    {
        return $this->hasMany(SysCountytown::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysCountytowns0()
    {
        return $this->hasMany(SysCountytown::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysCurrencies()
    {
        return $this->hasMany(SysCurrency::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysCurrencies0()
    {
        return $this->hasMany(SysCurrency::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysDistricts()
    {
        return $this->hasMany(SysDistricts::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysDistricts0()
    {
        return $this->hasMany(SysDistricts::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysLanguages()
    {
        return $this->hasMany(SysLanguage::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysLanguages0()
    {
        return $this->hasMany(SysLanguage::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysProvinces()
    {
        return $this->hasMany(SysProvince::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysProvinces0()
    {
        return $this->hasMany(SysProvince::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysSeqPatterns()
    {
        return $this->hasMany(SysSeqPattern::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysSeqPatterns0()
    {
        return $this->hasMany(SysSeqPattern::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysUoms()
    {
        return $this->hasMany(SysUom::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysUoms0()
    {
        return $this->hasMany(SysUom::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransAdjustments()
    {
        return $this->hasMany(TransAdjustments::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransAdjustments0()
    {
        return $this->hasMany(TransAdjustments::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransCarts()
    {
        return $this->hasMany(TransCarts::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransCarts0()
    {
        return $this->hasMany(TransCarts::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransPaymentmethods()
    {
        return $this->hasMany(TransPaymentmethod::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransPaymentmethods0()
    {
        return $this->hasMany(TransPaymentmethod::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransPaymentmethodbanks()
    {
        return $this->hasMany(TransPaymentmethodbank::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransPaymentmethodbanks0()
    {
        return $this->hasMany(TransPaymentmethodbank::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransShippingproviders()
    {
        return $this->hasMany(TransShippingprovider::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransShippingproviders0()
    {
        return $this->hasMany(TransShippingprovider::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransSoDetails()
    {
        return $this->hasMany(TransSoDetails::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransSoDetails0()
    {
        return $this->hasMany(TransSoDetails::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransStatuscodes()
    {
        return $this->hasMany(TransStatuscode::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransStatuscodes0()
    {
        return $this->hasMany(TransStatuscode::className(), ['lastmodifby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransWishlists()
    {
        return $this->hasMany(TransWishlist::className(), ['createdby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransWishlists0()
    {
        return $this->hasMany(TransWishlist::className(), ['lastmodifby' => 'id']);
    }
}
