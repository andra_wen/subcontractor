<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "trans_so_details".
 *
 * @property integer $id
 * @property integer $so_id
 * @property integer $product_id
 * @property string $product_name
 * @property integer $product_varians_id
 * @property string $product_varians_name
 * @property integer $product_price
 * @property integer $product_quantity
 * @property integer $provider_id
 * @property string $provider_name
 * @property integer $shippingcost_id
 * @property integer $shippingcost
 * @property integer $user_address_id
 * @property string $address_name
 * @property string $address_line1
 * @property string $address_line2
 * @property string $address_country
 * @property string $address_province
 * @property string $address_countytown
 * @property string $address_districts
 * @property integer $address_postalzip
 * @property string $cart_remarks
 * @property integer $details_total
 * @property integer $details_statuscode
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property TransSo $so
 * @property CustUsersAddress $userAddress
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 * @property MasterProducts $product
 * @property TransShippingprovider $provider
 * @property TransShippingcost $shippingcost0
 * @property MasterProductVarians $productVarians
 */
class TransSoDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trans_so_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['so_id', 'product_id', 'product_price', 'createdby', 'createdon', 'lastmodifby', 'lastmodif'], 'required'],
            [['so_id', 'product_id', 'product_varians_id', 'product_price', 'product_quantity', 'provider_id', 'shippingcost_id', 'shippingcost', 'user_address_id', 'address_postalzip', 'details_total', 'details_statuscode', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['createdon', 'lastmodif'], 'safe'],
            [['product_name', 'product_varians_name', 'provider_name', 'address_line1', 'address_line2', 'address_country', 'address_province', 'address_countytown', 'address_districts', 'cart_remarks'], 'string', 'max' => 200],
            [['address_name'], 'string', 'max' => 50],
            [['so_id'], 'exist', 'skipOnError' => true, 'targetClass' => TransSo::className(), 'targetAttribute' => ['so_id' => 'id']],
            [['user_address_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsersAddress::className(), 'targetAttribute' => ['user_address_id' => 'id']],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['lastmodifby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['lastmodifby' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterProducts::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => TransShippingprovider::className(), 'targetAttribute' => ['provider_id' => 'id']],
            [['shippingcost_id'], 'exist', 'skipOnError' => true, 'targetClass' => TransShippingcost::className(), 'targetAttribute' => ['shippingcost_id' => 'id']],
            [['product_varians_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterProductVarians::className(), 'targetAttribute' => ['product_varians_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'so_id' => Yii::t('language', 'So ID'),
            'product_id' => Yii::t('language', 'Product ID'),
            'product_name' => Yii::t('language', 'Product Name'),
            'product_varians_id' => Yii::t('language', 'Product Varians ID'),
            'product_varians_name' => Yii::t('language', 'Product Varians Name'),
            'product_price' => Yii::t('language', 'Product Price'),
            'product_quantity' => Yii::t('language', 'Product Quantity'),
            'provider_id' => Yii::t('language', 'Provider ID'),
            'provider_name' => Yii::t('language', 'Provider Name'),
            'shippingcost_id' => Yii::t('language', 'Shippingcost ID'),
            'shippingcost' => Yii::t('language', 'Shippingcost'),
            'user_address_id' => Yii::t('language', 'User Address ID'),
            'address_name' => Yii::t('language', 'Address Name'),
            'address_line1' => Yii::t('language', 'Address Line1'),
            'address_line2' => Yii::t('language', 'Address Line2'),
            'address_country' => Yii::t('language', 'Address Country'),
            'address_province' => Yii::t('language', 'Address Province'),
            'address_countytown' => Yii::t('language', 'Address Countytown'),
            'address_districts' => Yii::t('language', 'Address Districts'),
            'address_postalzip' => Yii::t('language', 'Address Postalzip'),
            'cart_remarks' => Yii::t('language', 'Cart Remarks'),
            'details_total' => Yii::t('language', 'Details Total'),
            'details_statuscode' => Yii::t('language', 'Details Statuscode'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSo()
    {
        return $this->hasOne(TransSo::className(), ['id' => 'so_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAddress()
    {
        return $this->hasOne(CustUsersAddress::className(), ['id' => 'user_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(MasterProducts::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(TransShippingprovider::className(), ['id' => 'provider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShippingcost0()
    {
        return $this->hasOne(TransShippingcost::className(), ['id' => 'shippingcost_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVarians()
    {
        return $this->hasOne(MasterProductVarians::className(), ['id' => 'product_varians_id']);
    }
}
