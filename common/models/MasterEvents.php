<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "master_events".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $page_id
 * @property string $event_name
 * @property string $event_title
 * @property string $event_content
 * @property string $event_pict
 * @property string $event_alt
 * @property integer $isactive
 * @property integer $sort_no
 * @property string $event_date
 * @property string $effective_from
 * @property string $effective_till
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustUsers $createdby0
 * @property MasterCustomer $customer
 * @property CustUsers $lastmodifby0
 * @property MasterPages $page
 * @property MasterEventsPictures[] $masterEventsPictures
 */
class MasterEvents extends \yii\db\ActiveRecord
{
    public $getfile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_events';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
            [['customer_id', 'page_id', 'isactive', 'sort_no', 'status'], 'integer'],
            [['event_date', 'effective_from', 'effective_till'], 'safe'],
            [['event_name', 'event_title', 'event_pict'], 'string', 'max' => 100],
            [['event_alt'], 'string', 'max' => 500],
            [['event_content'], 'string', 'max' => 5000],
            [['getfile'],'safe'],
            ['getfile',  'file', 'maxSize'=>'5000000', 'skipOnEmpty' => true, 'maxFiles' => 6],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterPages::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_id' => Yii::t('language', 'Customer ID'),
            'page_id' => Yii::t('language', 'Page ID'),
            'event_name' => Yii::t('language', 'Event Name'),
            'event_title' => Yii::t('language', 'Event Title'),
            'event_content' => Yii::t('language', 'Event Content'),
            'event_pict' => Yii::t('language', 'Event Pict'),
            'event_alt' => Yii::t('language', 'Event Alt'),
            'isactive' => Yii::t('language', 'Isactive'),
            'sort_no' => Yii::t('language', 'Sort No'),
            'getfile'=>Yii::t('language', 'Picture'),
            'event_date' => Yii::t('language', 'Event Date'),
            'effective_from' => Yii::t('language', 'Effective From'),
            'effective_till' => Yii::t('language', 'Effective Till'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(MasterPages::className(), ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterEventsPictures()
    {
        return $this->hasMany(MasterEventsPictures::className(), ['event_id' => 'id']);
    }
}
