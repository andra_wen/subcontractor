<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterProductVarians;

/**
 * MasterProductVariansSearch represents the model behind the search form about `common\models\MasterProductVarians`.
 */
class MasterProductVariansSearch extends MasterProductVarians
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'quantity', 'sort_no', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['varian_name', 'varian_value', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterProductVarians::find();

        //modification for join 2 table
        $query->joinWith(['product']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['product_id'] = [
            'asc' => ['master_products.product_name' => SORT_ASC],
            'desc' => ['master_products.product_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'quantity' => $this->quantity,
            'sort_no' => $this->sort_no,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'varian_name', $this->varian_name])
            ->andFilterWhere(['like', 'master_products.product_name', $this->product_id])
            ->andFilterWhere(['like', 'varian_value', $this->varian_value]);

        return $dataProvider;
    }
}
