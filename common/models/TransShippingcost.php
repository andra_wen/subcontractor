<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "trans_shippingcost".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $shippingprovider_id
 * @property integer $shipping_country
 * @property integer $shipping_province
 * @property integer $shipping_countytown
 * @property integer $shipping_districts
 * @property string $shipping_note
 * @property integer $shipping_prices
 * @property integer $shipping_minweight
 * @property integer $shipping_minweightprices
 * @property integer $shipping_minwegithnextprice
 * @property integer $shipping_maxweight
 * @property integer $isactive
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property TransCarts[] $transCarts
 * @property SysCountry $shippingCountry
 * @property SysCountytown $shippingCountytown
 * @property MasterCustomer $customer
 * @property SysDistricts $shippingDistricts
 * @property TransShippingprovider $shippingprovider
 * @property SysProvince $shippingProvince
 * @property TransSoDetails[] $transSoDetails
 */
class TransShippingcost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trans_shippingcost';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'shippingprovider_id', 'shipping_country', 'shipping_province', 'shipping_countytown', 'shipping_districts'], 'required'],
            [['customer_id', 'shippingprovider_id', 'shipping_country', 'shipping_province', 'shipping_countytown', 'shipping_districts', 'shipping_prices', 'shipping_minweight', 'shipping_minweightprices', 'shipping_minwegithnextprice', 'shipping_maxweight', 'isactive', 'status'], 'integer'],
            [['shipping_note'], 'string', 'max' => 100],
            [['shipping_country'], 'exist', 'skipOnError' => true, 'targetClass' => SysCountry::className(), 'targetAttribute' => ['shipping_country' => 'id']],
            [['shipping_countytown'], 'exist', 'skipOnError' => true, 'targetClass' => SysCountytown::className(), 'targetAttribute' => ['shipping_countytown' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['shipping_districts'], 'exist', 'skipOnError' => true, 'targetClass' => SysDistricts::className(), 'targetAttribute' => ['shipping_districts' => 'id']],
            [['shippingprovider_id'], 'exist', 'skipOnError' => true, 'targetClass' => TransShippingprovider::className(), 'targetAttribute' => ['shippingprovider_id' => 'id']],
            [['shipping_province'], 'exist', 'skipOnError' => true, 'targetClass' => SysProvince::className(), 'targetAttribute' => ['shipping_province' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_id' => Yii::t('language', 'Customer ID'),
            'shippingprovider_id' => Yii::t('language', 'Shippingprovider ID'),
            'shipping_country' => Yii::t('language', 'Shipping Country'),
            'shipping_province' => Yii::t('language', 'Shipping Province'),
            'shipping_countytown' => Yii::t('language', 'Shipping Countytown'),
            'shipping_districts' => Yii::t('language', 'Shipping Districts'),
            'shipping_note' => Yii::t('language', 'Shipping Note'),
            'shipping_prices' => Yii::t('language', 'Shipping Prices'),
            'shipping_minweight' => Yii::t('language', 'Shipping Minweight'),
            'shipping_minweightprices' => Yii::t('language', 'Shipping Minweightprices'),
            'shipping_minwegithnextprice' => Yii::t('language', 'Shipping Minwegithnextprice'),
            'shipping_maxweight' => Yii::t('language', 'Shipping Maxweight'),
            'isactive' => Yii::t('language', 'Isactive'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransCarts()
    {
        return $this->hasMany(TransCarts::className(), ['shippingcost_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShippingCountry()
    {
        return $this->hasOne(SysCountry::className(), ['id' => 'shipping_country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShippingCountytown()
    {
        return $this->hasOne(SysCountytown::className(), ['id' => 'shipping_countytown']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShippingDistricts()
    {
        return $this->hasOne(SysDistricts::className(), ['id' => 'shipping_districts']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShippingprovider()
    {
        return $this->hasOne(TransShippingprovider::className(), ['id' => 'shippingprovider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShippingProvince()
    {
        return $this->hasOne(SysProvince::className(), ['id' => 'shipping_province']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransSoDetails()
    {
        return $this->hasMany(TransSoDetails::className(), ['shippingcost_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }
}
