<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cust_client_profiles".
 *
 * @property integer $id
 * @property integer $cust_client_id
 * @property string $user_firstname
 * @property string $user_middlename
 * @property string $user_lastname
 * @property string $placeofbirth
 * @property string $dateofbirth
 * @property string $gender
 * @property string $marriagestatus
 * @property integer $language
 * @property string $user_picture
 *
 * @property CustClients $custClient
 */
class CustClientProfiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cust_client_profiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cust_client_id', 'user_firstname', 'user_middlename', 'user_lastname', 'language'], 'required'],
            [['cust_client_id', 'language'], 'integer'],
            [['dateofbirth'], 'safe'],
            [['user_firstname', 'user_middlename', 'user_lastname', 'placeofbirth'], 'string', 'max' => 150],
            [['gender'], 'string', 'max' => 1],
            [['marriagestatus'], 'string', 'max' => 15],
            [['user_picture'], 'string', 'max' => 200],
            [['cust_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustClients::className(), 'targetAttribute' => ['cust_client_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'cust_client_id' => Yii::t('language', 'Cust Client ID'),
            'user_firstname' => Yii::t('language', 'User Firstname'),
            'user_middlename' => Yii::t('language', 'User Middlename'),
            'user_lastname' => Yii::t('language', 'User Lastname'),
            'placeofbirth' => Yii::t('language', 'Placeofbirth'),
            'dateofbirth' => Yii::t('language', 'Dateofbirth'),
            'gender' => Yii::t('language', 'Gender'),
            'marriagestatus' => Yii::t('language', 'Marriagestatus'),
            'language' => Yii::t('language', 'Language'),
            'user_picture' => Yii::t('language', 'User Picture'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustClient()
    {
        return $this->hasOne(CustClients::className(), ['id' => 'cust_client_id']);
    }
}
