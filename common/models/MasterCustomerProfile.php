<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "master_customer_profile".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $customer_name
 * @property string $customer_desc
 * @property integer $customer_country
 * @property string $customer_contactperson
 * @property string $customer_contactno
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustUsers $createdby0
 * @property MasterCustomer $customer
 * @property CustUsers $lastmodifby0
 */
class MasterCustomerProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_customer_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'customer_name', 'createdby', 'createdon', 'lastmodifby', 'lastmodif'], 'required'],
            [['customer_id', 'customer_country', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['createdon', 'lastmodif'], 'safe'],
            [['customer_name', 'customer_contactperson'], 'string', 'max' => 200],
            [['customer_desc'], 'string', 'max' => 400],
            [['customer_contactno'], 'string', 'max' => 17],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['lastmodifby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['lastmodifby' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_id' => Yii::t('language', 'Customer ID'),
            'customer_name' => Yii::t('language', 'Customer Name'),
            'customer_desc' => Yii::t('language', 'Customer Desc'),
            'customer_country' => Yii::t('language', 'Customer Country'),
            'customer_contactperson' => Yii::t('language', 'Customer Contactperson'),
            'customer_contactno' => Yii::t('language', 'Customer Contactno'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }
}
