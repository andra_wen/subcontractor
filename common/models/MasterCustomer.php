<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "master_customer".
 *
 * @property integer $id
 * @property string $customer_name
 * @property string $customer_desc
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustBanks[] $custBanks
 * @property CustClients[] $custClients
 * @property CustLanguage[] $custLanguages
 * @property CustRoles[] $custRoles
 * @property CustUsers[] $custUsers
 * @property MasterCategory[] $masterCategories
 * @property MasterCustomerProfile[] $masterCustomerProfiles
 * @property MasterEvents[] $masterEvents
 * @property MasterNews[] $masterNews
 * @property MasterPages[] $masterPages
 * @property MasterProducts[] $masterProducts
 * @property MasterSliders[] $masterSliders
 * @property SysSeqPattern[] $sysSeqPatterns
 * @property TransAdjustments[] $transAdjustments
 * @property TransCarts[] $transCarts
 * @property TransPaymentmethod[] $transPaymentmethods
 * @property TransPaymentmethodbank[] $transPaymentmethodbanks
 * @property TransShippingcost[] $transShippingcosts
 * @property TransShippingprovider[] $transShippingproviders
 * @property TransSo[] $transSos
 * @property TransStatuscode[] $transStatuscodes
 * @property TransWishlist[] $transWishlists
 */
class MasterCustomer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    //public $user;
    public static function tableName()
    {
        return 'master_customer';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_name' => Yii::t('language', 'Customer Name'),
            'customer_desc' => Yii::t('language', 'Customer Desc'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_name'], 'required'],
            [['createdby', 'lastmodifby', 'status'], 'integer'],
            [['createdon', 'lastmodif'], 'safe'],
            [['customer_name'], 'string', 'max' => 100],
            [['customer_desc'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    /**
     * @return \yii\db\ActiveQuery
     */

    public function getCustBanks()
    {
        return $this->hasMany(CustBanks::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustClients()
    {
        return $this->hasMany(CustClients::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustLanguages()
    {
        return $this->hasMany(CustLanguage::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustRoles()
    {
        return $this->hasMany(CustRoles::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustUsers()
    {
        return $this->hasOne(CustUsers::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterCategories()
    {
        return $this->hasMany(MasterCategory::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterCustomerProfiles()
    {
        return $this->hasMany(MasterCustomerProfile::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterEvents()
    {
        return $this->hasMany(MasterEvents::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterNews()
    {
        return $this->hasMany(MasterNews::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterPages()
    {
        return $this->hasMany(MasterPages::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProducts()
    {
        return $this->hasMany(MasterProducts::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterSliders()
    {
        return $this->hasMany(MasterSliders::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysSeqPatterns()
    {
        return $this->hasMany(SysSeqPattern::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransAdjustments()
    {
        return $this->hasMany(TransAdjustments::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransCarts()
    {
        return $this->hasMany(TransCarts::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransPaymentmethods()
    {
        return $this->hasMany(TransPaymentmethod::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransPaymentmethodbanks()
    {
        return $this->hasMany(TransPaymentmethodbank::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransShippingcosts()
    {
        return $this->hasMany(TransShippingcost::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransShippingproviders()
    {
        return $this->hasMany(TransShippingprovider::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransSos()
    {
        return $this->hasMany(TransSo::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransStatuscodes()
    {
        return $this->hasMany(TransStatuscode::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransWishlists()
    {
        return $this->hasMany(TransWishlist::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustCreated()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby'])->from(['custCreated' => CustUsers::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustLastmodif()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby'])->from(['custLastmodif' => CustUsers::tableName()]);
    }
}
