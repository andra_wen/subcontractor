<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sys_uom".
 *
 * @property integer $id
 * @property string $code
 * @property string $uom_name
 * @property string $uom_description
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property MasterProductProfiles[] $masterProductProfiles
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 */
class SysUom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys_uom';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'createdby', 'createdon', 'lastmodifby', 'lastmodif'], 'required'],
            [['createdby', 'lastmodifby', 'status'], 'integer'],
            [['createdon', 'lastmodif'], 'safe'],
            [['code'], 'string', 'max' => 10],
            [['uom_name'], 'string', 'max' => 30],
            [['uom_description'], 'string', 'max' => 50],
            [['code'], 'unique'],
            [['uom_name'], 'unique'],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['lastmodifby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['lastmodifby' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'code' => Yii::t('language', 'Code'),
            'uom_name' => Yii::t('language', 'Uom Name'),
            'uom_description' => Yii::t('language', 'Uom Description'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProductProfiles()
    {
        return $this->hasMany(MasterProductProfiles::className(), ['uom_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }
}
