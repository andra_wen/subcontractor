<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CustUsersProduct;

/**
 * CustUserProductSearch represents the model behind the search form about `common\models\CustUserProduct`.
 */
class CustUsersProductSearch extends CustUsersProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cust_user_id', 'product_id', 'createdby', 'lastmodifby'], 'integer'],
            [['createdon', 'lastmodifon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustUsersProduct::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'cust_user_id' => $this->cust_user_id,
            'product_id' => $this->product_id,
            'createdon' => $this->createdon,
            'lastmodifon' => $this->lastmodifon,
            'createdby' => $this->createdby,
            'lastmodifby' => $this->lastmodifby,
        ]);

        return $dataProvider;
    }
}
