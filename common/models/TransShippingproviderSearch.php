<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TransShippingprovider;

/**
 * TransShippingproviderSearch represents the model behind the search form about `common\models\TransShippingprovider`.
 */
class TransShippingproviderSearch extends TransShippingprovider
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'isactive', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['shipping_code', 'shipping_name', 'shipping_desc', 'shipping_pict', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransShippingprovider::find();

        //modification for join 2 table
        $query->joinWith(['customer']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['customer_id'] = [
            'asc' => ['master_customer.customer_name' => SORT_ASC],
            'desc' => ['master_customer.customer_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'isactive' => $this->isactive,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'shipping_code', $this->shipping_code])
            ->andFilterWhere(['like', 'shipping_name', $this->shipping_name])
            ->andFilterWhere(['like', 'shipping_desc', $this->shipping_desc])
            ->andFilterWhere(['like', 'master_customer.customer_name', $this->customer_id])
            ->andFilterWhere(['like', 'shipping_pict', $this->shipping_pict]);

        return $dataProvider;
    }
}
