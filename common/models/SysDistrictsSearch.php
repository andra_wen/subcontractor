<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SysDistricts;

/**
 * SysDistrictsSearch represents the model behind the search form about `common\models\SysDistricts`.
 */
class SysDistrictsSearch extends SysDistricts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country_id', 'province_id', 'countytown_id', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['name', 'iso_code', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SysDistricts::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'country_id' => $this->country_id,
            'province_id' => $this->province_id,
            'countytown_id' => $this->countytown_id,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'iso_code', $this->iso_code]);

        return $dataProvider;
    }
}
