<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "master_files".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $page_id
 * @property string $file_name
 * @property string $file_title
 * @property string $file_desc
 * @property string $file_pict
 * @property string $the_files
 * @property string $file_alt
 * @property integer $isactive
 * @property integer $sort_no
 * @property string $file_date
 * @property string $effective_from
 * @property string $effective_till
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustUsers $createdby0
 * @property MasterCustomer $customer
 * @property CustUsers $lastmodifby0
 * @property MasterPages $page
 */
class MasterFiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_files';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
            [['customer_id', 'page_id', 'isactive', 'sort_no', 'status'], 'integer'],
            [['file_date', 'effective_from', 'effective_till', 'createdon', 'lastmodif'], 'safe'],
            [['file_name', 'file_title', 'file_pict', 'the_files', 'file_alt'], 'string', 'max' => 100],
            [['file_desc'], 'string', 'max' => 500],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterPages::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_id' => Yii::t('language', 'Customer ID'),
            'page_id' => Yii::t('language', 'Page ID'),
            'file_name' => Yii::t('language', 'File Name'),
            'file_title' => Yii::t('language', 'File Title'),
            'file_desc' => Yii::t('language', 'File Desc'),
            'file_pict' => Yii::t('language', 'File Pict'),
            'the_files' => Yii::t('language', 'The Files'),
            'file_alt' => Yii::t('language', 'File Alt'),
            'isactive' => Yii::t('language', 'Isactive'),
            'sort_no' => Yii::t('language', 'Sort No'),
            'file_date' => Yii::t('language', 'File Date'),
            'effective_from' => Yii::t('language', 'Effective From'),
            'effective_till' => Yii::t('language', 'Effective Till'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(MasterPages::className(), ['id' => 'page_id']);
    }
}
