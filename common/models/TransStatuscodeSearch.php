<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TransStatuscode;

/**
 * TransStatuscodeSearch represents the model behind the search form about `common\models\TransStatuscode`.
 */
class TransStatuscodeSearch extends TransStatuscode
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'trans_value', 'isactive', 'sort_no', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['trans_code', 'trans_name', 'trans_description', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransStatuscode::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'trans_value' => $this->trans_value,
            'isactive' => $this->isactive,
            'sort_no' => $this->sort_no,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'trans_code', $this->trans_code])
            ->andFilterWhere(['like', 'trans_name', $this->trans_name])
            ->andFilterWhere(['like', 'trans_description', $this->trans_description]);

        return $dataProvider;
    }
}
