<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterProductProfiles;

/**
 * MasterProductProfilesSearch represents the model behind the search form about `common\models\MasterProductProfiles`.
 */
class MasterProductProfilesSearch extends MasterProductProfiles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'uom_id', 'alias', 'shelflife', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['barcode', 'manufacturing_date', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterProductProfiles::find();

        //modification for join 2 table
        $query->joinWith(['product']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'product_id' => $this->product_id,
            'uom_id' => $this->uom_id,
            'alias' => $this->alias,
            'manufacturing_date' => $this->manufacturing_date,
            'shelflife' => $this->shelflife,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'barcode', $this->barcode]);

        return $dataProvider;
    }
}
