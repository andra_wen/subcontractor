<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "cust_users_culture".
 *
 * @property integer $id
 * @property integer $cust_user_id
 * @property string $themes_color
 * @property integer $language
 * @property integer $country
 * @property string $dateFormat
 * @property integer $user_currency
 * @property string $user_picture
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 * @property CustUsers $custUser
 */
class CustUsersCulture extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cust_users_culture';
    }

    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    public function rules()
    {
        return [
            [['cust_user_id', 'themes_color', 'language'], 'required'],
            [['cust_user_id', 'country', 'user_currency'], 'integer'],
            [['themes_color'], 'string', 'max' => 100],
            [['dateFormat'], 'string', 'max' => 30],
            [['language'], 'string', 'max'=> 5],
            [['user_picture'], 'string', 'max' => 300],
            [['cust_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['cust_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'cust_user_id' => Yii::t('language', 'Cust User ID'),
            'themes_color' => Yii::t('language', 'Themes Color'),
            'language' => Yii::t('language', 'Language'),
            'country' => Yii::t('language', 'Country'),
            'dateFormat' => Yii::t('language', 'Date Format'),
            'user_currency' => Yii::t('language', 'User Currency'),
            'user_picture' => Yii::t('language', 'User Picture'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustUser()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'cust_user_id']);
    }

    public static function getCustculturebyuserid($user_id)
    {
        $data = \common\models\CustUsersCulture::find()->where(['cust_user_id'=>$user_id])->one();

        return $data;
    }
}
