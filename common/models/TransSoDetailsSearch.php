<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TransSoDetails;

/**
 * TransSoDetailsSearch represents the model behind the search form about `common\models\TransSoDetails`.
 */
class TransSoDetailsSearch extends TransSoDetails
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'so_id', 'product_id', 'product_varians_id', 'product_price', 'product_quantity', 'provider_id', 'shippingcost_id', 'shippingcost', 'user_address_id', 'address_postalzip', 'details_total', 'details_statuscode', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['product_name', 'product_varians_name', 'provider_name', 'address_name', 'address_line1', 'address_line2', 'address_country', 'address_province', 'address_countytown', 'address_districts', 'cart_remarks', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransSoDetails::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'so_id' => $this->so_id,
            'product_id' => $this->product_id,
            'product_varians_id' => $this->product_varians_id,
            'product_price' => $this->product_price,
            'product_quantity' => $this->product_quantity,
            'provider_id' => $this->provider_id,
            'shippingcost_id' => $this->shippingcost_id,
            'shippingcost' => $this->shippingcost,
            'user_address_id' => $this->user_address_id,
            'address_postalzip' => $this->address_postalzip,
            'details_total' => $this->details_total,
            'details_statuscode' => $this->details_statuscode,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'product_varians_name', $this->product_varians_name])
            ->andFilterWhere(['like', 'provider_name', $this->provider_name])
            ->andFilterWhere(['like', 'address_name', $this->address_name])
            ->andFilterWhere(['like', 'address_line1', $this->address_line1])
            ->andFilterWhere(['like', 'address_line2', $this->address_line2])
            ->andFilterWhere(['like', 'address_country', $this->address_country])
            ->andFilterWhere(['like', 'address_province', $this->address_province])
            ->andFilterWhere(['like', 'address_countytown', $this->address_countytown])
            ->andFilterWhere(['like', 'address_districts', $this->address_districts])
            ->andFilterWhere(['like', 'cart_remarks', $this->cart_remarks]);

        return $dataProvider;
    }
}
