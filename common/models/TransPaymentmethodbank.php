<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "trans_paymentmethodbank".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $paymentmethod_id
 * @property integer $cust_bank_account_id
 * @property integer $isactive
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustBankAccounts $custBankAccount
 * @property CustUsers $createdby0
 * @property MasterCustomer $customer
 * @property CustUsers $lastmodifby0
 * @property TransPaymentmethod $paymentmethod
 */
class TransPaymentmethodbank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trans_paymentmethodbank';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'paymentmethod_id', 'cust_bank_account_id'], 'required'],
            [['customer_id', 'paymentmethod_id', 'cust_bank_account_id', 'isactive', 'status'], 'integer'],
            [['cust_bank_account_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustBankAccounts::className(), 'targetAttribute' => ['cust_bank_account_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['paymentmethod_id'], 'exist', 'skipOnError' => true, 'targetClass' => TransPaymentmethod::className(), 'targetAttribute' => ['paymentmethod_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_id' => Yii::t('language', 'Customer ID'),
            'paymentmethod_id' => Yii::t('language', 'Paymentmethod ID'),
            'cust_bank_account_id' => Yii::t('language', 'Cust Bank Account ID'),
            'isactive' => Yii::t('language', 'Isactive'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustBankAccount()
    {
        return $this->hasOne(CustBankAccounts::className(), ['id' => 'cust_bank_account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentmethod()
    {
        return $this->hasOne(TransPaymentmethod::className(), ['id' => 'paymentmethod_id']);
    }

    public static function getBankAccountByPaymentMethod($paymentmethod)
    {
        $id = \common\models\TransPaymentmethodbank::find()->select(['cust_bank_account_id'])->where(['paymentmethod_id'=>$paymentmethod])->asArray()->all();
        $bank_accounts=[];
        foreach($id as $key => $ids)
        {
            $bank_accounts[] = $ids['cust_bank_account_id'];
        }

        $data = \common\models\CustBankAccounts::find()
            ->where(['id' => $bank_accounts])
            ->select(['id', 'bank_accountname as name'])->asArray()->all();

        return $data;
    }

}
