<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sys_country".
 *
 * @property integer $id
 * @property string $code
 * @property string $short_name
 * @property string $long_name
 * @property string $iso_code
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustAddress[] $custAddresses
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 * @property SysProvince[] $sysProvinces
 */
class SysCountry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys_country';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'short_name'], 'required'],
            [['status'], 'integer'],
            [['code'], 'string', 'max' => 10],
            [['short_name'], 'string', 'max' => 100],
            [['long_name'], 'string', 'max' => 200],
            [['iso_code'], 'string', 'max' => 50],
            [['code'], 'unique'],
            [['short_name'], 'unique'],
            [['iso_code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'code' => Yii::t('language', 'Code'),
            'short_name' => Yii::t('language', 'Short Name'),
            'long_name' => Yii::t('language', 'Long Name'),
            'iso_code' => Yii::t('language', 'Iso Code'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustAddresses()
    {
        return $this->hasMany(CustAddress::className(), ['address_country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysProvinces()
    {
        return $this->hasMany(SysProvince::className(), ['country_id' => 'id']);
    }
	
	public static function getCountry()
	{
		$data = SysCountry::find()->select(['id','short_name as name'])->asArray()->orderBy([
				  'short_name' => SORT_ASC
				])->all();
		return ArrayHelper::map($data,'id','name');
	}
}
