<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TransSoConfirmation;

/**
 * TransSoConfirmationSearch represents the model behind the search form about `common\models\TransSoConfirmation`.
 */
class TransSoConfirmationSearch extends TransSoConfirmation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'so_id', 'paymentmethod_id', 'total_amount', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['so_no', 'paymentmethod_name', 'account_no', 'account_holder', 'phone_number', 'payment_date', 'confirm_remarks', 'confirm_attachment', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransSoConfirmation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'so_id' => $this->so_id,
            'paymentmethod_id' => $this->paymentmethod_id,
            'total_amount' => $this->total_amount,
            'payment_date' => $this->payment_date,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'so_no', $this->so_no])
            ->andFilterWhere(['like', 'paymentmethod_name', $this->paymentmethod_name])
            ->andFilterWhere(['like', 'account_no', $this->account_no])
            ->andFilterWhere(['like', 'account_holder', $this->account_holder])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'confirm_remarks', $this->confirm_remarks])
            ->andFilterWhere(['like', 'confirm_attachment', $this->confirm_attachment]);

        return $dataProvider;
    }
}
