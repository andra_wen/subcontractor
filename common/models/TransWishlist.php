<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "trans_wishlist".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $cust_client_id
 * @property string $wishlist_cat
 * @property integer $product_id
 * @property integer $product_varians_id
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustClients $custClient
 * @property CustUsers $createdby0
 * @property MasterCustomer $customer
 * @property CustUsers $lastmodifby0
 * @property MasterProducts $product
 * @property MasterProductVarians $productVarians
 */
class TransWishlist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trans_wishlist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'cust_client_id', 'product_id', 'createdby', 'createdon', 'lastmodifby', 'lastmodif'], 'required'],
            [['customer_id', 'cust_client_id', 'product_id', 'product_varians_id', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['createdon', 'lastmodif'], 'safe'],
            [['wishlist_cat'], 'string', 'max' => 100],
            [['cust_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustClients::className(), 'targetAttribute' => ['cust_client_id' => 'id']],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['lastmodifby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['lastmodifby' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterProducts::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['product_varians_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterProductVarians::className(), 'targetAttribute' => ['product_varians_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_id' => Yii::t('language', 'Customer ID'),
            'cust_client_id' => Yii::t('language', 'Cust Client ID'),
            'wishlist_cat' => Yii::t('language', 'Wishlist Cat'),
            'product_id' => Yii::t('language', 'Product ID'),
            'product_varians_id' => Yii::t('language', 'Product Varians ID'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustClient()
    {
        return $this->hasOne(CustClients::className(), ['id' => 'cust_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(MasterProducts::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVarians()
    {
        return $this->hasOne(MasterProductVarians::className(), ['id' => 'product_varians_id']);
    }
}
