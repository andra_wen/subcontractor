<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "master_product_events".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $event_name
 * @property string $event_caption
 * @property string $event_label
 * @property string $event_stringvalue
 * @property integer $event_numbervalue
 * @property string $effective_from
 * @property string $effective_till
 * @property integer $isactive
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 * @property MasterProducts $product
 */
class MasterProductEvents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_product_events';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'event_name'], 'required'],
            [['product_id', 'event_numbervalue', 'isactive', 'status'], 'integer'],
            [['effective_from', 'effective_till'], 'safe'],
            [['event_name'], 'string', 'max' => 50],
            [['event_caption', 'event_label', 'event_stringvalue'], 'string', 'max' => 100],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterProducts::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'product_id' => Yii::t('language', 'Product ID'),
            'event_name' => Yii::t('language', 'Event Name'),
            'event_caption' => Yii::t('language', 'Event Caption'),
            'event_label' => Yii::t('language', 'Event Label'),
            'event_stringvalue' => Yii::t('language', 'Event Stringvalue'),
            'event_numbervalue' => Yii::t('language', 'Event Numbervalue'),
            'effective_from' => Yii::t('language', 'Effective From'),
            'effective_till' => Yii::t('language', 'Effective Till'),
            'isactive' => Yii::t('language', 'Isactive'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(MasterProducts::className(), ['id' => 'product_id']);
    }
}
