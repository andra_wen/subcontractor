<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterEventsPictures;

/**
 * MasterEventsPicturesSearch represents the model behind the search form about `common\models\MasterEventsPictures`.
 */
class MasterEventsPicturesSearch extends MasterEventsPictures
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'event_id', 'sort_no', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['picture_name', 'picture_path', 'picture_path_md', 'picture_path_sm', 'picture_path_xs', 'picture_path_thumbnail', 'picture_alt', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterEventsPictures::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'event_id' => $this->event_id,
            'sort_no' => $this->sort_no,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'picture_name', $this->picture_name])
            ->andFilterWhere(['like', 'picture_path', $this->picture_path])
            ->andFilterWhere(['like', 'picture_path_md', $this->picture_path_md])
            ->andFilterWhere(['like', 'picture_path_sm', $this->picture_path_sm])
            ->andFilterWhere(['like', 'picture_path_xs', $this->picture_path_xs])
            ->andFilterWhere(['like', 'picture_path_thumbnail', $this->picture_path_thumbnail])
            ->andFilterWhere(['like', 'picture_alt', $this->picture_alt]);

        return $dataProvider;
    }
}
