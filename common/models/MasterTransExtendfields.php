<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "master_trans_extendfields".
 *
 * @property integer $id
 * @property string $table_name
 * @property string $table_fields
 * @property integer $table_fields_ref
 * @property string $field_label
 * @property string $field_value
 * @property integer $field_language
 *
 * @property SysLanguage $fieldLanguage
 */
class MasterTransExtendfields extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_trans_extendfields';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table_name', 'field_label', 'field_language'], 'required'],
            [['table_fields_ref', 'field_language'], 'integer'],
            [['table_name', 'table_fields'], 'string', 'max' => 50],
            [['field_label'], 'string', 'max' => 100],
            [['field_value'], 'string', 'max' => 200],
            [['field_language'], 'exist', 'skipOnError' => true, 'targetClass' => SysLanguage::className(), 'targetAttribute' => ['field_language' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'table_name' => Yii::t('language', 'Table Name'),
            'table_fields' => Yii::t('language', 'Table Fields'),
            'table_fields_ref' => Yii::t('language', 'Table Fields Ref'),
            'field_label' => Yii::t('language', 'Field Label'),
            'field_value' => Yii::t('language', 'Field Value'),
            'field_language' => Yii::t('language', 'Field Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFieldLanguage()
    {
        return $this->hasOne(SysLanguage::className(), ['id' => 'field_language']);
    }
}
