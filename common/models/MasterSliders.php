<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "master_sliders".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $page_id
 * @property string $slider_name
 * @property string $slider_title
 * @property string $slider_pict
 * @property string $slider_alt
 * @property integer $isactive
 * @property integer $sort_no
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustUsers $createdby0
 * @property MasterCustomer $customer
 * @property CustUsers $lastmodifby0
 * @property MasterPages $page
 */
class MasterSliders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_sliders';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id','page_id','status'], 'required'],
            [['customer_id', 'page_id', 'isactive', 'sort_no', 'status'], 'integer'],
            [['slider_pict'], 'safe'],
            [['slider_name', 'slider_title', 'slider_alt'], 'string', 'max' => 100],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterPages::className(), 'targetAttribute' => ['page_id' => 'id']],
            [['slider_pict'], 'file', 'extensions'=>'jpg, gif, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_id' => Yii::t('language', 'Customer ID'),
            'page_id' => Yii::t('language', 'Page ID'),
            'slider_name' => Yii::t('language', 'Slider Name'),
            'slider_title' => Yii::t('language', 'Slider Title'),
            'slider_pict' => Yii::t('language', 'Slider Pict'),
            'slider_alt' => Yii::t('language', 'Slider Alt'),
            'isactive' => Yii::t('language', 'Isactive'),
            'sort_no' => Yii::t('language', 'Sort No'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(MasterPages::className(), ['id' => 'page_id']);
    }
}
