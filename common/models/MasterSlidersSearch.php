<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterSliders;

/**
 * MasterSlidersSearch represents the model behind the search form about `common\models\MasterSliders`.
 */
class MasterSlidersSearch extends MasterSliders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'page_id', 'isactive', 'sort_no', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['slider_name', 'slider_title', 'slider_pict', 'slider_alt', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterSliders::find();

        //modification for join 2 table
        $query->joinWith(['customer','page']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['customer_id'] = [
            'asc' => ['master_customer.customer_name' => SORT_ASC],
            'desc' => ['master_customer.customer_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['page_id'] = [
            'asc' => ['master_pages.page_name' => SORT_ASC],
            'desc' => ['master_pages.page_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'isactive' => $this->isactive,
            'sort_no' => $this->sort_no,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'slider_name', $this->slider_name])
            ->andFilterWhere(['like', 'slider_title', $this->slider_title])
            ->andFilterWhere(['like', 'slider_pict', $this->slider_pict])
            ->andFilterWhere(['like', 'master_customer.customer_name', $this->customer_id])
            ->andFilterWhere(['like', 'master_pages.page_name', $this->page_id])
            ->andFilterWhere(['like', 'slider_alt', $this->slider_alt]);

        return $dataProvider;
    }
}
