<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CustUsersCulture;

/**
 * CustUsersCultureSearch represents the model behind the search form about `common\models\CustUsersCulture`.
 */
class CustUsersCultureSearch extends CustUsersCulture
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cust_user_id', 'language', 'country', 'user_currency', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['themes_color', 'dateFormat', 'user_picture', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustUsersCulture::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'cust_user_id' => $this->cust_user_id,
            'language' => $this->language,
            'country' => $this->country,
            'user_currency' => $this->user_currency,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'themes_color', $this->themes_color])
            ->andFilterWhere(['like', 'dateFormat', $this->dateFormat])
            ->andFilterWhere(['like', 'user_picture', $this->user_picture]);

        return $dataProvider;
    }
}
