<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SysCountry;

/**
 * SysCountrySearch represents the model behind the search form about `common\models\SysCountry`.
 */
class SysCountrySearch extends SysCountry
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['code', 'short_name', 'long_name', 'iso_code', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SysCountry::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'short_name', $this->short_name])
            ->andFilterWhere(['like', 'long_name', $this->long_name])
            ->andFilterWhere(['like', 'iso_code', $this->iso_code]);

        return $dataProvider;
    }
}
