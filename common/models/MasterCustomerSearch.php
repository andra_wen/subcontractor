<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterCustomer;

/**
 * MasterCustomerSearch represents the model behind the search form about `common\models\MasterCustomer`.
 */
class MasterCustomerSearch extends MasterCustomer
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lastmodifby', 'status'], 'integer'],
            [['customer_name', 'customer_desc', 'createdby', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterCustomer::find();

        //modification for join 2 table
        $query->joinWith(['custCreated','custLastmodif']);
        //$query->addSelect(['cust_users.username','master_customer.*']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['createdby'] = [
            'asc' => ['cust_users.username' => SORT_ASC],
            'desc' => ['cust_users.username' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['lastmodifby'] = [
            'asc' => ['cust_users.username' => SORT_ASC],
            'desc' => ['cust_users.username' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            //'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'master_customer.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'customer_name', $this->customer_name])
            ->andFilterWhere(['like', 'customer_desc', $this->customer_desc])
            ->andFilterWhere(['like', 'custCreated.username', $this->createdby])
            ->andFilterWhere(['like', 'custLastmodif.username', $this->lastmodifby]);

        return $dataProvider;
    }
}
