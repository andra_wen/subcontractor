<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "cust_users_address".
 *
 * @property integer $id
 * @property integer $cust_user_id
 * @property string $address_name
 * @property string $address_line1
 * @property string $address_line2
 * @property string $address_phonenumber
 * @property string $address_faxnumber
 * @property integer $address_country_id
 * @property integer $address_province_id
 * @property integer $address_countytown_id
 * @property integer $address_districts_id
 * @property integer $address_postalzip
 * @property integer $address_default
 * @property string $address_remarks
 * @property string $address_email
 * @property string $createdon
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustUsers $custUser
 * @property SysCountry $addressCountry
 * @property SysCountytown $addressCountytown
 * @property SysDistricts $addressDistricts
 * @property SysProvince $addressProvince
 * @property TransSoDetails[] $transSoDetails
 */
class CustUsersAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cust_users_address';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cust_user_id', 'address_line1', 'address_country_id', 'address_province_id'], 'required'],
            [['cust_user_id', 'address_country_id', 'address_province_id', 'address_countytown_id', 'address_districts_id', 'address_postalzip', 'address_default', 'status'], 'integer'],
            [['address_name'], 'string', 'max' => 50],
            [['address_line1', 'address_line2', 'address_remarks', 'address_email'], 'string', 'max' => 200],
            [['address_phonenumber', 'address_faxnumber'], 'string', 'max' => 20],
            [['cust_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['cust_user_id' => 'id']],
            [['address_country_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysCountry::className(), 'targetAttribute' => ['address_country_id' => 'id']],
            [['address_countytown_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysCountytown::className(), 'targetAttribute' => ['address_countytown_id' => 'id']],
            [['address_districts_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysDistricts::className(), 'targetAttribute' => ['address_districts_id' => 'id']],
            [['address_province_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysProvince::className(), 'targetAttribute' => ['address_province_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'cust_user_id' => Yii::t('language', 'Cust User ID'),
            'address_name' => Yii::t('language', 'Address Name'),
            'address_line1' => Yii::t('language', 'Address Line1'),
            'address_line2' => Yii::t('language', 'Address Line2'),
            'address_phonenumber' => Yii::t('language', 'Address Phonenumber'),
            'address_faxnumber' => Yii::t('language', 'Address Faxnumber'),
            'address_country_id' => Yii::t('language', 'Address Country ID'),
            'address_province_id' => Yii::t('language', 'Address Province ID'),
            'address_countytown_id' => Yii::t('language', 'Address Countytown ID'),
            'address_districts_id' => Yii::t('language', 'Address Districts ID'),
            'address_postalzip' => Yii::t('language', 'Address Postalzip'),
            'address_default' => Yii::t('language', 'Address Default'),
            'address_remarks' => Yii::t('language', 'Address Remarks'),
            'address_email' => Yii::t('language', 'Address Email'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustUser()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'cust_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressCountry()
    {
        return $this->hasOne(SysCountry::className(), ['id' => 'address_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressCountytown()
    {
        return $this->hasOne(SysCountytown::className(), ['id' => 'address_countytown_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressDistricts()
    {
        return $this->hasOne(SysDistricts::className(), ['id' => 'address_districts_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressProvince()
    {
        return $this->hasOne(SysProvince::className(), ['id' => 'address_province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransSoDetails()
    {
        return $this->hasMany(TransSoDetails::className(), ['user_address_id' => 'id']);
    }
}
