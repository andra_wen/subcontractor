<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "master_product_varians".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $varian_name
 * @property string $varian_value
 * @property integer $quantity
 * @property integer $sort_no
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 * @property MasterProducts $product
 * @property TransCarts[] $transCarts
 * @property TransSoDetails[] $transSoDetails
 * @property TransWishlist[] $transWishlists
 */
class MasterProductVarians extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_product_varians';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id'], 'required'],
            [['product_id', 'quantity', 'sort_no', 'status'], 'integer'],
            [['varian_name', 'varian_value'], 'string', 'max' => 50],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterProducts::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'product_id' => Yii::t('language', 'Product ID'),
            'varian_name' => Yii::t('language', 'Varian Name'),
            'varian_value' => Yii::t('language', 'Varian Value'),
            'quantity' => Yii::t('language', 'Quantity'),
            'sort_no' => Yii::t('language', 'Sort No'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(MasterProducts::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransCarts()
    {
        return $this->hasMany(TransCarts::className(), ['product_varians_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransSoDetails()
    {
        return $this->hasMany(TransSoDetails::className(), ['product_varians_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransWishlists()
    {
        return $this->hasMany(TransWishlist::className(), ['product_varians_id' => 'id']);
    }

    public static function getVariansByProductId($product_id)
    {
        $data = \common\models\MasterProductVarians::find()
            ->where(['product_id' => $product_id])
            ->select(['id', 'varian_name as name'])->asArray()->all();

        return $data;
    }

    public static function getVarianByProduct($product)
    {
        $data = \common\models\MasterProductVarians::find()
            ->where(['product_id' => $product])
            ->select(['id', 'varian_name'])->asArray()->all();

        return $data;
    }
}
