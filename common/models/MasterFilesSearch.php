<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterFiles;

/**
 * MasterFilesSearch represents the model behind the search form about `common\models\MasterFiles`.
 */
class MasterFilesSearch extends MasterFiles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'page_id', 'isactive', 'sort_no', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['file_name', 'file_title', 'file_desc', 'file_pict', 'the_files', 'file_alt', 'file_date', 'effective_from', 'effective_till', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterFiles::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'page_id' => $this->page_id,
            'isactive' => $this->isactive,
            'sort_no' => $this->sort_no,
            'file_date' => $this->file_date,
            'effective_from' => $this->effective_from,
            'effective_till' => $this->effective_till,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'file_name', $this->file_name])
            ->andFilterWhere(['like', 'file_title', $this->file_title])
            ->andFilterWhere(['like', 'file_desc', $this->file_desc])
            ->andFilterWhere(['like', 'file_pict', $this->file_pict])
            ->andFilterWhere(['like', 'the_files', $this->the_files])
            ->andFilterWhere(['like', 'file_alt', $this->file_alt]);

        return $dataProvider;
    }
}
