<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cust_address".
 *
 * @property integer $id
 * @property integer $cust_user_id
 * @property string $address_name
 * @property string $address_line1
 * @property string $address_line2
 * @property integer $address_country_id
 * @property integer $address_province_id
 * @property integer $address_countytown_id
 * @property integer $address_districts_id
 * @property integer $address_postalzip
 * @property string $address_remarks
 * @property integer $address_default
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property SysCountry $addressCountry
 * @property SysCountytown $addressCountytown
 * @property CustUsers $createdby0
 * @property SysDistricts $addressDistricts
 * @property CustUsers $lastmodifby0
 * @property SysProvince $addressProvince
 * @property CustUsers $custUser
 */
class CustAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cust_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cust_user_id', 'address_line1', 'address_country_id', 'address_province_id', 'createdby', 'createdon', 'lastmodifby', 'lastmodif'], 'required'],
            [['cust_user_id', 'address_country_id', 'address_province_id', 'address_countytown_id', 'address_districts_id', 'address_postalzip', 'address_default', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['createdon', 'lastmodif'], 'safe'],
            [['address_name'], 'string', 'max' => 50],
            [['address_line1', 'address_line2', 'address_remarks'], 'string', 'max' => 200],
            [['address_country_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysCountry::className(), 'targetAttribute' => ['address_country_id' => 'id']],
            [['address_countytown_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysCountytown::className(), 'targetAttribute' => ['address_countytown_id' => 'id']],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['address_districts_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysDistricts::className(), 'targetAttribute' => ['address_districts_id' => 'id']],
            [['lastmodifby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['lastmodifby' => 'id']],
            [['address_province_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysProvince::className(), 'targetAttribute' => ['address_province_id' => 'id']],
            [['cust_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['cust_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'cust_user_id' => Yii::t('language', 'Cust User ID'),
            'address_name' => Yii::t('language', 'Address Name'),
            'address_line1' => Yii::t('language', 'Address Line1'),
            'address_line2' => Yii::t('language', 'Address Line2'),
            'address_country_id' => Yii::t('language', 'Address Country ID'),
            'address_province_id' => Yii::t('language', 'Address Province ID'),
            'address_countytown_id' => Yii::t('language', 'Address Countytown ID'),
            'address_districts_id' => Yii::t('language', 'Address Districts ID'),
            'address_postalzip' => Yii::t('language', 'Address Postalzip'),
            'address_remarks' => Yii::t('language', 'Address Remarks'),
            'address_default' => Yii::t('language', 'Address Default'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressCountry()
    {
        return $this->hasOne(SysCountry::className(), ['id' => 'address_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressCountytown()
    {
        return $this->hasOne(SysCountytown::className(), ['id' => 'address_countytown_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressDistricts()
    {
        return $this->hasOne(SysDistricts::className(), ['id' => 'address_districts_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressProvince()
    {
        return $this->hasOne(SysProvince::className(), ['id' => 'address_province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustUser()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'cust_user_id']);
    }
}
