<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sys_countytown".
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $province_id
 * @property string $name
 * @property string $iso_code
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustAddress[] $custAddresses
 * @property SysCountry $country
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 * @property SysProvince $province
 * @property SysDistricts[] $sysDistricts
 * @property TransShippingcost[] $transShippingcosts
 */
class SysCountytown extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys_countytown';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'province_id', 'name', 'createdby', 'createdon', 'lastmodifby', 'lastmodif'], 'required'],
            [['country_id', 'province_id', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['createdon', 'lastmodif'], 'safe'],
            [['name', 'iso_code'], 'string', 'max' => 50],
            [['name'], 'unique'],
            [['iso_code'], 'unique'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysCountry::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['lastmodifby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['lastmodifby' => 'id']],
            [['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysProvince::className(), 'targetAttribute' => ['province_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'country_id' => Yii::t('language', 'Country ID'),
            'province_id' => Yii::t('language', 'Province ID'),
            'name' => Yii::t('language', 'Name'),
            'iso_code' => Yii::t('language', 'Iso Code'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustAddresses()
    {
        return $this->hasMany(CustAddress::className(), ['address_countytown_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(SysCountry::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvince()
    {
        return $this->hasOne(SysProvince::className(), ['id' => 'province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysDistricts()
    {
        return $this->hasMany(SysDistricts::className(), ['countytown_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransShippingcosts()
    {
        return $this->hasMany(TransShippingcost::className(), ['shipping_countytown' => 'id']);
    }

    /**
     * @param $country
     * @param $province
     * @return array
     */
    public static function getCountytownByCountryProvince($country, $province)
    {
        $out = [];
        $data = \common\models\SysCountytown::find()
            ->where(['country_id' => $country, 'province_id' => $province])
            ->select(['id', 'name'])->asArray()->orderBy([
				  'name' => SORT_ASC
				])->all();

        $selected = '';

        foreach ($data as $dat => $datas) {
            $out[] = ['id' => $datas['id'], 'name' => $datas['name']];

            if ($dat == 0) {
                $aux = $datas['id'];
            }

            ($datas['name'] == $country) ? $selected = $country : $selected = $aux;
        }

        return $output = [
            'output' => $out,
            'selected' => $selected
        ];
    }
	
	public static function getTown()
	{
		$data = SysCountytown::find()->select(['id','name'])->asArray()->orderBy([
				  'name' => SORT_ASC
				])->all();
		return ArrayHelper::map($data,'id','name');
	}
}
