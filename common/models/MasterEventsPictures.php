<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "master_events_pictures".
 *
 * @property integer $id
 * @property integer $event_id
 * @property string $picture_name
 * @property string $picture_path
 * @property string $picture_path_md
 * @property string $picture_path_sm
 * @property string $picture_path_xs
 * @property string $picture_path_thumbnail
 * @property string $picture_alt
 * @property integer $sort_no
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property MasterEvents $event
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 */
class MasterEventsPictures extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_events_pictures';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'picture_path', 'picture_path_thumbnail'], 'required'],
            [['event_id', 'sort_no', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['picture_name', 'picture_alt'], 'string', 'max' => 50],
            [['picture_path', 'picture_path_md', 'picture_path_sm', 'picture_path_xs', 'picture_path_thumbnail'], 'string', 'max' => 100],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterEvents::className(), 'targetAttribute' => ['event_id' => 'id']],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['lastmodifby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['lastmodifby' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'event_id' => Yii::t('language', 'Event ID'),
            'picture_name' => Yii::t('language', 'Picture Name'),
            'picture_path' => Yii::t('language', 'Picture Path'),
            'picture_path_md' => Yii::t('language', 'Picture Path Md'),
            'picture_path_sm' => Yii::t('language', 'Picture Path Sm'),
            'picture_path_xs' => Yii::t('language', 'Picture Path Xs'),
            'picture_path_thumbnail' => Yii::t('language', 'Picture Path Thumbnail'),
            'picture_alt' => Yii::t('language', 'Picture Alt'),
            'sort_no' => Yii::t('language', 'Sort No'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(MasterEvents::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }
}
