<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;


/**
 * This is the model class for table "trans_so_pictures".
 *
 * @property integer $id
 * @property integer $id_so_details
 * @property string $picture_name
 * @property string $picture_path
 *
 * @property TransSoDetails $idSoDetails
 */
class TransSoPictures extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trans_so_pictures';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_so_details', 'picture_path'], 'required'],
            [['id_so_details'], 'integer'],
            [['picture_name'], 'string', 'max' => 50],
            [['picture_path'], 'string', 'max' => 100],
            [['id_so_details'], 'exist', 'skipOnError' => true, 'targetClass' => TransSoDetails::className(), 'targetAttribute' => ['id_so_details' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'id_so_details' => Yii::t('language', 'Id So Details'),
            'picture_name' => Yii::t('language', 'Picture Name'),
            'picture_path' => Yii::t('language', 'Picture Path'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSoDetails()
    {
        return $this->hasOne(TransSoDetails::className(), ['id' => 'id_so_details']);
    }
}
