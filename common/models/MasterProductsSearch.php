<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterProducts;

/**
 * MasterProductsSearch represents the model behind the search form about `common\models\MasterProducts`.
 */
class MasterProductsSearch extends MasterProducts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'product_category', 'product_price', 'product_weight', 'product_height', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['product_code', 'product_name', 'product_desc', 'product_remarks', 'custome', 'createdon', 'lastmodif'], 'safe'],
            [['product_volume'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterProducts::find();

        //modification for join 2 table
        $query->joinWith(['customer','productCategory']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['customer_id'] = [
            'asc' => ['master_customer.customer_name' => SORT_ASC],
            'desc' => ['master_customer.customer_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['product_category'] = [
            'asc' => ['master_category.category_name' => SORT_ASC],
            'desc' => ['master_category.category_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'product_price' => $this->product_price,
            'product_weight' => $this->product_weight,
            'product_height' => $this->product_height,
            'product_volume' => $this->product_volume,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
            'custome' => $this->custome,
        ]);

        $query->andFilterWhere(['like', 'product_code', $this->product_code])
            ->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'master_customer.customer_name', $this->customer_id])
            ->andFilterWhere(['like', 'master_category.category_name', $this->product_category])
            ->andFilterWhere(['like', 'product_desc', $this->product_desc])
            ->andFilterWhere(['like', 'product_remarks', $this->product_remarks]);

        return $dataProvider;
    }
}
