<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mailer_log".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $mail_to_address
 * @property string $mail_to_name
 * @property string $mail_cc_address
 * @property string $mail_cc_name
 * @property string $mail_to_message
 * @property integer $isactive
 * @property integer $createdby
 * @property string $createdon
 * @property integer $status
 *
 * @property CustUsers $createdby0
 * @property MasterCustomer $customer
 */
class MailerLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mailer_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'createdby', 'createdon'], 'required'],
            [['customer_id', 'isactive', 'createdby', 'status'], 'integer'],
            [['createdon'], 'safe'],
            [['mail_to_address', 'mail_to_name', 'mail_cc_address', 'mail_cc_name'], 'string', 'max' => 500],
            [['mail_to_message'], 'string', 'max' => 2500],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_id' => Yii::t('language', 'Customer ID'),
            'mail_to_address' => Yii::t('language', 'Mail To Address'),
            'mail_to_name' => Yii::t('language', 'Mail To Name'),
            'mail_cc_address' => Yii::t('language', 'Mail Cc Address'),
            'mail_cc_name' => Yii::t('language', 'Mail Cc Name'),
            'mail_to_message' => Yii::t('language', 'Mail To Message'),
            'isactive' => Yii::t('language', 'Isactive'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }
}
