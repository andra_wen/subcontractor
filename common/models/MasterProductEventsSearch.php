<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterProductEvents;

/**
 * MasterProductEventsSearch represents the model behind the search form about `common\models\MasterProductEvents`.
 */
class MasterProductEventsSearch extends MasterProductEvents
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'event_numbervalue', 'isactive', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['event_name', 'event_caption', 'event_label', 'event_stringvalue', 'effective_from', 'effective_till', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterProductEvents::find();

        //modification for join 2 table
        $query->joinWith(['product']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['product_id'] = [
            'asc' => ['master_products.product_name' => SORT_ASC],
            'desc' => ['master_products.product_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'event_numbervalue' => $this->event_numbervalue,
            'effective_from' => $this->effective_from,
            'effective_till' => $this->effective_till,
            'isactive' => $this->isactive,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'event_name', $this->event_name])
            ->andFilterWhere(['like', 'event_caption', $this->event_caption])
            ->andFilterWhere(['like', 'event_label', $this->event_label])
            ->andFilterWhere(['like', 'master_products.product_name', $this->product_id])
            ->andFilterWhere(['like', 'event_stringvalue', $this->event_stringvalue]);

        return $dataProvider;
    }
}
