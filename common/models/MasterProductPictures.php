<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "master_product_pictures".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $picture_name
 * @property string $picture_path
 * @property string $picture_path_md
 * @property string $picture_path_sm
 * @property string $picture_path_xs
 * @property string $picture_path_thumbnail
 * @property string $picture_alt
 * @property integer $sort_no
 * @property integer $featured
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property MasterProducts $product
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 */
class MasterProductPictures extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_product_pictures';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'picture_path', 'picture_path_thumbnail'], 'required'],
            [['product_id', 'sort_no', 'featured'], 'integer'],
            [['picture_name', 'picture_alt'], 'string', 'max' => 50],
            [['picture_path', 'picture_path_md', 'picture_path_sm', 'picture_path_xs', 'picture_path_thumbnail'], 'string', 'max' => 100],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterProducts::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['lastmodifby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['lastmodifby' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'product_id' => Yii::t('language', 'Product ID'),
            'picture_name' => Yii::t('language', 'Picture Name'),
            'picture_path' => Yii::t('language', 'Picture Path'),
            'picture_path_md' => Yii::t('language', 'Picture Path Md'),
            'picture_path_sm' => Yii::t('language', 'Picture Path Sm'),
            'picture_path_xs' => Yii::t('language', 'Picture Path Xs'),
            'picture_path_thumbnail' => Yii::t('language', 'Picture Path Thumbnail'),
            'picture_alt' => Yii::t('language', 'Picture Alt'),
            'sort_no' => Yii::t('language', 'Sort No'),
            'featured' => Yii::t('language', 'Featured'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(MasterProducts::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }
}
