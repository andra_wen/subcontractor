<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TransSo;

/**
 * TransSoSearch represents the model behind the search form about `common\models\TransSo`.
 */
class TransSoSearch extends TransSo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'cust_user_id', 'paymentmethod_id', 'cust_bank_account_id', 'so_total', 'so_statuscode', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['so_no', 'shipping_totalcost', 'product_totalfee', 'effective_from', 'effective_till', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransSo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'cust_user_id' => $this->cust_user_id,
            'paymentmethod_id' => $this->paymentmethod_id,
            'cust_bank_account_id' => $this->cust_bank_account_id,
            'so_total' => $this->so_total,
            'so_statuscode' => $this->so_statuscode,
            'effective_from' => $this->effective_from,
            'effective_till' => $this->effective_till,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'so_no', $this->so_no])
            ->andFilterWhere(['like', 'shipping_totalcost', $this->shipping_totalcost])
            ->andFilterWhere(['like', 'product_totalfee', $this->product_totalfee]);

        return $dataProvider;
    }
}
