<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sys_language".
 *
 * @property integer $id
 * @property string $code
 * @property string $short_name
 * @property string $long_name
 * @property string $iso_code
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustLanguage $custLanguage
 * @property CustTransExtendfields[] $custTransExtendfields
 * @property MasterTransExtendfields[] $masterTransExtendfields
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 */
class SysLanguage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys_language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'short_name', 'createdby', 'createdon', 'lastmodifby', 'lastmodif'], 'required'],
            [['createdby', 'lastmodifby', 'status'], 'integer'],
            [['createdon', 'lastmodif'], 'safe'],
            [['code'], 'string', 'max' => 10],
            [['short_name'], 'string', 'max' => 100],
            [['long_name'], 'string', 'max' => 200],
            [['iso_code'], 'string', 'max' => 50],
            [['code'], 'unique'],
            [['short_name'], 'unique'],
            [['iso_code'], 'unique'],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['lastmodifby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['lastmodifby' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'code' => Yii::t('language', 'Code'),
            'short_name' => Yii::t('language', 'Short Name'),
            'long_name' => Yii::t('language', 'Long Name'),
            'iso_code' => Yii::t('language', 'Iso Code'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustLanguage()
    {
        return $this->hasOne(CustLanguage::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustTransExtendfields()
    {
        return $this->hasMany(CustTransExtendfields::className(), ['field_language' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterTransExtendfields()
    {
        return $this->hasMany(MasterTransExtendfields::className(), ['field_language' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }
}
