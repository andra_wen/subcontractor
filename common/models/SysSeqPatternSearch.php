<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SysSeqPattern;

/**
 * SysSeqPatternSearch represents the model behind the search form about `common\models\SysSeqPattern`.
 */
class SysSeqPatternSearch extends SysSeqPattern
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'pattern_lastsequences', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['pattern_name', 'pattern_desc', 'pattern_fortable', 'pattern_format', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SysSeqPattern::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'pattern_lastsequences' => $this->pattern_lastsequences,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'pattern_name', $this->pattern_name])
            ->andFilterWhere(['like', 'pattern_desc', $this->pattern_desc])
            ->andFilterWhere(['like', 'pattern_fortable', $this->pattern_fortable])
            ->andFilterWhere(['like', 'pattern_format', $this->pattern_format]);

        return $dataProvider;
    }
}
