<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "cust_user_product".
 *
 * @property int $id
 * @property int $cust_user_id
 * @property int $product_id
 * @property string $createdon
 * @property string $lastmodifon
 * @property int $createdby
 * @property int $lastmodifby
 */
class CustUsersProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cust_users_product';
    }
	
	public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodifon',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cust_user_id', 'product_id'], 'required'],
            [['cust_user_id', 'product_id', 'createdby', 'lastmodifby'], 'integer'],
            [['createdon', 'lastmodifon'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cust_user_id' => Yii::t('app', 'Cust User ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'createdon' => Yii::t('app', 'Createdon'),
            'lastmodifon' => Yii::t('app', 'Lastmodifon'),
            'createdby' => Yii::t('app', 'Createdby'),
            'lastmodifby' => Yii::t('app', 'Lastmodifby'),
        ];
    }
}
