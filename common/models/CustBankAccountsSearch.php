<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CustBankAccounts;

/**
 * CustBankAccountsSearch represents the model behind the search form about `common\models\CustBankAccounts`.
 */
class CustBankAccountsSearch extends CustBankAccounts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bank_id', 'isactive', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['bank_accountname', 'bank_accountno', 'bank_info', 'bank_remarks', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustBankAccounts::find();

        //modification for join 2 table
        $query->joinWith(['bank']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['bank_id'] = [
            'asc' => ['cust_banks.bank_shortname' => SORT_ASC],
            'desc' => ['cust_banks.bank_shortname' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'isactive' => $this->isactive,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'bank_accountname', $this->bank_accountname])
            ->andFilterWhere(['like', 'bank_accountno', $this->bank_accountno])
            ->andFilterWhere(['like', 'bank_info', $this->bank_info])
            ->andFilterWhere(['like', 'cust_banks.bank_shortname', $this->bank_id])
            ->andFilterWhere(['like', 'bank_remarks', $this->bank_remarks]);

        return $dataProvider;
    }
}
