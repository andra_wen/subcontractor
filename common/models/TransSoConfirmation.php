<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "trans_so_confirmation".
 *
 * @property integer $id
 * @property integer $so_id
 * @property string $so_no
 * @property integer $paymentmethod_id
 * @property string $paymentmethod_name
 * @property string $account_no
 * @property string $account_holder
 * @property string $phone_number
 * @property integer $total_amount
 * @property string $payment_date
 * @property string $confirm_remarks
 * @property string $confirm_attachment
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property TransPaymentmethod $paymentmethod
 * @property TransSo $so
 */
class TransSoConfirmation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trans_so_confirmation';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['so_id', 'so_no'], 'required'],
            [['so_id', 'paymentmethod_id', 'total_amount', 'status'], 'integer'],
            [['payment_date', 'createdon', 'lastmodif'], 'safe'],
            [['so_no', 'paymentmethod_name', 'account_no', 'account_holder'], 'string', 'max' => 50],
            [['phone_number'], 'string', 'max' => 30],
            [['confirm_remarks'], 'string', 'max' => 300],
            [['confirm_attachment'], 'string', 'max' => 200],
            [['so_no'], 'unique'],
            [['paymentmethod_id'], 'exist', 'skipOnError' => true, 'targetClass' => TransPaymentmethod::className(), 'targetAttribute' => ['paymentmethod_id' => 'id']],
            [['so_id'], 'exist', 'skipOnError' => true, 'targetClass' => TransSo::className(), 'targetAttribute' => ['so_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'so_id' => Yii::t('language', 'So ID'),
            'so_no' => Yii::t('language', 'So No'),
            'paymentmethod_id' => Yii::t('language', 'Paymentmethod ID'),
            'paymentmethod_name' => Yii::t('language', 'Paymentmethod Name'),
            'account_no' => Yii::t('language', 'Account No'),
            'account_holder' => Yii::t('language', 'Account Holder'),
            'phone_number' => Yii::t('language', 'Phone Number'),
            'total_amount' => Yii::t('language', 'Total Amount'),
            'payment_date' => Yii::t('language', 'Payment Date'),
            'confirm_remarks' => Yii::t('language', 'Confirm Remarks'),
            'confirm_attachment' => Yii::t('language', 'Confirm Attachment'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentmethod()
    {
        return $this->hasOne(TransPaymentmethod::className(), ['id' => 'paymentmethod_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSo()
    {
        return $this->hasOne(TransSo::className(), ['id' => 'so_id']);
    }
}
