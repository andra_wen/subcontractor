<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterEvents;

/**
 * MasterEventsSearch represents the model behind the search form about `common\models\MasterEvents`.
 */
class MasterEventsSearch extends MasterEvents
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'page_id', 'isactive', 'sort_no', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['event_name', 'event_title', 'event_content', 'event_pict', 'event_alt', 'event_date', 'effective_from', 'effective_till', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterEvents::find();

        //modification for join 2 table
        $query->joinWith(['customer','page']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['customer_id'] = [
            'asc' => ['master_customer.customer_name' => SORT_ASC],
            'desc' => ['master_customer.customer_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['page_id'] = [
            'asc' => ['master_pages.page_name' => SORT_ASC],
            'desc' => ['master_pages.page_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'isactive' => $this->isactive,
            'sort_no' => $this->sort_no,
            'event_date' => $this->event_date,
            'effective_from' => $this->effective_from,
            'effective_till' => $this->effective_till,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'event_name', $this->event_name])
            ->andFilterWhere(['like', 'event_title', $this->event_title])
            ->andFilterWhere(['like', 'event_content', $this->event_content])
            ->andFilterWhere(['like', 'event_pict', $this->event_pict])
            ->andFilterWhere(['master_customer.id' => $this->customer_id])
            ->andFilterWhere(['master_pages.id' => $this->page_id])
            ->andFilterWhere(['like', 'event_alt', $this->event_alt]);

        return $dataProvider;
    }
}
