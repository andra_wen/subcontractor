<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SysCurrency;

/**
 * SysCurrencySearch represents the model behind the search form about `common\models\SysCurrency`.
 */
class SysCurrencySearch extends SysCurrency
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'intl_formatting', 'min_fraction_digits', 'max_fraction_digits', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['code', 'iso_code', 'name', 'dec_point', 'thousands_sep', 'format_string', 'createdon', 'lastmodif'], 'safe'],
            [['convert_nomimal', 'convert_rate'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SysCurrency::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'convert_nomimal' => $this->convert_nomimal,
            'convert_rate' => $this->convert_rate,
            'intl_formatting' => $this->intl_formatting,
            'min_fraction_digits' => $this->min_fraction_digits,
            'max_fraction_digits' => $this->max_fraction_digits,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'iso_code', $this->iso_code])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'dec_point', $this->dec_point])
            ->andFilterWhere(['like', 'thousands_sep', $this->thousands_sep])
            ->andFilterWhere(['like', 'format_string', $this->format_string]);

        return $dataProvider;
    }
}
