<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MailerLog;

/**
 * MailerLogSearch represents the model behind the search form about `common\models\MailerLog`.
 */
class MailerLogSearch extends MailerLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'isactive', 'createdby', 'status'], 'integer'],
            [['mail_to_address', 'mail_to_name', 'mail_cc_address', 'mail_cc_name', 'mail_to_message', 'createdon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MailerLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'isactive' => $this->isactive,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'mail_to_address', $this->mail_to_address])
            ->andFilterWhere(['like', 'mail_to_name', $this->mail_to_name])
            ->andFilterWhere(['like', 'mail_cc_address', $this->mail_cc_address])
            ->andFilterWhere(['like', 'mail_cc_name', $this->mail_cc_name])
            ->andFilterWhere(['like', 'mail_to_message', $this->mail_to_message]);

        return $dataProvider;
    }
}
