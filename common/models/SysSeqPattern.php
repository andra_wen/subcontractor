<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sys_seq_pattern".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $pattern_name
 * @property string $pattern_desc
 * @property string $pattern_fortable
 * @property string $pattern_format
 * @property integer $pattern_lastsequences
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustUsers $createdby0
 * @property MasterCustomer $customer
 * @property CustUsers $lastmodifby0
 */
class SysSeqPattern extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys_seq_pattern';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'pattern_name', 'createdby', 'createdon', 'lastmodifby', 'lastmodif'], 'required'],
            [['customer_id', 'pattern_lastsequences', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['createdon', 'lastmodif'], 'safe'],
            [['pattern_name'], 'string', 'max' => 50],
            [['pattern_desc'], 'string', 'max' => 200],
            [['pattern_fortable', 'pattern_format'], 'string', 'max' => 100],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['lastmodifby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['lastmodifby' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_id' => Yii::t('language', 'Customer ID'),
            'pattern_name' => Yii::t('language', 'Pattern Name'),
            'pattern_desc' => Yii::t('language', 'Pattern Desc'),
            'pattern_fortable' => Yii::t('language', 'Pattern Fortable'),
            'pattern_format' => Yii::t('language', 'Pattern Format'),
            'pattern_lastsequences' => Yii::t('language', 'Pattern Lastsequences'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }
}
