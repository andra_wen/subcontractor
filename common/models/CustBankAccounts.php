<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "cust_bank_accounts".
 *
 * @property integer $id
 * @property integer $bank_id
 * @property string $bank_accountname
 * @property string $bank_accountno
 * @property string $bank_info
 * @property string $bank_remarks
 * @property integer $isactive
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustBanks $bank
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 * @property TransPaymentmethodbank[] $transPaymentmethodbanks
 * @property TransSo[] $transSos
 */
class CustBankAccounts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cust_bank_accounts';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank_id', 'bank_accountname', 'bank_accountno'], 'required'],
            [['bank_id', 'isactive', 'status'], 'integer'],
            [['bank_accountname', 'bank_accountno'], 'string', 'max' => 100],
            [['bank_info', 'bank_remarks'], 'string', 'max' => 200],
            [['bank_accountno'], 'unique'],
            [['bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustBanks::className(), 'targetAttribute' => ['bank_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'bank_id' => Yii::t('language', 'Bank ID'),
            'bank_accountname' => Yii::t('language', 'Bank Accountname'),
            'bank_accountno' => Yii::t('language', 'Bank Accountno'),
            'bank_info' => Yii::t('language', 'Bank Info'),
            'bank_remarks' => Yii::t('language', 'Bank Remarks'),
            'isactive' => Yii::t('language', 'Isactive'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBank()
    {
        return $this->hasOne(CustBanks::className(), ['id' => 'bank_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransPaymentmethodbanks()
    {
        return $this->hasMany(TransPaymentmethodbank::className(), ['cust_bank_account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransSos()
    {
        return $this->hasMany(TransSo::className(), ['cust_bank_account_id' => 'id']);
    }
}
