<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "trans_carts".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $cust_client_id
 * @property integer $product_id
 * @property integer $product_varians_id
 * @property integer $product_quantity
 * @property integer $provider_id
 * @property integer $shippingcost_id
 * @property integer $client_address_id
 * @property string $cart_remarks
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustClientAddress $clientAddress
 * @property CustClients $custClient
 * @property CustUsers $createdby0
 * @property MasterCustomer $customer
 * @property CustUsers $lastmodifby0
 * @property MasterProducts $product
 * @property TransShippingprovider $provider
 * @property TransShippingcost $shippingcost
 * @property MasterProductVarians $productVarians
 */
class TransCarts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trans_carts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'cust_client_id', 'product_id', 'createdby', 'createdon', 'lastmodifby', 'lastmodif'], 'required'],
            [['customer_id', 'cust_client_id', 'product_id', 'product_varians_id', 'product_quantity', 'provider_id', 'shippingcost_id', 'client_address_id', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['createdon', 'lastmodif'], 'safe'],
            [['cart_remarks'], 'string', 'max' => 200],
            [['client_address_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustClientAddress::className(), 'targetAttribute' => ['client_address_id' => 'id']],
            [['cust_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustClients::className(), 'targetAttribute' => ['cust_client_id' => 'id']],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['lastmodifby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['lastmodifby' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterProducts::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => TransShippingprovider::className(), 'targetAttribute' => ['provider_id' => 'id']],
            [['shippingcost_id'], 'exist', 'skipOnError' => true, 'targetClass' => TransShippingcost::className(), 'targetAttribute' => ['shippingcost_id' => 'id']],
            [['product_varians_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterProductVarians::className(), 'targetAttribute' => ['product_varians_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_id' => Yii::t('language', 'Customer ID'),
            'cust_client_id' => Yii::t('language', 'Cust Client ID'),
            'product_id' => Yii::t('language', 'Product ID'),
            'product_varians_id' => Yii::t('language', 'Product Varians ID'),
            'product_quantity' => Yii::t('language', 'Product Quantity'),
            'provider_id' => Yii::t('language', 'Provider ID'),
            'shippingcost_id' => Yii::t('language', 'Shippingcost ID'),
            'client_address_id' => Yii::t('language', 'Client Address ID'),
            'cart_remarks' => Yii::t('language', 'Cart Remarks'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientAddress()
    {
        return $this->hasOne(CustClientAddress::className(), ['id' => 'client_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustClient()
    {
        return $this->hasOne(CustClients::className(), ['id' => 'cust_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(MasterProducts::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(TransShippingprovider::className(), ['id' => 'provider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShippingcost()
    {
        return $this->hasOne(TransShippingcost::className(), ['id' => 'shippingcost_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVarians()
    {
        return $this->hasOne(MasterProductVarians::className(), ['id' => 'product_varians_id']);
    }
}
