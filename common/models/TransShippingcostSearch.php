<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TransShippingcost;

/**
 * TransShippingcostSearch represents the model behind the search form about `common\models\TransShippingcost`.
 */
class TransShippingcostSearch extends TransShippingcost
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'shippingprovider_id', 'shipping_country', 'shipping_province', 'shipping_countytown', 'shipping_districts', 'shipping_prices', 'shipping_minweight', 'shipping_minweightprices', 'shipping_minwegithnextprice', 'shipping_maxweight', 'isactive', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['shipping_note', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransShippingcost::find();

        //modification for join 2 table
        $query->joinWith(['customer', 'shippingprovider', 'shippingCountry', 'shippingProvince', 'shippingCountytown', 'shippingDistricts']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['customer_id'] = [
            'asc' => ['master_customer.customer_name' => SORT_ASC],
            'desc' => ['master_customer.customer_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['shippingprovider_id'] = [
            'asc' => ['trans_shippingprovider.shipping_name' => SORT_ASC],
            'desc' => ['trans_shippingprovider.shipping_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['shipping_country'] = [
            'asc' => ['sys_country.short_name' => SORT_ASC],
            'desc' => ['sys_country.short_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['shipping_province'] = [
            'asc' => ['sys_province.name' => SORT_ASC],
            'desc' => ['sys_province.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['shipping_countytown'] = [
            'asc' => ['sys_countytown.name' => SORT_ASC],
            'desc' => ['sys_countytown.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['shipping_districts'] = [
            'asc' => ['sys_districts.name' => SORT_ASC],
            'desc' => ['sys_districts.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'shipping_prices' => $this->shipping_prices,
            'shipping_minweight' => $this->shipping_minweight,
            'shipping_minweightprices' => $this->shipping_minweightprices,
            'shipping_minwegithnextprice' => $this->shipping_minwegithnextprice,
            'shipping_maxweight' => $this->shipping_maxweight,
            'isactive' => $this->isactive,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'shipping_note', $this->shipping_note])
            ->andFilterWhere(['like', 'master_customer.customer_name', $this->customer_id])
            ->andFilterWhere(['like', 'trans_shippingprovider.shipping_name', $this->shippingprovider_id])
            ->andFilterWhere(['like', 'sys_country.short_name', $this->shipping_country])
            ->andFilterWhere(['like', 'sys_province.name', $this->shipping_province])
            ->andFilterWhere(['like', 'sys_countytown.name', $this->shipping_countytown])
            ->andFilterWhere(['like', 'sys_districts.name', $this->shipping_districts]);

        return $dataProvider;
    }
}
