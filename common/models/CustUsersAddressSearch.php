<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CustUsersAddress;

/**
 * CustUsersAddressSearch represents the model behind the search form about `common\models\CustUsersAddress`.
 */
class CustUsersAddressSearch extends CustUsersAddress
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cust_user_id', 'address_country_id', 'address_province_id', 'address_countytown_id', 'address_districts_id', 'address_postalzip', 'address_default', 'status'], 'integer'],
            [['address_name', 'address_line1', 'address_line2', 'address_phonenumber', 'address_remarks', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustUsersAddress::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'cust_user_id' => $this->cust_user_id,
            'address_country_id' => $this->address_country_id,
            'address_province_id' => $this->address_province_id,
            'address_countytown_id' => $this->address_countytown_id,
            'address_districts_id' => $this->address_districts_id,
            'address_postalzip' => $this->address_postalzip,
            'address_default' => $this->address_default,
            'createdon' => $this->createdon,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'address_name', $this->address_name])
            ->andFilterWhere(['like', 'address_line1', $this->address_line1])
            ->andFilterWhere(['like', 'address_line2', $this->address_line2])
            ->andFilterWhere(['like', 'address_phonenumber', $this->address_phonenumber])
            ->andFilterWhere(['like', 'address_remarks', $this->address_remarks]);

        return $dataProvider;
    }
}
