<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sys_currency".
 *
 * @property integer $id
 * @property string $code
 * @property string $iso_code
 * @property string $name
 * @property double $convert_nomimal
 * @property double $convert_rate
 * @property integer $intl_formatting
 * @property integer $min_fraction_digits
 * @property integer $max_fraction_digits
 * @property string $dec_point
 * @property string $thousands_sep
 * @property string $format_string
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 */
class SysCurrency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys_currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'createdby', 'createdon', 'lastmodifby', 'lastmodif'], 'required'],
            [['convert_nomimal', 'convert_rate'], 'number'],
            [['intl_formatting', 'min_fraction_digits', 'max_fraction_digits', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['createdon', 'lastmodif'], 'safe'],
            [['code'], 'string', 'max' => 10],
            [['iso_code'], 'string', 'max' => 50],
            [['name', 'format_string'], 'string', 'max' => 100],
            [['dec_point', 'thousands_sep'], 'string', 'max' => 2],
            [['iso_code'], 'unique'],
            [['name'], 'unique'],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['lastmodifby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['lastmodifby' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'code' => Yii::t('language', 'Code'),
            'iso_code' => Yii::t('language', 'Iso Code'),
            'name' => Yii::t('language', 'Name'),
            'convert_nomimal' => Yii::t('language', 'Convert Nomimal'),
            'convert_rate' => Yii::t('language', 'Convert Rate'),
            'intl_formatting' => Yii::t('language', 'Intl Formatting'),
            'min_fraction_digits' => Yii::t('language', 'Min Fraction Digits'),
            'max_fraction_digits' => Yii::t('language', 'Max Fraction Digits'),
            'dec_point' => Yii::t('language', 'Dec Point'),
            'thousands_sep' => Yii::t('language', 'Thousands Sep'),
            'format_string' => Yii::t('language', 'Format String'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }
}
