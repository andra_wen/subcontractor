<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "trans_paymentmethod".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $payment_group
 * @property string $payment_title
 * @property string $payment_name
 * @property string $payment_pict
 * @property integer $isactive
 * @property string $payment_notes
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustUsers $createdby0
 * @property MasterCustomer $customer
 * @property CustUsers $lastmodifby0
 * @property TransPaymentmethodbank[] $transPaymentmethodbanks
 * @property TransSo[] $transSos
 */
class TransPaymentmethod extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trans_paymentmethod';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'payment_title'], 'required'],
            [['customer_id', 'isactive', 'status'], 'integer'],
            [['payment_group', 'payment_title', 'payment_name', 'payment_pict'], 'string', 'max' => 100],
            [['payment_notes'], 'string', 'max' => 300],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_id' => Yii::t('language', 'Customer ID'),
            'payment_group' => Yii::t('language', 'Payment Group'),
            'payment_title' => Yii::t('language', 'Payment Title'),
            'payment_name' => Yii::t('language', 'Payment Name'),
            'payment_pict' => Yii::t('language', 'Payment Pict'),
            'isactive' => Yii::t('language', 'Isactive'),
            'payment_notes' => Yii::t('language', 'Payment Notes'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransPaymentmethodbanks()
    {
        return $this->hasMany(TransPaymentmethodbank::className(), ['paymentmethod_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransSos()
    {
        return $this->hasMany(TransSo::className(), ['paymentmethod_id' => 'id']);
    }
}
