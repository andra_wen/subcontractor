<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TransCarts;

/**
 * TransCartsSearch represents the model behind the search form about `common\models\TransCarts`.
 */
class TransCartsSearch extends TransCarts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'cust_client_id', 'product_id', 'product_varians_id', 'product_quantity', 'provider_id', 'shippingcost_id', 'client_address_id', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['cart_remarks', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransCarts::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'cust_client_id' => $this->cust_client_id,
            'product_id' => $this->product_id,
            'product_varians_id' => $this->product_varians_id,
            'product_quantity' => $this->product_quantity,
            'provider_id' => $this->provider_id,
            'shippingcost_id' => $this->shippingcost_id,
            'client_address_id' => $this->client_address_id,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'cart_remarks', $this->cart_remarks]);

        return $dataProvider;
    }
}
