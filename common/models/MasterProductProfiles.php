<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "master_product_profiles".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $barcode
 * @property integer $uom_id
 * @property integer $alias
 * @property string $manufacturing_date
 * @property integer $shelflife
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property MasterProducts $alias0
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 * @property MasterProducts $product
 * @property SysUom $uom
 */
class MasterProductProfiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_product_profiles';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id'], 'required'],
            [['product_id', 'uom_id', 'alias', 'shelflife', 'status'], 'integer'],
            [['manufacturing_date', 'createdon', 'lastmodif'], 'safe'],
            [['barcode'], 'string', 'max' => 50],
            [['barcode'], 'unique'],
            [['alias'], 'exist', 'skipOnError' => true, 'targetClass' => MasterProducts::className(), 'targetAttribute' => ['alias' => 'id']],
            [['uom_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysUom::className(), 'targetAttribute' => ['uom_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'product_id' => Yii::t('language', 'Product ID'),
            'barcode' => Yii::t('language', 'Barcode'),
            'uom_id' => Yii::t('language', 'Uom ID'),
            'alias' => Yii::t('language', 'Alias'),
            'manufacturing_date' => Yii::t('language', 'Manufacturing Date'),
            'shelflife' => Yii::t('language', 'Shelflife'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlias0()
    {
        return $this->hasOne(MasterProducts::className(), ['id' => 'alias']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(MasterProducts::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUom()
    {
        return $this->hasOne(SysUom::className(), ['id' => 'uom_id']);
    }
}
