<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CustUsersProfile;

/**
 * CustUserProfilesSearch represents the model behind the search form about `common\models\CustUsersProfile`.
 */
class CustUserProfilesSearch extends CustUsersProfile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cust_user_id', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['user_firstname', 'user_middlename', 'user_lastname', 'user_picture', 'placeofbirth', 'dateofbirth', 'gender', 'marriagestatus', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustUsersProfile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cust_user_id' => $this->cust_user_id,
            'dateofbirth' => $this->dateofbirth,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'user_firstname', $this->user_firstname])
            ->andFilterWhere(['like', 'user_middlename', $this->user_middlename])
            ->andFilterWhere(['like', 'user_lastname', $this->user_lastname])
            ->andFilterWhere(['like', 'user_picture', $this->user_picture])
            ->andFilterWhere(['like', 'placeofbirth', $this->placeofbirth])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'marriagestatus', $this->marriagestatus]);

        return $dataProvider;
    }
}
