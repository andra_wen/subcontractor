<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TransPaymentmethodbank;

/**
 * TransPaymentMethodBankSearch represents the model behind the search form about `common\models\TransPaymentMethodBank`.
 */
class TransPaymentmethodbankSearch extends TransPaymentmethodbank
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'paymentmethod_id', 'cust_bank_account_id', 'isactive', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransPaymentMethodBank::find();

        //modification for join 2 table
        $query->joinWith(['customer','custBankAccount','paymentmethod']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['customer_id'] = [
            'asc' => ['master_customer.customer_name' => SORT_ASC],
            'desc' => ['master_customer.customer_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['paymentmethod_id'] = [
            'asc' => ['trans_paymentmethod.payment_name' => SORT_ASC],
            'desc' => ['trans_paymentmethod.payment_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['cust_bank_account_id'] = [
            'asc' => ['cust_bank_accounts.bank_accountno' => SORT_ASC],
            'desc' => ['cust_bank_accounts.bank_accountno' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'isactive' => $this->isactive,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'trans_paymentmethod.payment_name', $this->paymentmethod_id])
            ->andFilterWhere(['like', 'cust_bank_accounts.bank_accountno', $this->cust_bank_account_id])
            ->andFilterWhere(['like', 'master_customer.customer_name', $this->customer_id]);

        return $dataProvider;
    }
}
