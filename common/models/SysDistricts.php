<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sys_districts".
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $province_id
 * @property integer $countytown_id
 * @property string $name
 * @property string $iso_code
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustAddress[] $custAddresses
 * @property SysCountry $country
 * @property SysCountytown $countytown
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 * @property SysProvince $province
 * @property TransShippingcost[] $transShippingcosts
 */
class SysDistricts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys_districts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'province_id', 'countytown_id', 'name', 'createdby', 'createdon', 'lastmodifby', 'lastmodif'], 'required'],
            [['country_id', 'province_id', 'countytown_id', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['createdon', 'lastmodif'], 'safe'],
            [['name', 'iso_code'], 'string', 'max' => 50],
            [['iso_code'], 'unique'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysCountry::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['countytown_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysCountytown::className(), 'targetAttribute' => ['countytown_id' => 'id']],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['lastmodifby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['lastmodifby' => 'id']],
            [['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysProvince::className(), 'targetAttribute' => ['province_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'country_id' => Yii::t('language', 'Country ID'),
            'province_id' => Yii::t('language', 'Province ID'),
            'countytown_id' => Yii::t('language', 'Countytown ID'),
            'name' => Yii::t('language', 'Name'),
            'iso_code' => Yii::t('language', 'Iso Code'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustAddresses()
    {
        return $this->hasMany(CustAddress::className(), ['address_districts_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(SysCountry::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountytown()
    {
        return $this->hasOne(SysCountytown::className(), ['id' => 'countytown_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvince()
    {
        return $this->hasOne(SysProvince::className(), ['id' => 'province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransShippingcosts()
    {
        return $this->hasMany(TransShippingcost::className(), ['shipping_districts' => 'id']);
    }

    public static function getDistrictsByCountryProvinceCountytown($country, $province, $countytown)
    {
        $out = [];
        $data = \common\models\SysDistricts::find()
            ->where(['country_id' => $country, 'province_id' => $province, 'countytown_id'=> $countytown])
            ->select(['id', 'name'])->asArray()->orderBy([
				  'name' => SORT_ASC
				])->all();

        $selected = '';

        foreach ($data as $dat => $datas) {
            $out[] = ['id' => $datas['id'], 'name' => $datas['name']];

            if ($dat == 0) {
                $aux = $datas['id'];
            }

            ($datas['name'] == $country) ? $selected = $country : $selected = $aux;
        }

        return $output = [
            'output' => $out,
            'selected' => $selected
        ];
    }
	
	public static function getDistricts()
	{
		$data = SysDistricts::find()->select(['id','name'])->asArray()->orderBy([
				  'name' => SORT_ASC
				])->all();
		return ArrayHelper::map($data,'id','name');
	}
	
}
