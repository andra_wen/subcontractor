<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "trans_so".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $cust_user_id
 * @property string $so_no
 * @property integer $paymentmethod_id
 * @property integer $cust_bank_account_id
 * @property string $shipping_totalcost
 * @property string $product_totalfee
 * @property integer $so_total
 * @property integer $so_statuscode
 * @property string $effective_from
 * @property string $effective_till
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustBankAccounts $custBankAccount
 * @property MasterCustomer $customer
 * @property TransPaymentmethod $paymentmethod
 * @property CustUsers $custUser
 * @property TransSoDetails[] $transSoDetails
 */
class TransSo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trans_so';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'cust_user_id', 'so_no', 'so_statuscode'], 'required'],
            [['customer_id', 'cust_user_id', 'paymentmethod_id', 'cust_bank_account_id', 'so_total', 'so_statuscode', 'status'], 'integer'],
            [['effective_from', 'effective_till'], 'safe'],
            [['so_no'], 'string', 'max' => 50],
            [['shipping_totalcost'], 'string', 'max' => 300],
            [['product_totalfee'], 'string', 'max' => 100],
            [['so_no'], 'unique'],
            [['cust_bank_account_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustBankAccounts::className(), 'targetAttribute' => ['cust_bank_account_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['paymentmethod_id'], 'exist', 'skipOnError' => true, 'targetClass' => TransPaymentmethod::className(), 'targetAttribute' => ['paymentmethod_id' => 'id']],
            [['cust_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['cust_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_id' => Yii::t('language', 'Customer ID'),
            'cust_user_id' => Yii::t('language', 'Cust User ID'),
            'so_no' => Yii::t('language', 'So No'),
            'paymentmethod_id' => Yii::t('language', 'Paymentmethod ID'),
            'cust_bank_account_id' => Yii::t('language', 'Cust Bank Account ID'),
            'shipping_totalcost' => Yii::t('language', 'Shipping Totalcost'),
            'product_totalfee' => Yii::t('language', 'Product Totalfee'),
            'so_total' => Yii::t('language', 'So Total'),
            'so_statuscode' => Yii::t('language', 'So Statuscode'),
            'effective_from' => Yii::t('language', 'Effective From'),
            'effective_till' => Yii::t('language', 'Effective Till'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustBankAccount()
    {
        return $this->hasOne(CustBankAccounts::className(), ['id' => 'cust_bank_account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentmethod()
    {
        return $this->hasOne(TransPaymentmethod::className(), ['id' => 'paymentmethod_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustUser()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'cust_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransSoDetails()
    {
        return $this->hasMany(TransSoDetails::className(), ['so_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransStatusCode()
    {
        return $this->hasOne(TransStatuscode::className(), ['id' => 'so_statuscode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    public static function getSodetailbyid($so_id)
    {
        $connection = Yii::$app->getDb();
        $command = '
            SELECT
            h.so_no,
            h.paymentmethod_id,
            h.shipping_totalcost,
            h.product_totalfee,
            h.so_total,
            h.so_statuscode,
            cu.username,
            cu.email,
            d.product_id,
            d.product_name,
            d.product_price,
            d.provider_name,
            d.shippingcost_id,
            d.shippingcost,
            d.user_address_id,
            d.address_name,
            d.address_line1,
            d.address_line2,
            d.address_country,
            d.address_province,
            d.address_countytown,
            d.address_districts,
            d.address_postalzip,
            d.cart_remarks,
            d.custome,
            d.details_total,
            d.product_quantity,
            d.provider_id,
            ts.trans_name,
            cba.bank_accountname,
            cba.bank_accountno,
            tp.payment_namepayment_name
            FROM trans_so h
            LEFT JOIN cust_users cu ON h.cust_user_id = cu.id
            LEFT JOIN trans_so_details AS d ON h.id = d.so_id
            LEFT JOIN trans_statuscode ts ON h.so_statuscode = ts.id
            LEFT JOIN cust_bank_accounts cba ON h.cust_bank_account_id = cba.id
            LEFT JOIN trans_paymentmethod tp ON h.paymentmethod_id = tp.id
            WHERE h.id = :so_id';
            $queryProvider = new \yii\data\SqlDataProvider([
            'sql'=>$command,
            'params'=>[':so_id'=>$so_id]
        ]);

        $dataQuery = $queryProvider->getModels();
        return $dataQuery;
    }
}
