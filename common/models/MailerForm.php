<?php
/**
 * Created by JetBrains PhpStorm.
 * Framwork: Yii2
 * User: andrawen
 * Date: 1/27/18
 * Time: 10:05 PM
 * To change this template use File | Settings | File Templates.
 */

namespace common\models;

use Yii;
use yii\base\Model;

class MailerForm extends \yii\db\ActiveRecord
{
    public $name;
    public $email;
    public $mail_to_address;

    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'email'],
        ];
    }
}