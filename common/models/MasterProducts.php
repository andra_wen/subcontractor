<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "master_products".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $product_category
 * @property string $product_code
 * @property string $product_name
 * @property string $product_desc
 * @property integer $product_price
 * @property integer $product_weight
 * @property integer $product_height
 * @property string $product_volume
 * @property string $product_remarks
 * @property integer $custome
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property MasterProductEvents[] $masterProductEvents
 * @property MasterProductPictures[] $masterProductPictures
 * @property MasterProductProfiles[] $masterProductProfiles
 * @property MasterProductProfiles[] $masterProductProfiles0
 * @property MasterProductVarians[] $masterProductVarians
 * @property MasterCategory $productCategory
 * @property CustUsers $createdby0
 * @property MasterCustomer $customer
 * @property CustUsers $lastmodifby0
 * @property TransAdjustments[] $transAdjustments
 * @property TransCarts[] $transCarts
 * @property TransSoDetails[] $transSoDetails
 * @property TransWishlist[] $transWishlists
 */
class MasterProducts extends \yii\db\ActiveRecord
{
    public $getfile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_products';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'createdby',
                'updatedByAttribute' => 'lastmodifby',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'product_name', 'product_category'], 'required'],
            [['customer_id', 'product_category', 'product_price', 'product_weight', 'product_height','custome', 'status'], 'integer'],
            [['product_volume'], 'number'],
            [['product_code'], 'string', 'max' => 50],
            [['product_name'], 'string', 'max' => 150],
            [['product_desc', 'product_remarks'], 'string', 'max' => 200],
            [['product_code'], 'unique'],
            [['getfile'],'safe'],
            ['getfile',  'file', 'maxSize'=>'5000000', 'skipOnEmpty' => true, 'maxFiles' => 5],
            [['product_category'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCategory::className(), 'targetAttribute' => ['product_category' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_id' => Yii::t('language', 'Customer ID'),
            'product_category' => Yii::t('language', 'Product Category'),
            'product_code' => Yii::t('language', 'Product Code'),
            'product_name' => Yii::t('language', 'Product Name'),
            'product_desc' => Yii::t('language', 'Product Desc'),
            'product_price' => Yii::t('language', 'Product Price'),
            'product_weight' => Yii::t('language', 'Product Weight'),
            'product_height' => Yii::t('language', 'Product Height'),
            'product_volume' => Yii::t('language', 'Product Volume'),
            'product_remarks' => Yii::t('language', 'Product Remarks'),
            'getfile'=>Yii::t('language', 'Picture'),
            'custome' => Yii::t('language', 'Custome'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProductEvents()
    {
        return $this->hasMany(MasterProductEvents::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProductPictures()
    {
        return $this->hasMany(MasterProductPictures::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProductProfiles()
    {
        return $this->hasMany(MasterProductProfiles::className(), ['alias' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProductProfiles0()
    {
        return $this->hasMany(MasterProductProfiles::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProductVarians()
    {
        return $this->hasMany(MasterProductVarians::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategory()
    {
        return $this->hasOne(MasterCategory::className(), ['id' => 'product_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransAdjustments()
    {
        return $this->hasMany(TransAdjustments::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransCarts()
    {
        return $this->hasMany(TransCarts::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransSoDetails()
    {
        return $this->hasMany(TransSoDetails::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransWishlists()
    {
        return $this->hasMany(TransWishlist::className(), ['product_id' => 'id']);
    }
}
