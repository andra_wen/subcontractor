<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cust_clients".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $effective_from
 * @property string $effective_till
 * @property string $createdon
 * @property integer $status
 *
 * @property CustClientAddress[] $custClientAddresses
 * @property CustClientProfiles[] $custClientProfiles
 * @property MasterCustomer $customer
 * @property TransCarts[] $transCarts
 * @property TransSo[] $transSos
 * @property TransWishlist[] $transWishlists
 */
class CustClients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cust_clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'username', 'auth_key', 'password_hash'], 'required'],
            [['customer_id', 'status'], 'integer'],
            [['effective_from', 'effective_till', 'createdon'], 'safe'],
            [['username'], 'string', 'max' => 100],
            [['auth_key'], 'string', 'max' => 50],
            [['password_hash'], 'string', 'max' => 255],
            [['password_reset_token', 'email'], 'string', 'max' => 300],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'customer_id' => Yii::t('language', 'Customer ID'),
            'username' => Yii::t('language', 'Username'),
            'auth_key' => Yii::t('language', 'Auth Key'),
            'password_hash' => Yii::t('language', 'Password Hash'),
            'password_reset_token' => Yii::t('language', 'Password Reset Token'),
            'email' => Yii::t('language', 'Email'),
            'effective_from' => Yii::t('language', 'Effective From'),
            'effective_till' => Yii::t('language', 'Effective Till'),
            'createdon' => Yii::t('language', 'Createdon'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustClientAddresses()
    {
        return $this->hasMany(CustClientAddress::className(), ['cust_client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustClientProfiles()
    {
        return $this->hasMany(CustClientProfiles::className(), ['cust_client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransCarts()
    {
        return $this->hasMany(TransCarts::className(), ['cust_client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransSos()
    {
        return $this->hasMany(TransSo::className(), ['cust_client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransWishlists()
    {
        return $this->hasMany(TransWishlist::className(), ['cust_client_id' => 'id']);
    }
}
