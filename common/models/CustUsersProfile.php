<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "cust_users_profile".
 *
 * @property integer $id
 * @property integer $cust_user_id
 * @property string $user_firstname
 * @property string $user_middlename
 * @property string $user_lastname
 * @property string $user_picture
 * @property string $placeofbirth
 * @property string $dateofbirth
 * @property string $gender
 * @property string $marriagestatus
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 * @property CustUsers $custUser
 */
class CustUsersProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cust_users_profile';
    }

    public function behaviors()
    {
        //Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'lastmodif',
                'value'=> new Expression('NOW()'),
                //'value' => Yii::$app->formatter->asDatetime(date('yyyy-mm-dd H:i:s'))
            ],
            // [
                // 'class' => BlameableBehavior::className(),
                // 'createdByAttribute' => 'createdby',
                // 'updatedByAttribute' => 'lastmodifby',
            // ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cust_user_id', 'user_firstname', 'user_middlename', 'user_lastname', 'gender', 'marriagestatus'], 'required'],
            [['cust_user_id', 'status'], 'integer'],
            [['dateofbirth', 'createdby', 'lastmodifby'], 'safe'],
            [['user_firstname', 'user_middlename', 'user_lastname', 'placeofbirth'], 'string', 'max' => 150],
            [['user_picture'], 'string', 'max' => 100],
            [['gender'], 'string', 'max' => 1],
            [['bloodtype'], 'string', 'max' => 2],
            [['marriagestatus'], 'string', 'max' => 15],
            [['cust_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['cust_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'cust_user_id' => Yii::t('language', 'Cust User ID'),
            'user_firstname' => Yii::t('language', 'User Firstname'),
            'user_middlename' => Yii::t('language', 'User Middlename'),
            'user_lastname' => Yii::t('language', 'User Lastname'),
            'user_picture' => Yii::t('language', 'User Picture'),
            'placeofbirth' => Yii::t('language', 'Place of Birth'),
            'dateofbirth' => Yii::t('language', 'Date of Birth'),
            'gender' => Yii::t('language', 'Gender'),
            'marriagestatus' => Yii::t('language', 'Marriage Status'),
            'bloodtype' => Yii::t('language', 'Blood Type'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustUser()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'cust_user_id']);
    }

    public static function getCustprofilebyuserid($user_id)
    {
        $data = \common\models\CustUsersProfile::find()->where(['cust_user_id'=>$user_id])->one();

        return $data;
    }
	
	public function getGenderOptions() {
		return array(
				'M'=>'Male',
				'F'=>'Female',
		);
	}
	
	public function getMarriage() {
		return array(
				'Single'=>'Single',
				'Married'=>'Married',
				'Separated'=>'Separated',
		);
	}
	
	public function getBlood() {
		return array(
				'A'=>'A',
				'B'=>'B',
				'AB'=>'AB',
				'O'=>'O',
		);
	}
}
