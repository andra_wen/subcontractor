<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterNews;

/**
 * MasterNewsSearch represents the model behind the search form about `common\models\MasterNews`.
 */
class MasterNewsSearch extends MasterNews
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'page_id', 'isactive', 'sort_no', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['news_name', 'news_title', 'news_content', 'news_pict', 'news_alt', 'news_date', 'effective_from', 'effective_till', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterNews::find();

        //modification for join 2 table
        $query->joinWith(['customer','page']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['customer_id'] = [
            'asc' => ['master_customer.customer_name' => SORT_ASC],
            'desc' => ['master_customer.customer_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['page_id'] = [
            'asc' => ['master_pages.page_name' => SORT_ASC],
            'desc' => ['master_pages.page_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'isactive' => $this->isactive,
            'sort_no' => $this->sort_no,
            'news_date' => $this->news_date,
            'effective_from' => $this->effective_from,
            'effective_till' => $this->effective_till,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'news_name', $this->news_name])
            ->andFilterWhere(['like', 'news_title', $this->news_title])
            ->andFilterWhere(['like', 'news_content', $this->news_content])
            ->andFilterWhere(['like', 'news_pict', $this->news_pict])
            ->andFilterWhere(['like', 'master_customer.customer_name', $this->customer_id])
            ->andFilterWhere(['like', 'master_pages.page_name', $this->page_id])
            ->andFilterWhere(['like', 'news_alt', $this->news_alt]);

        return $dataProvider;
    }
}
