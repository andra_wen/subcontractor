<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cust_client_address".
 *
 * @property integer $id
 * @property integer $cust_client_id
 * @property string $address_name
 * @property string $address_line1
 * @property string $address_line2
 * @property integer $address_country_id
 * @property integer $address_province_id
 * @property integer $address_countytown_id
 * @property integer $address_districts_id
 * @property integer $address_postalzip
 * @property string $address_remarks
 * @property string $createdon
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustClients $custClient
 * @property SysCountry $addressCountry
 * @property SysCountytown $addressCountytown
 * @property SysDistricts $addressDistricts
 * @property SysProvince $addressProvince
 * @property TransCarts[] $transCarts
 * @property TransSoDetails[] $transSoDetails
 */
class CustClientAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cust_client_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cust_client_id', 'address_line1', 'address_country_id', 'address_province_id', 'createdon', 'lastmodif'], 'required'],
            [['cust_client_id', 'address_country_id', 'address_province_id', 'address_countytown_id', 'address_districts_id', 'address_postalzip', 'status'], 'integer'],
            [['createdon', 'lastmodif'], 'safe'],
            [['address_name'], 'string', 'max' => 50],
            [['address_line1', 'address_line2', 'address_remarks'], 'string', 'max' => 200],
            [['cust_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustClients::className(), 'targetAttribute' => ['cust_client_id' => 'id']],
            [['address_country_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysCountry::className(), 'targetAttribute' => ['address_country_id' => 'id']],
            [['address_countytown_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysCountytown::className(), 'targetAttribute' => ['address_countytown_id' => 'id']],
            [['address_districts_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysDistricts::className(), 'targetAttribute' => ['address_districts_id' => 'id']],
            [['address_province_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysProvince::className(), 'targetAttribute' => ['address_province_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'cust_client_id' => Yii::t('language', 'Cust Client ID'),
            'address_name' => Yii::t('language', 'Address Name'),
            'address_line1' => Yii::t('language', 'Address Line1'),
            'address_line2' => Yii::t('language', 'Address Line2'),
            'address_country_id' => Yii::t('language', 'Address Country ID'),
            'address_province_id' => Yii::t('language', 'Address Province ID'),
            'address_countytown_id' => Yii::t('language', 'Address Countytown ID'),
            'address_districts_id' => Yii::t('language', 'Address Districts ID'),
            'address_postalzip' => Yii::t('language', 'Address Postalzip'),
            'address_remarks' => Yii::t('language', 'Address Remarks'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustClient()
    {
        return $this->hasOne(CustClients::className(), ['id' => 'cust_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressCountry()
    {
        return $this->hasOne(SysCountry::className(), ['id' => 'address_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressCountytown()
    {
        return $this->hasOne(SysCountytown::className(), ['id' => 'address_countytown_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressDistricts()
    {
        return $this->hasOne(SysDistricts::className(), ['id' => 'address_districts_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressProvince()
    {
        return $this->hasOne(SysProvince::className(), ['id' => 'address_province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransCarts()
    {
        return $this->hasMany(TransCarts::className(), ['client_address_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransSoDetails()
    {
        return $this->hasMany(TransSoDetails::className(), ['client_address_id' => 'id']);
    }
}
