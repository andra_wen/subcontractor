<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterCustomerProfile;

/**
 * MasterCustomerProfileSearch represents the model behind the search form about `common\models\MasterCustomerProfile`.
 */
class MasterCustomerProfileSearch extends MasterCustomerProfile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'customer_country', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['customer_name', 'customer_desc', 'customer_contactperson', 'customer_contactno', 'createdon', 'lastmodif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterCustomerProfile::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'customer_country' => $this->customer_country,
            'createdby' => $this->createdby,
            'createdon' => $this->createdon,
            'lastmodifby' => $this->lastmodifby,
            'lastmodif' => $this->lastmodif,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'customer_name', $this->customer_name])
            ->andFilterWhere(['like', 'customer_desc', $this->customer_desc])
            ->andFilterWhere(['like', 'customer_contactperson', $this->customer_contactperson])
            ->andFilterWhere(['like', 'customer_contactno', $this->customer_contactno]);

        return $dataProvider;
    }
}
