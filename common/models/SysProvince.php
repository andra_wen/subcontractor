<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sys_province".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 * @property string $iso_code
 * @property integer $createdby
 * @property string $createdon
 * @property integer $lastmodifby
 * @property string $lastmodif
 * @property integer $status
 *
 * @property CustAddress[] $custAddresses
 * @property SysCountytown[] $sysCountytowns
 * @property SysCountry $country
 * @property CustUsers $createdby0
 * @property CustUsers $lastmodifby0
 * @property TransShippingcost[] $transShippingcosts
 */
class SysProvince extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys_province';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'name', 'createdby', 'createdon', 'lastmodifby', 'lastmodif'], 'required'],
            [['country_id', 'createdby', 'lastmodifby', 'status'], 'integer'],
            [['createdon', 'lastmodif'], 'safe'],
            [['name', 'iso_code'], 'string', 'max' => 50],
            [['name'], 'unique'],
            [['iso_code'], 'unique'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => SysCountry::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['lastmodifby'], 'exist', 'skipOnError' => true, 'targetClass' => CustUsers::className(), 'targetAttribute' => ['lastmodifby' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'country_id' => Yii::t('language', 'Country ID'),
            'name' => Yii::t('language', 'Name'),
            'iso_code' => Yii::t('language', 'Iso Code'),
            'createdby' => Yii::t('language', 'Createdby'),
            'createdon' => Yii::t('language', 'Createdon'),
            'lastmodifby' => Yii::t('language', 'Lastmodifby'),
            'lastmodif' => Yii::t('language', 'Lastmodif'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustAddresses()
    {
        return $this->hasMany(CustAddress::className(), ['address_province_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysCountytowns()
    {
        return $this->hasMany(SysCountytown::className(), ['province_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(SysCountry::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastmodifby0()
    {
        return $this->hasOne(CustUsers::className(), ['id' => 'lastmodifby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransShippingcosts()
    {
        return $this->hasMany(TransShippingcost::className(), ['shipping_province' => 'id']);
    }

    /**
     * @param $id
     * @return mixed
     */

    public static function getProvinceByCountry($country)
    {
        $data = \common\models\SysProvince::find()
            ->where(['country_id' => $country])
            ->select(['id', 'name'])->asArray()->orderBy([
				  'name' => SORT_ASC
				])->all();

        return $data;
    }
	
	public static function getProvince()
	{
		$data = SysProvince::find()->select(['id','name'])->asArray()->orderBy([
				  'name' => SORT_ASC
				])->all();
		return ArrayHelper::map($data,'id','name');
	}
}
