<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    //'language' => 'en_us',
    'language' => 'en-US',
    'bootstrap' => ['log'],
    'modules' => [
    ],
    'components' => [
		'request' => [
			'parsers' => [
			  'application/json' => 'yii\web\JsonParser',
			],
		],
	  'urlManager' => [
		'enablePrettyUrl' => true,
		'enableStrictParsing' => true,
		'showScriptName' => false,
		'rules' => [
		  ['class' => 'yii\rest\UrlRule', 'controller' => 'user'],
		],
	  ],
	  'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-api', 'httpOnly' => true],
        ],
    ],
    'params' => $params,
];
