<?php

namespace backend\controllers;

use Yii;

use common\models\Model;
use yii\helpers\ArrayHelper;

use common\models\CustUsers;
use common\models\CustUsersSearch;
use common\models\CustUsersProfile;
use common\models\CustUsersProfileSearch;
use common\models\CustUsersCulture;
use common\models\CustUsersCultureSearch;
use common\models\CustUsersAddress;
use common\models\CustUsersAddressSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
USE yii\widgets\ActiveForm;

/**
 * CustUsersController implements the CRUD actions for CustUsers model.
 */
class CustUsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustUsers models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new CustUsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single CustUsers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $modelCustomer = $this->findModel($id);
        $modelsAddress = $modelCustomer->custUsersAddresses;

        //$oldAddressIDs = ArrayHelper::map($modelsAddress, 'id', 'id');
        //$modelsAddress = Model::createMultiple(CustUsersAddress::classname(),$modelsAddress);
        //Model::loadMultiple($modelsAddress, Yii::$app->request->post());
        //$deletedAddressIDs = array_diff($oldAddressIDs, array_filter(ArrayHelper::map($modelsAddress, 'id', 'id')));

        //$valid = Model::validateMultiple($modelsAddress);

        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "CustUsers #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                        'modelAddress' => (empty($modelsAddress)) ? [new CustUsersAddress] : $modelsAddress,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
                'modelAddress' => (empty($modelsAddress)) ? [new CustUsersAddress] : $modelsAddress,
            ]);
        }
    }

    /**
     * Creates a new CustUsers model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new CustUsers();
        $model2 = new CustUsersProfile();
        $model3 = new CustUsersCulture();
        $modelsAddress = [new CustUsersAddress];

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new CustUsers",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,'model2'=>$model2, 'model3'=>$model3, 'modelAddress'=>$modelsAddress
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post())){
                $model->status = $model->status==1?10:0;

                if($model->validate()){
                    $model2->cust_user_id = 1;
                    $model3->cust_user_id = 1;
                    $model2->load($request->post());
                    $model2->status = $model2->status==1?10:0;
                    if($model2->validate())
                    {
                        //special for picture
                        //$model2->user_picture = $model2->user_picture=='' ? $model->oldAttributes['user_picture'] : $model2->user_picture;

                        $image = UploadedFile::getInstance($model2, 'user_picture');
                        if (!is_null($image)) {
                            $model2->user_picture = $image->name;
                            $ext = end((explode(".", $image->name)));
                            // generate a unique file name to prevent duplicate filenames
                            $model2->user_picture = Yii::$app->security->generateRandomString().".{$ext}";
                            // the path to save file, you can set an uploadPath
                            // in Yii::$app->params (as used in example below)
                            Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/userpictures/';
                            $path = Yii::$app->params['uploadPath'] . $model2->user_picture;
                            $image->saveAs($path);
                        }
                        //.special for picture
                        $model3->load($request->post());
                        $model3->status = $model3->status==1?10:0;
                        if($model3->validate() && $model->save() && ($model2->cust_user_id = $model->id) && ($model3->cust_user_id = $model->id) && $model2->save() && $model3->save())
                        {
                            $modelsAddress = Model::createMultiple(CustUsersAddress::classname());
                            Model::loadMultiple($modelsAddress, Yii::$app->request->post());
                            foreach ($modelsAddress as $modelAddress) {
                                $modelAddress->cust_user_id = $model->id;
                                $modelAddress->status = 10;
                                $modelAddress->address_default = $modelAddress->address_default;
                                $modelAddress->save(false);
                            }

                            return [
                                'forceReload'=>'#crud-datatable-pjax',
                                'title'=> "Create new CustUsers",
                                'content'=>'<span class="text-success">Create CustUsers success</span>',
                                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
                            ];
                        }
                        else
                        {
                            return [
                                'title'=> "Create new Customer Users",
                                'content'=>$this->renderAjax('create', [
                                    'model' => $model,'model2' => $model2, 'model3' => $model3, 'modelAddress'=>$modelsAddress
                                ]),
                                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                            ];
                        }
                    } else {
                        return [
                            'title'=> "Create new Customer Users",
                            'content'=>$this->renderAjax('create', [
                                'model' => $model,'model2'=>$model2, 'model3'=>$model3
                            ]),
                            'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                        ];
                    }
                }else{
                    return [
                        'title'=> "Create new Customer Users",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model,'model2'=>$model2,'model3'=>$model3, 'modelAddress'=>$modelsAddress
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                    ];
                }
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post())) {
                $model->status = $model->status==1?10:0;
                $model2->status = $model2->status==1?10:0;
                $model3->status = $model3->status==1?10:0;
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } else {
                return $this->render('create', [
                    'model' => $model,'model2'=>$model2,'model3'=>$model3,'modelAddress'=>$modelsAddress
                ]);
            }
        }
       
    }

    public function actionEdit($id)
    {
        $model = $this->findModel($id);
        $request = Yii::$app->request;
        $modelsAddress = $model->custUsersAddresses;

        if(CustUsersProfile::find()->where(['cust_user_id' => $id])->one())
        {
            $model2 = CustUsersProfile::find()->where(['cust_user_id' => $id])->one();
        }
        else
        {
            $model2 = new CustUsersProfile();
            $model2->cust_user_id = $id;
            $model2->user_firstname = ' ';
            $model2->user_middlename = ' ';
            $model2->user_lastname = ' ';
            $model2->gender = 'M';
            if($model2->validate() && $model2->save())
            {
                $model2 = CustUsersProfile::find()->where(['cust_user_id' => $id])->one();
            }
        }

        if(CustUsersCulture::find()->where(['cust_user_id' => $id])->one())
        {
            $model3 = CustUsersCulture::find()->where(['cust_user_id' => $id])->one();
        }
        else
        {
            $model3 = new CustUsersCulture();
            $model3->cust_user_id = $id;
            $model3->themes_color = 'skin-blue-light';
            $model3->language = 'en-US';
            $model3->country = 95;
            if($model3->validate() && $model3->save())
            {
                $model3 = CustUsersCulture::find()->where(['cust_user_id' => $id])->one();
            }
        }

        if($model->load($request->post()))
        {
            if($model->validate()){
                $model2->cust_user_id = $id;
                $model3->cust_user_id = $id;
                $model2->load($request->post());
                //special for picture
                $model2->user_picture = $model2->user_picture=='' ? $model2->oldAttributes['user_picture'] : $model2->user_picture;
                if($model2->validate())
                {
                    $image = UploadedFile::getInstance($model2, 'user_picture');
                    if (!is_null($image)) {
                        //$model2->user_picture = $image->name;
                        $ext = end((explode(".", $image->name)));
                        // generate a unique file name to prevent duplicate filenames
                        $model2->user_picture = Yii::$app->security->generateRandomString().".{$ext}";
                        // the path to save file, you can set an uploadPath
                        // in Yii::$app->params (as used in example below)
                        Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/userpictures/';
                        $path = Yii::$app->params['uploadPath'] . $model2->user_picture;
                        $image->saveAs($path);
                    }
                    //.special for picture
                    $model3->load($request->post());
                    if($model3->validate() && $model->save() && $model2->save() && $model3->save())
                    {
                        $oldAddressIDs = ArrayHelper::map($modelsAddress, 'id', 'id');
                        $modelsAddress = Model::createMultiple(CustUsersAddress::classname(), $modelsAddress);
                        Model::loadMultiple($modelsAddress, Yii::$app->request->post());
                        $deletedAddressIDs = array_diff($oldAddressIDs, array_filter(ArrayHelper::map($modelsAddress, 'id', 'id')));
                        $valid = Model::validateMultiple($modelsAddress);
                        if (!empty($deletedAddressIDs)) {
                            CustUsersAddress::deleteAll(['id' => $deletedAddressIDs]);
                        }
                        foreach ($modelsAddress as $modelAddress) {
                            $modelAddress->cust_user_id = $model->id;
                            $modelAddress->status = 10;
                            $modelAddress->save(false);
                        }

                        return $this->render('edit', [
                            'model' => $model, 'model2' => $model2, 'model3' => $model3,
                            'modelAddress' => (empty($modelsAddress)) ? [new CustUsersAddress] : $modelsAddress,
                        ]);
                    }
                    else
                    {
                        return $this->render('edit', [
                            'model' => $model, 'model2' => $model2, 'model3' => $model3,
                            'modelAddress' => (empty($modelsAddress)) ? [new CustUsersAddress] : $modelsAddress,
                        ]);
                    }
                }
                else
                {
                    return $this->render('edit', [
                        'model' => $model, 'model2' => $model2, 'model3' => $model3,
                        'modelAddress' => (empty($modelsAddress)) ? [new CustUsersAddress] : $modelsAddress,
                    ]);
                }
            }
            else
            {
                return $this->render('edit', [
                    'model' => $model, 'model2' => $model2, 'model3' => $model3,
                    'modelAddress' => (empty($modelsAddress)) ? [new CustUsersAddress] : $modelsAddress,
                ]);
            }
        }
        else {
            return $this->render('edit', [
                'model' => $model, 'model2' => $model2, 'model3' => $model3,
                'modelAddress' => (empty($modelsAddress)) ? [new CustUsersAddress] : $modelsAddress,
            ]);
        }
    }

    /**
     * Updates an existing CustUsers model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $modelsAddress = $model->custUsersAddresses;

        //$model2 = new CustUsersProfile();
        //$model3 = new CustUsersCulture();
        if(CustUsersProfile::find()->where(['cust_user_id' => $id])->one())
        {
            $model2 = CustUsersProfile::find()->where(['cust_user_id' => $id])->one();
        }
        else
        {
            $model2 = new CustUsersProfile();
            $model2->cust_user_id = $id;
            $model2->user_firstname = ' ';
            $model2->user_middlename = ' ';
            $model2->user_lastname = ' ';
            $model2->gender = 'M';
            if($model2->validate() && $model2->save())
            {
                $model2 = CustUsersProfile::find()->where(['cust_user_id' => $id])->one();
            }
        }

        if(CustUsersCulture::find()->where(['cust_user_id' => $id])->one())
        {
            $model3 = CustUsersCulture::find()->where(['cust_user_id' => $id])->one();
        }
        else
        {
            $model3 = new CustUsersCulture();
            $model3->cust_user_id = $id;
            $model3->themes_color = 'skin-blue-light';
            $model3->language = 'en-US';
            $model3->country = 95;
            if($model3->validate() && $model3->save())
            {
                $model3 = CustUsersCulture::find()->where(['cust_user_id' => $id])->one();
            }
        }

        $model->status = $model->status==10?1:0;
        $model2->status = $model2->status==10?1:0;
        $model3->status = $model3->status==10?1:0;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update CustUsers #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model, 'model2' => $model2, 'model3' => $model3,
                        'modelAddress' => (empty($modelsAddress)) ? [new CustUsersAddress] : $modelsAddress,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }
            else if($model->load($request->post()))
            {
                $model->status = $model->status==1?10:0;
                if($model->validate()){
                    $model2->cust_user_id = $id;
                    $model3->cust_user_id = $id;
                    $model2->load($request->post());
                    $model2->status = $model2->status==1?10:0;
                    //special for picture
                    $model2->user_picture = $model2->user_picture=='' ? $model2->oldAttributes['user_picture'] : $model2->user_picture;
                    if($model2->validate())
                    {
                        $image = UploadedFile::getInstance($model2, 'user_picture');
                        if (!is_null($image)) {
                            $model2->user_picture = $image->name;
                            $ext = end((explode(".", $image->name)));
                            // generate a unique file name to prevent duplicate filenames
                            $model2->user_picture = Yii::$app->security->generateRandomString().".{$ext}";
                            // the path to save file, you can set an uploadPath
                            // in Yii::$app->params (as used in example below)
                            Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/userpictures/';
                            $path = Yii::$app->params['uploadPath'] . $model2->user_picture;
                            $image->saveAs($path);
                        }
                        //.special for picture
                        $model3->load($request->post());
                        $model3->status = $model3->status==1?10:0;
                        if($model3->validate() && $model->save() && $model2->save() && $model3->save())
                        {
                            $oldAddressIDs = ArrayHelper::map($modelsAddress, 'id', 'id');
                            $modelsAddress = Model::createMultiple(CustUsersAddress::classname(), $modelsAddress);
                            Model::loadMultiple($modelsAddress, Yii::$app->request->post());
                            $deletedAddressIDs = array_diff($oldAddressIDs, array_filter(ArrayHelper::map($modelsAddress, 'id', 'id')));
                            $valid = Model::validateMultiple($modelsAddress);
                            if (!empty($deletedAddressIDs)) {
                                CustUsersAddress::deleteAll(['id' => $deletedAddressIDs]);
                            }
                            foreach ($modelsAddress as $modelAddress) {
                                $modelAddress->cust_user_id = $model->id;
                                $modelAddress->status = 10;
                                $modelAddress->save(false);
                            }

                            return [
                                'forceReload'=>'#crud-datatable-pjax',
                                'title'=> "CustUsers #".$id,
                                'content'=>$this->renderAjax('view', [
                                    'model' => $model,
                                    'modelAddress' => (empty($modelsAddress)) ? [new CustUsersAddress] : $modelsAddress,
                                ]),
                                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                            ];
                        }
                        else
                        {
                            //$model->status = $model->status==1?10:0;
                            return [
                                'title'=> "Update CustUsers #".$id,
                                'content'=>$this->renderAjax('update', [
                                    'model' => $model, 'model2' => $model2, 'model3' => $model3,
                                    'modelAddress' => (empty($modelsAddress)) ? [new CustUsersAddress] : $modelsAddress,
                                ]),
                                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                            ];
                        }
                    }
                    else
                    {
                        //$model->status = $model->status==1?10:0;
                        return [
                            'title'=> "Update CustUsers #".$id,
                            'content'=>$this->renderAjax('update', [
                                'model' => $model, 'model2' => $model2, 'model3' => $model3,
                                'modelAddress' => (empty($modelsAddress)) ? [new CustUsersAddress] : $modelsAddress,
                            ]),
                            'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                        ];
                    }
                }
                else
                {
                    //$model->status = $model->status==1?10:0;
                    return [
                        'title'=> "Update CustUsers #".$id,
                        'content'=>$this->renderAjax('update', [
                            'model' => $model, 'model2' => $model2, 'model3' => $model3,
                            'modelAddress' => (empty($modelsAddress)) ? [new CustUsersAddress] : $modelsAddress,
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                    ];
                }
            }

        }else{
            /*
            *   Process for non-ajax request
            */
            $model->status = $model->status==1?10:0;
            if ($model->load($request->post()))
            {
                $model->status = $model->status==1?10:0;
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } else {
                return $this->render('update', [
                    'model' => $model, 'model2' => $model2, 'model3' => $model3,
                    'modelAddress' => (empty($modelsAddress)) ? [new CustUsersAddress] : $modelsAddress,
                ]);
            }
        }
    }

    /**
     * Delete an existing CustUsers model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing CustUsers model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the CustUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustUsers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
