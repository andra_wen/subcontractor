<?php

namespace backend\controllers;

use Yii;
use common\models\MasterFiles;
use common\models\MasterFilesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * MasterFilesController implements the CRUD actions for MasterFiles model.
 */
class MasterFilesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterFiles models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new MasterFilesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single MasterFiles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "MasterFiles #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new MasterFiles model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new MasterFiles();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new MasterFiles",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()))
            {
                $model->status = $model->status==1?10:0;
                $model->isactive = $model->isactive==1?10:0;

                $image = UploadedFile::getInstance($model, 'file_pict');
                if (!is_null($image)) {
                    $model->file_pict = $image->name;
                    $ext = end((explode(".", $image->name)));
                    // generate a unique file name to prevent duplicate filenames
                    $model->file_pict = Yii::$app->security->generateRandomString().".{$ext}";
                    // the path to save file, you can set an uploadPath
                    // in Yii::$app->params (as used in example below)
                    Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/file_pictures/';
                    $path = Yii::$app->params['uploadPath'] . $model->file_pict;
                    $image->saveAs($path);
                }

                $thefiles = UploadedFile::getInstance($model, 'the_files');
                if (!is_null($thefiles)) {
                    $model->the_files = $thefiles->name;
                    $ext = end((explode(".", $thefiles->name)));
                    // generate a unique file name to prevent duplicate filenames
                    $model->the_files = Yii::$app->security->generateRandomString().".{$ext}";
                    // the path to save file, you can set an uploadPath
                    // in Yii::$app->params (as used in example below)
                    Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/file_folder/';
                    $path = Yii::$app->params['uploadPath'] . $model->the_files;
                    $thefiles->saveAs($path);
                }

                if($model->validate() && $model->save())
                {
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Create new MasterFiles",
                        'content'=>'<span class="text-success">Create MasterFiles success</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                    ];
                }
                else
                {
                    return [
                        'title'=> "Create new MasterFiles",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                    ];
                }

            }else{           
                return [
                    'title'=> "Create new MasterFiles",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing MasterFiles model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->status = $model->status==10?1:0;
        $model->isactive = $model->isactive==10?1:0;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update MasterFiles #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()))
            {
                $model->status = $model->status==1?10:0;
                $model->isactive = $model->isactive==1?10:0;

                if($model->validate())
                {
                    $model->file_pict = $model->file_pict=='' ? $model->oldAttributes['file_pict'] : $model->file_pict;
                    $image = UploadedFile::getInstance($model, 'file_pict');
                    if (!is_null($image)) {
                        $model->file_pict = $image->name;
                        $ext = end((explode(".", $image->name)));
                        // generate a unique file name to prevent duplicate filenames
                        $model->file_pict = Yii::$app->security->generateRandomString().".{$ext}";
                        // the path to save file, you can set an uploadPath
                        // in Yii::$app->params (as used in example below)
                        Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/file_pictures/';
                        $path = Yii::$app->params['uploadPath'] . $model->file_pict;
                        $image->saveAs($path);
                    }

                    $model->the_files = $model->the_files=='' ? $model->oldAttributes['the_files'] : $model->the_files;
                    $thefiles = UploadedFile::getInstance($model, 'the_files');
                    if (!is_null($thefiles)) {
                        $model->the_files = $thefiles->name;
                        $ext = end((explode(".", $thefiles->name)));
                        // generate a unique file name to prevent duplicate filenames
                        $model->the_files = Yii::$app->security->generateRandomString().".{$ext}";
                        // the path to save file, you can set an uploadPath
                        // in Yii::$app->params (as used in example below)
                        Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/file_folder/';
                        $path = Yii::$app->params['uploadPath'] . $model->the_files;
                        $thefiles->saveAs($path);
                    }

                    if($model->save())
                    {
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "MasterFiles #".$id,
                            'content'=>$this->renderAjax('view', [
                                'model' => $model,
                            ]),
                            'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                        ];
                    }
                    else
                    {
                        return [
                            'title'=> "Update MasterFiles #".$id,
                            'content'=>$this->renderAjax('update', [
                                'model' => $model,
                            ]),
                            'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                        ];
                    }
                }
                else
                {
                    return [
                        'title'=> "Update MasterFiles #".$id,
                        'content'=>$this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                    ];
                }

            }else{
                 return [
                    'title'=> "Update MasterFiles #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing MasterFiles model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing MasterFiles model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the MasterFiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterFiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterFiles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
