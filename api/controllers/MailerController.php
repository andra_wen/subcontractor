<?php
/**
 * Created by JetBrains PhpStorm.
 * Framwork: Yii2
 * User: andrawen
 * Date: 1/27/18
 * Time: 7:36 PM
 * To change this template use File | Settings | File Templates.
 */
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\MailerForm;

/**
 * Site controller
 */
class MailerController extends Controller
{
    // ...existing code...
    /*public function actionIndex()
    {
        return $this->render('index');
    }
    */

    public function actionIndex()
    {
        $model = new MailerForm();
        $result = [];

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            // valid data received in $model

            //$model->save();

            // do something meaningful here about $model ...
            $name = $_POST["MailerForm"]["name"];
            $email = $_POST["MailerForm"]["email"];

            foreach($name as $key => $val){ // Loop though one array
                $val2 = $email[$key]; // Get the values from the other array
                $result[$key] = [$val,$val2]; // combine 'em
            }

            foreach($result as $index => $item){
                Yii::$app->mailer->compose([
                        //'@common/mail/layouts/email-orderOnProgress.php'
                        'html'=>'@common/mail/timisimi-emails/email-orderReceived.html'],
                    [
                        // 'data' => $item,
                        'image' => Yii::getAlias('@backend/web/uploads/pages/logo.png'),
                    ])
                    ->setFrom('noreply@singsub.com')
                    ->setTo($item[1])
                    ->setSubject('Test Email'.$item[0])
                    ->send();
            }


            return $this->render('index', ['model' => $model,'result'=>$result]);
        }
        else
        {
            // either the page is initially displayed or there is some validation error
            return $this->render('index', ['model' => $model,'result'=>$result]);
        }
    }
}