<?php

namespace backend\controllers;

use Yii;

use common\models\Model;
use yii\helpers\ArrayHelper;

use common\models\TransSo;
use common\models\TransSoSearch;
use common\models\TransSoDetails;
use common\models\TransSoDetailsSearch;
use common\models\TransSoConfirmation;
use common\models\TransSoConfirmationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
USE yii\widgets\ActiveForm;

/**
 * TransSoController implements the CRUD actions for TransSo model.
 */
class TransSoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TransSo models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new TransSoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single TransSo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $modelSO = $this->findModel($id);
        $modelsDetails = $modelSO->transSoDetails;
        $exist = TransSoConfirmation::find()->where(['so_id' => $id])->one();

        if($exist)
        {
            $model2 = TransSoConfirmation::find()->where(['so_id' => $id])->one();
        }
        else
        {
            $model2 = new TransSoConfirmation();
        }

        //$status = \common\models\TransSo::getSodetailbyid($id);
        //var_dump($status);

        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "TransSo #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id), 'model2' => $model2,
                        'modelDetails' => (empty($modelsDetails)) ? [new TransSoDetails()] : $modelsDetails,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id), 'model2' => $model2,
                'modelDetails' => (empty($modelsDetails)) ? [new TransSoDetails()] : $modelsDetails,
            ]);
        }
    }

    /**
     * Creates a new TransSo model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new TransSo();
        $model2 = new TransSoConfirmation();
        $modelsDetails = [new TransSoDetails];

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new TransSo",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model, 'model2'=>$model2, 'modelDetails'=>$modelsDetails,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post())){
                $model->status = $model->status==1?10:0;
                if($model->validate())
                {
                    $model2->load($request->post());
                    $model2->status = $model2->status==1?10:0;
                    $model2->so_id = 1;
                    if($model2->validate() && $model->save() && ($model2->so_id = $model->id) && $model2->save())
                    {
                        $modelsDetails = Model::createMultiple(TransSoDetails::classname());
                        Model::loadMultiple($modelsDetails, Yii::$app->request->post());
                        foreach ($modelsDetails as $modelDetails) {
                            $modelDetails->so_id = $model->id;
                            $modelDetails->save(false);
                        }

                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "Create new TransSo",
                            'content'=>'<span class="text-success">Create TransSo success</span>',
                            'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                        ];
                    }
                    else
                    {
                        $model2->status = $model2->status==10?1:0;
                        return [
                            'title'=> "Create new TransSo",
                            'content'=>$this->renderAjax('create', [
                                'model' => $model, 'modelDetails'=>$modelsDetails,
                            ]),
                            'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                        ];
                    }
                }
                else
                {
                    return [
                        'title'=> "Create new TransSo",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model, 'modelDetails'=>$modelsDetails,
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                    ];
                }
            }else{
                return [
                    'title'=> "Create new TransSo",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,  'modelDetails'=>$modelsDetails,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model, 'modelDetails'=>$modelsDetails,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing TransSo model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $modelsDetails = $model->transSoDetails;

        if(TransSoConfirmation::find()->where(['so_id' => $id])->one())
        {
            $model2 = TransSoConfirmation::find()->where(['so_id' => $id])->one();
        }
        else
        {
            $model2 = new TransSoConfirmation();
        }

        $model->status = $model->status==10?1:0;
        $model2->status = $model2->status==10?1:0;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet)
            {
                return [
                    'title'=> "Update TransSo #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model, 'model2'=> $model2,
                        'modelDetails' => (empty($modelsDetails)) ? [new TransSoDetails()] : $modelsDetails,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }
            else if($model->load($request->post()))
            {
                if($model->validate())
                {
                    $model2->load($request->post());
                    $model2->so_id = $id;
                    $model2->status = $model2->status==1?10:0;

                    if($model2->validate())
                    {
                        if($model2->save() && $model->save())
                        {
                            $oldDetailsIDs = ArrayHelper::map($modelsDetails, 'id', 'id');
                            $modelsDetails = Model::createMultiple(TransSoDetails::classname(), $modelsDetails);
                            Model::loadMultiple($modelsDetails, Yii::$app->request->post());
                            $deletedDetailsIDs = array_diff($oldDetailsIDs, array_filter(ArrayHelper::map($modelsDetails, 'id', 'id')));
                            $valid = Model::validateMultiple($modelsDetails);
                            if (!empty($deletedDetailsIDs)) {
                                CustUsersAddress::deleteAll(['id' => $deletedDetailsIDs]);
                            }
                            foreach ($modelsDetails as $modelDetails) {
                                $modelDetails->so_id = $model->id;
                                $modelDetails->status = 10;
                                $modelDetails->save(false);
                            }

                            if($model->so_statuscode==6)
                            {
                                //proses mengirim email kalau sudah siap dikirim
                                $status = \common\models\TransSo::getSodetailbyid($id);
                                Yii::$app->mailer->compose([
                                        //'@common/mail/layouts/email-orderOnProgress.php'
                                        'html'=>'@common/mail/timisimi-emails/email-orderOnProgress.html'],
                                    [
                                        'so_details' => $status,
                                        'image' => Yii::getAlias('@backend/web/uploads/pages/logo.png'),
                                    ])
                                    ->setFrom('noreply@prajnamaitreya.com')
                                    ->setTo($status[0]['email'])
                                // ->setCc('andra.wisata@kamadjaja.com')
                                    ->setSubject('Timisimi - Order Status '.$status[0]['so_no'])
                                    ->send();
                            }
                            else if($model->so_statuscode==7)
                            {
                                //proses mengirim email kalau sudah terkirim
                                $status = \common\models\TransSo::getSodetailbyid($id);
                                Yii::$app->mailer->compose([
                                        //'@common/mail/layouts/email-orderOnProgress.php'
                                        'html'=>'@common/mail/timisimi-emails/email-orderReceived.html'],
                                    [
                                        'so_details' => $status,
                                        'image' => Yii::getAlias('@backend/web/uploads/pages/logo.png'),
                                    ])
                                    ->setFrom('noreply@prajnamaitreya.com')
                                    ->setTo($status[0]['email'])
                                // ->setCc('andra.wisata@kamadjaja.com')
                                    ->setSubject('Timisimi - Order Status '.$status[0]['so_no'])
                                    ->send();
                            }

                            return [
                                'forceReload'=>'#crud-datatable-pjax',
                                'title'=> "TransSo #".$id,
                                'content'=>$this->renderAjax('view', [
                                    'model' => $model, 'model2'=> $model2,
                                    'modelDetails' => (empty($modelsDetails)) ? [new TransSoDetails()] : $modelsDetails,
                                ]),
                                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                            ];
                        }
                        else
                        {
                            return [
                                'title'=> "Update TransSo #".$id,
                                'content'=>$this->renderAjax('update', [
                                    'model' => $model, 'model2'=> $model2,
                                    'modelDetails' => (empty($modelsDetails)) ? [new TransSoDetails()] : $modelsDetails
                                ]),
                                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                            ];
                        }
                    }
                    else
                    {
                        return [
                            'title'=> "Update TransSo #".$id,
                            'content'=>$this->renderAjax('update', [
                                'model' => $model, 'model2'=> $model2,
                                'modelDetails' => (empty($modelsDetails)) ? [new TransSoDetails()] : $modelsDetails
                            ]),
                            'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                        ];
                    }
                }
                else
                {
                    return [
                        'title'=> "Update TransSo #".$id,
                        'content'=>$this->renderAjax('update', [
                            'model' => $model, 'model2'=> $model2,
                            'modelDetails' => (empty($modelsDetails)) ? [new TransSoDetails()] : $modelsDetails
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                    ];
                }
            }
            else
            {
                 return [
                    'title'=> "Update TransSo #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model, 'model2'=> $model2,
                        'modelDetails' => (empty($modelsDetails)) ? [new TransSoDetails()] : $modelsDetails
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model, 'model2'=> $model2,
                    'modelDetails' => (empty($modelsDetails)) ? [new TransSoDetails()] : $modelsDetails
                ]);
            }
        }
    }

    /**
     * Delete an existing TransSo model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing TransSo model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the TransSo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TransSo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TransSo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
