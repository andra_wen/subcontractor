<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use vova07\select2\Widget;
use kartik\widgets\SwitchInput;
use kartik\widgets\DateTimePicker;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use lajax\translatemanager\models\Language;
use yii\web\JsExpression;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\widgets\DepDrop;


/* @var $this yii\web\View */
/* @var $model common\models\CustUsers */
/* @var $form yii\widgets\ActiveForm */

$js = 'jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {

    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {

        jQuery(this).html("Address: " + (index + 1))

    });

});


jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {

    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {

        jQuery(this).html("Address: " + (index + 1))

    });

});';


$this->registerJs($js);

?>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Login Info</a></li>
        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Basic Profile Info</a></li>
        <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Culture</a></li>
        <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Address</a></li>
    </ul>

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <div class="cust-users-form">

                <?php $dataCustomer = ArrayHelper::map(\common\models\MasterCustomer::find()->where(['status'=>10])->all(), 'id', 'customer_name'); ?>
                <?=
                $form->field($model, 'customer_id')->widget(Widget::classname(),[
                    'items'=> $dataCustomer,
                    'options'=>[
                        'placeholder'=> 'Select Customer...'
                    ],
                    'events' => [
                        'select2-open' => 'function (e) { log("select2:open", e); }',
                        'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
                    ],
                    'settings' => [
                        'width' => '100%',
                    ],
                ]);
                ?>

                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'effective_from')->widget(DatetimePicker::classname(),
                [
                    'name'=>'effective_from',
                    'value'=> date('yyyy-mm-dd H:i:s'),
                    'options'=>['placeholder'=>'Select a date...'],
                    'pluginOptions'=>
                    [
                        'format'=>'yyyy-mm-dd H:i:s',
                        'todayHighlight'=>true,
                        'autoclose'=>true,
                    ]
                ]);
                ?>

                <?= $form->field($model, 'effective_till')->widget(DatetimePicker::classname(),
                [
                    'name'=>'effective_till',
                    'value'=> date('yyyy-mm-dd H:i:s'),
                    'options'=>['placeholder'=>'Select a date...'],
                    'pluginOptions'=>
                    [
                        'format'=>'yyyy-mm-dd H:i:s',
                        'todayHighlight'=>true,
                        'autoclose'=>true,
                    ]
                ]);
                ?>

                <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
                [
                    'pluginOptions'=>[
                        'onText'=>'Active',
                        'offText'=>'Inactive',
                    ],
                ]);
                ?>
            </div>
        </div>
        <!-- /.tab-panel -->

        <div class="tab-pane" id="tab_2">
            <div class="cust-users-profile-form">

                <?= $form->field($model2, 'user_firstname')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model2, 'user_middlename')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model2, 'user_lastname')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model2, 'user_picture')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'showCaption' => false,
                    'showRemove' => false,
                    'showUpload' => false,
                    'browseClass' => 'btn btn-primary btn-block',
                    'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                    'browseLabel' =>  'Select Photo',
                    'initialPreview'=>[
                        Html::img("@web/uploads/userpictures/" . $model2->user_picture,['width'=>100])
                    ],
                    'overwriteInitial'=>true
                ],
                ]); ?>

                <?= $form->field($model2, 'placeofbirth')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model2, 'dateofbirth')->widget(DatePicker::classname(),
                [
                    'name'=>'dateofbirth',
                    'value'=> date('yyyy-mm-dd'),
                    'options'=>['placeholder'=>'Select a date...'],
                    'pluginOptions'=>
                    [
                        'format'=>'yyyy-mm-dd',
                        'todayHighlight'=>true,
                        'autoclose'=>true,
                    ]
                ]);
                ?>

                <?php $dataGender = ['M'=>Yii::t('language', 'Male'),'F'=>Yii::t('language', 'Female')]; ?>
                <?=
                $form->field($model2, 'gender')->widget(Widget::classname(),[
                    'items'=> $dataGender,
                    'options'=>['placeholder'=> 'Select Gender...'],
                    'settings' => [
                        'width' => '100%',
                    ],
                ]);
                ?>

                <?php $mariageData = ['Single'=>Yii::t('language', 'Single'),'Married'=>Yii::t('language', 'Married'),'Separated'=>Yii::t('language', 'Separated')]; ?>
                <?=
                $form->field($model2, 'marriagestatus')->widget(Widget::classname(),[
                    'items'=> $mariageData,
                    'options'=>['placeholder'=> 'Select Mariage status...'],
                    'settings' => [
                        'width' => '100%',
                    ],
                ]);
                ?>



                <?php $bloodData = ['A', 'A', 'B'=>'B', 'AB'=>'AB', 'O'=>'O']; ?>
                <?=
                $form->field($model2, 'bloodtype')->widget(Widget::classname(),[
                    'items'=> $bloodData,
                    'options'=>['placeholder'=> 'Select Blood type...'],
                    'settings' => [
                        'width' => '100%',
                    ],
                ]);
                ?>

            </div>
        </div>
        <!-- /.tab-panel -->

        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_3">
            <div class="cust-culture-form">
                <?php $themesList = [
                'skin-blue'=>Yii::t('language', 'Skin Blue'),
                'skin-blue-light'=>Yii::t('language', 'Skin Blue Light'),
                'skin-yellow'=>Yii::t('language', 'Skin Yellow'),
                'skin-yellow-light'=>Yii::t('language', 'Skin Yellow Light'),
                'skin-green'=>Yii::t('language', 'Skin Green'),
                'skin-green-light'=>Yii::t('language', 'Skin Green Light'),
                'skin-purple'=>Yii::t('language', 'Skin Purple'),
                'skin-purple-light'=>Yii::t('language', 'Skin Purple Light'),
                'skin-red'=>Yii::t('language', 'Skin Red'),
                'skin-red-light'=>Yii::t('language', 'Skin Red Light'),
                'skin-black'=>Yii::t('language', 'Skin Black'),
                'skin-black-light'=>Yii::t('language', 'Skin Black Light')
                    ]; ?>
                <?=
                $form->field($model3, 'themes_color')->widget(Widget::classname(),[
                    'items'=> $themesList,
                    'options'=>['placeholder'=> 'Select Themes...'],
                    'settings' => [
                        'width' => '100%',
                    ],
                ]);
                ?>

                <?php $dataLanguage = ArrayHelper::map(lajax\translatemanager\models\Language::find()->all(), 'language_id', 'name'); ?>
                <?=
                $form->field($model3, 'language')->widget(Widget::classname(),[
                    'items'=> $dataLanguage,
                    'options'=>['placeholder'=> 'Select Language...'],
                    'settings' => [
                        'width' => '100%',
                    ],
                ]);
                ?>

                <?php $dataCountry = ArrayHelper::map(\common\models\SysCountry::find()->all(), 'id', 'short_name'); ?>
                <?=
                $form->field($model3, 'country')->widget(Widget::classname(),[
                    'items'=> $dataCountry,
                    'options'=>['placeholder'=> 'Select Country...'],
                    'settings' => [
                        'width' => '100%',
                    ],
                ]);
                ?>

                <?php $formatList = [
                'dd/mm/yy'=>'dd/mm/yy',
                'dd/mm/yyyy'=>'dd/mm/yyyy',
                'dd-mm-yy'=>'dd-mm-yy',
                'yy/mm/dd'=>'yy/mm/dd',
                'yyyy/mm/dd'=>'yyyy/mm/dd',
                'yy-mm-dd'=>'yy-mm-dd',
                'yyyy-mm-dd'=>'yyyy-mm-dd'
                ]; ?>
                <?=
                $form->field($model3, 'dateFormat')->widget(Widget::classname(),[
                    'items'=> $formatList,
                    'options'=>['placeholder'=> 'Select Format...'],
                    'settings' => [
                        'width' => '100%',
                    ],
                ]);
                ?>

                <?= $form->field($model3, 'user_currency')->textInput() ?>

                <?php  $form->field($model3, 'status')->widget(SwitchInput::classname(),
                [
                    'pluginOptions'=>[
                        'onText'=>'Active',
                        'offText'=>'Inactive',
                    ],
                ]);
                ?>
            </div>
        </div>
        <!-- /.tab-content -->

        <div class="tab-pane" id="tab_4">
            <div class="cust-address-form">
                <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 0, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelAddress[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'address_name',
                    'address_line1',
                    'address_line2',
                    'address_phonenumber',
                    'address_country_id',
                    'address_province_id',
                    'address_countytown_id',
                    'address_districts_id',
                    'address_postalzip',
                    'address_default',
                    'address_remarks',
                ],
                ]); ?>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <i class="glyphicon glyphicon-envelope"></i> Addresses
                            <button type="button" class="add-item btn btn-success btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> Add</button>
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="container-items"><!-- widgetBody -->
                            <?php foreach ($modelAddress as $i => $modelsAddress): ?>
                                <div class="item panel panel-default"><!-- widgetItem -->
                                    <div class="panel-heading">
                                        <h3 class="panel-title pull-left">Address</h3>
                                        <div class="pull-right">
                                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                        // necessary for update action.
                                        if (!$modelsAddress->isNewRecord) {
                                            echo Html::activeHiddenInput($modelsAddress, "[{$i}]id");
                                        }
                                        ?>
                                        <?= $form->field($modelsAddress, "[{$i}]address_name")->textInput(['maxlength' => true]) ?>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <?= $form->field($modelsAddress, "[{$i}]address_line1")->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $form->field($modelsAddress, "[{$i}]address_line2")->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $form->field($modelsAddress, "[{$i}]address_phonenumber")->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $form->field($modelsAddress, "[{$i}]address_default")->widget(SwitchInput::classname(),
                                                [
                                                    'pluginOptions'=>[
                                                        'onText'=>'Yes',
                                                        'offText'=>'No',
                                                    ],
                                                ]);
                                                ?>
                                            </div>
                                        </div><!-- .row -->
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <!--<?= $form->field($modelsAddress, "[{$i}]address_country_id")->textInput(['maxlength' => true]) ?>-->

                                                <?php
                                                $dataCountry = \common\models\SysCountry::find()->orderBy('short_name')->all();
                                                $listDataCountry = \yii\helpers\ArrayHelper::map($dataCountry, 'id', 'short_name');
                                                ?>
                                                <?= $form->field($modelsAddress, "[{$i}]address_country_id")->dropDownList($listDataCountry,
                                                [
                                                    'prompt' => Yii::t('language', 'Select Country...'),
                                                    'id' => "custusersaddress-$i-address_country_id",
                                                ]); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <!--<?= $form->field($modelsAddress, "[{$i}]address_province_id")->textInput(['maxlength' => true]) ?>-->

                                                <?php
                                                $dataProvince = \common\models\SysProvince::find()->orderBy('name')->all();
                                                $listDataProvince = \yii\helpers\ArrayHelper::map($dataProvince, 'id', 'name');
                                                ?>
                                                <?= $form->field($modelsAddress, "[{$i}]address_province_id")->widget(DepDrop::classname(), [
                                                'options' => ['id' => "custusersaddress-$i-address_province_id"],
                                                'data'=>$listDataProvince,
                                                'pluginOptions' => [
                                                    'depends' => ["custusersaddress-$i-address_country_id"],
                                                    'placeholder' => Yii::t('language', 'Select Province...'),
                                                    'url' => Url::to(['/sys-province/provincelist'])
                                                ]
                                                ]); ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <!--<?= $form->field($modelsAddress, "[{$i}]address_countytown_id")->textInput(['maxlength' => true]) ?>-->

                                                <?php
                                                $dataCountyTown = \common\models\SysCountytown::find()->orderBy('name')->all();
                                                $listDataCountyTown = \yii\helpers\ArrayHelper::map($dataCountyTown, 'id', 'name');
                                                ?>

                                                <?= $form->field($modelsAddress, "[{$i}]address_countytown_id")->widget(DepDrop::classname(), [
                                                'options' => ['id' => "custusersaddress-$i-address_countytown_id"],
                                                'data'=>$listDataCountyTown,
                                                'pluginOptions' => [
                                                    'depends' => ["custusersaddress-$i-address_country_id", "custusersaddress-$i-address_province_id"],
                                                    'placeholder' => Yii::t('language', 'Select Countytown..'),
                                                    'url' => Url::to(['/sys-countytown/countylist'])
                                                ]
                                                ]); ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <!--<?= $form->field($modelsAddress, "[{$i}]address_districts_id")->textInput(['maxlength' => true]) ?>-->

                                                <?php
                                                $dataDistricts = \common\models\SysDistricts::find()->orderBy('name')->all();
                                                $listDataDistricts = \yii\helpers\ArrayHelper::map($dataDistricts, 'id', 'name');
                                                ?>
                                                <?= $form->field($modelsAddress, "[{$i}]address_districts_id")->widget(DepDrop::classname(), [
                                                'options' => ['id' => "custusersaddress-$i-address_districts_id"],
                                                'data'=>$listDataDistricts,
                                                'pluginOptions' => [
                                                    'depends' => ["custusersaddress-$i-address_country_id", "custusersaddress-$i-address_province_id", "custusersaddress-$i-address_countytown_id"],
                                                    'placeholder' => Yii::t('language', 'Select Districts..'),
                                                    'url' => Url::to(['/sys-districts/districtslist'])
                                                ]
                                                ]);
                                                    ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $form->field($modelsAddress, "[{$i}]address_postalzip")->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div><!-- .row -->
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <?= $form->field($modelsAddress, "[{$i}]address_remarks")->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div><!-- .row -->
                                    </div>
                                </div>
                                <?php endforeach; ?>
                        </div>
                    </div>
                </div><!-- .panel -->

                <?php DynamicFormWidget::end(); ?>
            </div>
        </div>

        <!-- /.tab-panel -->
        <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        <?php } ?>

    </div><!-- /.tab-content -->
    <?php ActiveForm::end(); ?>
</div>
