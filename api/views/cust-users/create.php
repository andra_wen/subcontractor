<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CustUsers */

?>
<div class="cust-users-create">
    <?= $this->render('_form', [
        'model' => $model, 'model2' => $model2, 'model3' => $model3, 'modelAddress' => $modelAddress
    ]) ?>
</div>
