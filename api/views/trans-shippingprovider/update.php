<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TransShippingprovider */
?>
<div class="trans-shippingprovider-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
