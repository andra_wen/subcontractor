<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TransAdjustments */
?>
<div class="trans-adjustments-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
