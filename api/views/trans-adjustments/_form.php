<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\TransAdjustments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trans-adjustments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $dataCustomer = ArrayHelper::map(\common\models\MasterCustomer::find()->all(), 'id', 'customer_name'); ?>
    <?=
    $form->field($model, 'customer_id')->widget(Select2::classname(),[
        'data'=> $dataCustomer,
        'options'=>['placeholder'=> 'Select Customer...'],
        'pluginOptions'=> [
            'allowClear'=>false
        ]
    ]);
    ?>

    <?php $dataProduct = ArrayHelper::map(\common\models\MasterProducts::find()->all(), 'id', 'product_name'); ?>
    <?=
    $form->field($model, 'product_id')->widget(Select2::classname(),[
        'data'=> $dataProduct,
        'options'=>['placeholder'=> 'Select Product...'],
        'pluginOptions'=> [
            'allowClear'=>false
        ]
    ]);
    ?>

    <?= $form->field($model, 'adj_quantity')->textInput() ?>

    <?= $form->field($model, 'adj_reason')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'adj_remarks')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
    [
        'pluginOptions'=>[
            'onText'=>'Active',
            'offText'=>'Inactive',
        ],
    ]);
    ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
