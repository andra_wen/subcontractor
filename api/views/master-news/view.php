<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use Yii\helpers\Url;

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterNews */
?>
<div class="master-news-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer.customer_name',
            'page.page_name',
            'news_name',
            'news_title',
            'news_content',
            'news_pict',
            'news_alt',
            [
                'label'=>'isactive',
                'format'=>'html',
                'attribute'=>function($model)
                {
                    if($model->isactive == 10)
                    {
                        return '<span class="label label-primary">'.'Active'.'</span>';
                    }
                    else
                    {
                        return "<span class='label label-danger'>"."Inactive"."</span>";
                    }
                }
            ],
            'sort_no',
            'effective_from',
            'effective_till',
            'createdby0.username',
            'createdon',
            'lastmodifby0.username',
            'lastmodif',
            [
                'label'=>'status',
                'format'=>'html',
                'attribute'=>function($model)
                {
                    if($model->status == 10)
                    {
                        return '<span class="label label-primary">'.'Active'.'</span>';
                    }
                    else
                    {
                        return "<span class='label label-danger'>"."Inactive"."</span>";
                    }
                }
            ],
        ],
    ]) ?>

    <?php $form = ActiveForm::begin([
    'options'=>['enctype'=>'multipart/form-data'], // important
    'action' =>['master-news/upload','id'=>$model->id],
    'id' => 'picture_post', 'method' => 'post',
]);?>

    <?php
    if($model3){
        $pictures = [];
        $picturesConfig = [];
        foreach ($model3 as $picture)
        {
            $pictures[] = Html::img("@web/uploads/news/" .$picture->picture_path ,['width'=>'70%']);
            $picturesConfig[] = [
                'caption'=>$picture->picture_name,
                'url'=> Url::to(['master-news/deletepicture','id'=>$picture->id]),
                'key'=>$picture->id,
            ];
        }
    }else{
        $pictures = "no_picture.jpg";
        $picturesConfig = "no config";
    }
    ?>

    <?= $form->field($model, 'getfile[]')->widget(FileInput::classname(), [
    'options' => ['multiple'=>true, 'accept' => 'image/*'],
    'pluginOptions' => [
        'showCaption' => true,
        'showRemove' => true,
        'showUpload' => false,
        'browseClass' => 'btn btn-primary btn-block',
        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
        'browseLabel' =>  'Select Photo',
        'removeClass' => 'btn btn-danger',
        'removeIcon' => '<i class="glyphicon glyphicon-trash"></i>',
        'initialPreview'=>
        $pictures,
        'initialPreviewConfig'=>
        $picturesConfig,
        'overwriteInitial'=>false
    ],
]); ?>

    <div class="text-center col-lg-12">
        <div class="col-lg-12">
            <?= Html::submitButton('Upload', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
