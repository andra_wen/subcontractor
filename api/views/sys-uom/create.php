<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SysUom */

?>
<div class="sys-uom-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
