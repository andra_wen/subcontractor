<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use vova07\select2\Widget;
use yii\web\JsExpression;
use kartik\widgets\SwitchInput;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\MasterProducts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Product Basic Info</a></li>
        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Product Profile Info</a></li>
    </ul>

    <?php $form = ActiveForm::begin(); ?>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <div class="master-products-form">

                <?php $dataCustomer = ArrayHelper::map(\common\models\MasterCustomer::find()->where(['status'=>10])->all(), 'id', 'customer_name'); ?>
                <?=
                $form->field($model, 'customer_id')->widget(Widget::classname(),[
                    'items'=> $dataCustomer,
                    'options'=>[
                        'placeholder'=> 'Select Customer...'
                    ],
                    'events' => [
                        'select2-open' => 'function (e) { log("select2:open", e); }',
                        'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
                    ],
                    'settings' => [
                        'width' => '100%',
                    ],
                ]);
                ?>

                <?php $dataCategory = ArrayHelper::map(\common\models\MasterCategory::find()->where(['status'=>10])->all(), 'id', 'category_name'); ?>
                <?=
                $form->field($model, 'product_category')->widget(Widget::classname(),[
                    'items'=> $dataCategory,
                    'options'=>[
                        'placeholder'=> 'Select Category...'
                    ],
                    'events' => [
                        'select2-open' => 'function (e) { log("select2:open", e); }',
                        'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
                    ],
                    'settings' => [
                        'width' => '100%',
                    ],
                ]);
                ?>

                <?= $form->field($model, 'product_code')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'product_desc')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'product_price')->textInput() ?>

                <?= $form->field($model, 'product_weight')->textInput() ?>

                <?= $form->field($model, 'product_height')->textInput() ?>

                <?= $form->field($model, 'product_volume')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'product_remarks')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
                [
                    'pluginOptions'=>[
                        'onText'=>'Active',
                        'offText'=>'Inactive',
                    ],
                ]);
                ?>

                <?= $form->field($model, 'custome')->widget(SwitchInput::classname(),
                [
                    'pluginOptions'=>[
                        'onText'=>'Yes',
                        'offText'=>'No',
                    ],
                ]);
                ?>
                <?php if (!Yii::$app->request->isAjax){ ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <?php } ?>
            </div>
	    </div>
        <!-- /.tab-pane -->

        <div class="tab-pane" id="tab_2">
            <div class="master-product-profiles-form">
                <?= $form->field($model2, 'barcode')->textInput(['maxlength' => true]) ?>

                <?php $dataUOM = ArrayHelper::map(\common\models\SysUom::find()->where(['status'=>10])->all(), 'id', 'uom_name'); ?>
                <?=
                $form->field($model2, 'uom_id')->widget(Widget::classname(),[
                    'items'=> $dataUOM,
                    'options'=>[
                        'placeholder'=> 'Select UOM...'
                    ],
                    'events' => [
                        'select2-open' => 'function (e) { log("select2:open", e); }',
                        'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
                    ],
                    'settings' => [
                        'width' => '100%',
                    ],
                ]);
                ?>

                <?= $form->field($model2, 'alias')->textInput() ?>

                <?= $form->field($model2, 'manufacturing_date')->widget(DatePicker::classname(),
                [
                    'name'=>'dateofbirth',
                    'value'=> date('yyyy-mm-dd'),
                    'options'=>['placeholder'=>'Select a date...'],
                    'pluginOptions'=>
                    [
                        'format'=>'yyyy-mm-dd',
                        'todayHighlight'=>true,
                        'autoclose'=>true,
                    ]
                ]);
                ?>

                <?= $form->field($model2, 'shelflife')->textInput() ?>

                <?= $form->field($model2, 'status')->widget(SwitchInput::classname(),
                [
                    'pluginOptions'=>[
                        'onText'=>'Active',
                        'offText'=>'Inactive',
                    ],
                ]);
                ?>
            </div>
        </div>
        <!-- /.tab-pane -->
    </div>
    <?php ActiveForm::end(); ?>
    
</div>
