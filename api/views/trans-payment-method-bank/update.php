<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TransPaymentMethodBank */
?>
<div class="trans-payment-method-bank-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
