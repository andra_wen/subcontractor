<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SysCountytown */
?>
<div class="sys-countytown-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
