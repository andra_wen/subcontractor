<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <!--<img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>-->
                <?php
                    use yii\helpers\Html;
                    $hasil = \common\models\CustUsersProfile::getCustprofilebyuserid(Yii::$app->user->identity->id);
                    echo Html::img('@web/uploads/userpictures/'.$hasil->user_picture,['class'=>'img-circle','alt'=>"User Image"]);
                ?>
            </div>
            <div class="pull-left info">
                <p><?php echo Yii::$app->user->identity->username;?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <?php
        use mdm\admin\components\MenuHelper;
        $callback = function($menu){
            $data = $menu['data'];
            //if have syntax error, unexpected 'fa' (T_STRING)  Errorexception,can use
            //$data = $menu['data'];
            return [
                'label' => $menu['name'],
                'url' => [$menu['route']],
                'option' => $data,
                'icon' => $menu['data'],
                'items' => $menu['children'],
            ];
        };

        $items = MenuHelper::getAssignedMenu(Yii::$app->user->id, null, $callback, true);
        //untuk tambahan menu logout, khusus.
        $items[] = [
            'label'=>'Logout',
            'url'=>['/site/logout'],
            'icon'=>'fa fa-sign-out',
            'template' => '<a href="{url}" data-method="post">{icon} {label}</a>', 'visible' => !Yii::$app->user->isGuest
        ];

        echo dmstr\widgets\Menu::widget([
            'options' => ['class' => 'sidebar-menu','data-widget'=>'tree'],
            'items' => $items,
        ]);

        ?>


    </section>

</aside>
