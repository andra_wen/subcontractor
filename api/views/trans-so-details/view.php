<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TransSoDetails */
?>
<div class="trans-so-details-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'so_id',
            'product_id',
            'product_name',
            'product_varians_id',
            'product_varians_name',
            'product_price',
            'product_quantity',
            'provider_id',
            'provider_name',
            'shippingcost_id',
            'shippingcost',
            'user_address_id',
            'address_name',
            'address_line1',
            'address_line2',
            'address_country',
            'address_province',
            'address_countytown',
            'address_districts',
            'address_postalzip',
            'cart_remarks',
            'details_total',
            'details_statuscode',
            'createdby',
            'createdon',
            'lastmodifby',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
