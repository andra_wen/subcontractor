<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TransSoDetails */
?>
<div class="trans-so-details-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
