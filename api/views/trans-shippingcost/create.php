<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TransShippingcost */

?>
<div class="trans-shippingcost-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
