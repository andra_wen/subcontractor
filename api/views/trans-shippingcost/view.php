<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TransShippingcost */
?>
<div class="trans-shippingcost-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer.customer_name',
            'shippingprovider.shipping_name',
            'shippingCountry.short_name',
            'shippingProvince.name',
            'shippingCountytown.name',
            'shippingDistricts.name',
            'shipping_note',
            'shipping_prices',
            'shipping_minweight',
            'shipping_minweightprices',
            'shipping_minwegithnextprice',
            'shipping_maxweight',
            [
                'label'=>'isactive',
                'format'=>'html',
                'attribute'=>function($model)
                {
                    if($model->isactive == 10)
                    {
                        return '<span class="label label-primary">'.'Active'.'</span>';
                    }
                    else
                    {
                        return "<span class='label label-danger'>"."Inactive"."</span>";
                    }
                }
            ],
            'createdby0.username',
            'createdon',
            'lastmodifby0.username',
            'lastmodif',
            [
                'label'=>'status',
                'format'=>'html',
                'attribute'=>function($model)
                {
                    if($model->status == 10)
                    {
                        return '<span class="label label-primary">'.'Active'.'</span>';
                    }
                    else
                    {
                        return "<span class='label label-danger'>"."Inactive"."</span>";
                    }
                }
            ],
        ],
    ]) ?>

</div>
