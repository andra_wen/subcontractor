<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CustClientAddress */

?>
<div class="cust-client-address-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
