<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TransSoPictures */

$this->title = Yii::t('language', 'Create Trans So Pictures');
$this->params['breadcrumbs'][] = ['label' => Yii::t('language', 'Trans So Pictures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trans-so-pictures-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
