<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CustUsersAddress */
?>
<div class="cust-users-address-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cust_user_id',
            'address_name',
            'address_line1',
            'address_line2',
            'address_phonenumber',
            'address_country_id',
            'address_province_id',
            'address_countytown_id',
            'address_districts_id',
            'address_postalzip',
            'address_default',
            'address_remarks',
            'createdon',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
