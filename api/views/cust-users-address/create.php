<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CustUsersAddress */

?>
<div class="cust-users-address-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
