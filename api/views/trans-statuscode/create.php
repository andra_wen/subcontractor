<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TransStatuscode */

?>
<div class="trans-statuscode-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
