<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterNewsPictures */

?>
<div class="master-news-pictures-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
