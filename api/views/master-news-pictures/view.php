<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterNewsPictures */
?>
<div class="master-news-pictures-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'news_id',
            'picture_name',
            'picture_path',
            'picture_path_md',
            'picture_path_sm',
            'picture_path_xs',
            'picture_path_thumbnail',
            'picture_alt',
            'sort_no',
            'createdby',
            'createdon',
            'lastmodifby',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
