<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterNews */

$this->title = 'Create Master News';
$this->params['breadcrumbs'][] = ['label' => 'Master News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
