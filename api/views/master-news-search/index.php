<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterNewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Master News', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'customer_id',
            'page_id',
            'news_name',
            'news_title',
            // 'news_content',
            // 'news_pict',
            // 'news_alt',
            // 'isactive',
            // 'sort_no',
            // 'effective_from',
            // 'effective_till',
            // 'createdby',
            // 'createdon',
            // 'lastmodifby',
            // 'lastmodif',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
