<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterProductProfiles */
?>
<div class="master-product-profiles-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'product_id',
            'barcode',
            'uom_id',
            'alias',
            'manufacturing_date',
            'shelflife',
            'createdby',
            'createdon',
            'lastmodifby',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
