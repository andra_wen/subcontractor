<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterCustomer */
?>
<div class="master-customer-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer_name',
            'customer_desc',
            'custCreated.username',
            'createdon',
            'custLastmodif.username',
            'lastmodif',
            [
                'label' => 'Status',
                'format' => 'html',
                'attribute' => function($model)
                {
                    if($model->status == 10)
                    {
                        return '<span class="label label-primary">'.'Active'.'</span>';
                    }
                    else
                    {
                        return "<span class='label label-danger'>"."Inactive"."</span>";
                    }
                }
            ]
        ],
    ]) ?>

</div>
