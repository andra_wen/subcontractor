<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TransSoConfirmation */
?>
<div class="trans-so-confirmation-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
