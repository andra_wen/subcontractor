<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TransCarts */
?>
<div class="trans-carts-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer_id',
            'cust_client_id',
            'product_id',
            'product_varians_id',
            'product_quantity',
            'provider_id',
            'shippingcost_id',
            'client_address_id',
            'cart_remarks',
            'createdby',
            'createdon',
            'lastmodifby',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
