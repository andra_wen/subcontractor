<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SysSeqPattern */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sys-seq-pattern-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'customer_id')->textInput() ?>

    <?= $form->field($model, 'pattern_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pattern_desc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pattern_fortable')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pattern_format')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pattern_lastsequences')->textInput() ?>

    <?= $form->field($model, 'createdby')->textInput() ?>

    <?= $form->field($model, 'createdon')->textInput() ?>

    <?= $form->field($model, 'lastmodifby')->textInput() ?>

    <?= $form->field($model, 'lastmodif')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
