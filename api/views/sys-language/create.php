<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SysLanguage */

?>
<div class="sys-language-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
