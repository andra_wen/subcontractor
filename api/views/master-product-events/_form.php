<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use vova07\select2\Widget;
use yii\web\JsExpression;
use kartik\widgets\SwitchInput;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\MasterProductEvents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-product-events-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $dataProduct = ArrayHelper::map(\common\models\MasterProducts::find()->where(['status'=>10])->all(), 'id', 'product_name'); ?>
    <?=
    $form->field($model, 'product_id')->widget(Widget::classname(),[
        'items'=> $dataProduct,
        'options'=>[
            'placeholder'=> 'Select Product...'
        ],
        'events' => [
            'select2-open' => 'function (e) { log("select2:open", e); }',
            'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
        ],
        'settings' => [
            'width' => '100%',
        ],
    ]);
    ?>

    <?= $form->field($model, 'event_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'event_caption')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'event_label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'event_stringvalue')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'event_numbervalue')->textInput() ?>

    <?= $form->field($model, 'effective_from')->widget(DatetimePicker::classname(),
    [
        'name'=>'effective_from',
        'value'=> date('yyyy-mm-dd H:i:s'),
        'options'=>['placeholder'=>'Select a date...'],
        'pluginOptions'=>
        [
            'format'=>'yyyy-mm-dd H:i:s',
            'todayHighlight'=>true,
            'autoclose'=>true,
        ]
    ]);
    ?>

    <?= $form->field($model, 'effective_till')->widget(DatetimePicker::classname(),
    [
        'name'=>'effective_till',
        'value'=> date('yyyy-mm-dd H:i:s'),
        'options'=>['placeholder'=>'Select a date...'],
        'pluginOptions'=>
        [
            'format'=>'yyyy-mm-dd H:i:s',
            'todayHighlight'=>true,
            'autoclose'=>true,
        ]
    ]);
    ?>

    <?= $form->field($model, 'isactive')->widget(SwitchInput::classname(),
    [
        'pluginOptions'=>[
            'onText'=>'Active',
            'offText'=>'Inactive',
        ],
    ]);
    ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
    [
        'pluginOptions'=>[
            'onText'=>'Active',
            'offText'=>'Inactive',
        ],
    ]);
    ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
