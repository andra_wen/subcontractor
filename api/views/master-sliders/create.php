<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterSliders */

?>
<div class="master-sliders-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
