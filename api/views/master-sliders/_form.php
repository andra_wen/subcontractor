<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use vova07\select2\Widget;
use yii\web\JsExpression;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MasterSliders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-sliders-form">

    <?php $form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data'] // important
        ]);
    ?>

    <?php $dataCustomer = ArrayHelper::map(\common\models\MasterCustomer::find()->where(['status'=>10])->all(), 'id', 'customer_name'); ?>
    <?=
    $form->field($model, 'customer_id')->widget(Widget::classname(),[
        'items'=> $dataCustomer,
        'options'=>[
            'placeholder'=> 'Select Customer...'
        ],
        'events' => [
            'select2-open' => 'function (e) { log("select2:open", e); }',
            'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
        ],
        'settings' => [
            'width' => '100%',
        ],
    ]);
    ?>

    <?php $dataPage = ArrayHelper::map(\common\models\MasterPages::find()->where(['status'=>10])->all(), 'id', 'page_name'); ?>
    <?=
    $form->field($model, 'page_id')->widget(Widget::classname(),[
        'items'=> $dataPage,
        'options'=>['placeholder'=> 'Select Page...'],
        'events' => [
            'select2-open' => 'function (e) { log("select2:open", e); }',
            'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
        ],
        'settings' => [
            'width' => '100%',
        ],
    ]);
    ?>

    <?= $form->field($model, 'slider_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slider_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slider_pict')->widget(FileInput::classname(), [
    'options' => ['accept' => 'image/*'],
    'pluginOptions' => [
        'showCaption' => false,
        'showRemove' => false,
        'showUpload' => false,
        'browseClass' => 'btn btn-primary btn-block',
        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
        'browseLabel' =>  'Select Photo',
        'initialPreview'=>[
            Html::img("@web/uploads/sliders/" . $model->slider_pict,['width'=>100])
        ],
        'overwriteInitial'=>true
    ],
]); ?>

    <?= $form->field($model, 'slider_alt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isactive')->widget(SwitchInput::classname(),
    [
        'pluginOptions'=>[
            'onText'=>'Active',
            'offText'=>'Inactive',
        ],
    ]);
    ?>

    <?= $form->field($model, 'sort_no')->textInput() ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
    [
        'pluginOptions'=>[
            'onText'=>'Active',
            'offText'=>'Inactive',
        ],
    ]);
    ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
