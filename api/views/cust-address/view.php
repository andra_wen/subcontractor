<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CustAddress */
?>
<div class="cust-address-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cust_user_id',
            'address_name',
            'address_line1',
            'address_line2',
            'address_country_id',
            'address_province_id',
            'address_countytown_id',
            'address_districts_id',
            'address_postalzip',
            'address_remarks',
            'address_default',
            'createdby',
            'createdon',
            'lastmodifby',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
