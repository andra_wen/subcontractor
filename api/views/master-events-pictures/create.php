<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterEventsPictures */

?>
<div class="master-events-pictures-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
