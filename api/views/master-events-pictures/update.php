<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterEventsPictures */
?>
<div class="master-events-pictures-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
