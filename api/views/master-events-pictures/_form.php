<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MasterEventsPictures */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-events-pictures-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'event_id')->textInput() ?>

    <?= $form->field($model, 'picture_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'picture_path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'picture_path_md')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'picture_path_sm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'picture_path_xs')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'picture_path_thumbnail')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'picture_alt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sort_no')->textInput() ?>

    <?= $form->field($model, 'createdby')->textInput() ?>

    <?= $form->field($model, 'createdon')->textInput() ?>

    <?= $form->field($model, 'lastmodifby')->textInput() ?>

    <?= $form->field($model, 'lastmodif')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
