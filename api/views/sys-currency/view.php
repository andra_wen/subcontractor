<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SysCurrency */
?>
<div class="sys-currency-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            'iso_code',
            'name',
            'convert_nomimal',
            'convert_rate',
            'intl_formatting',
            'min_fraction_digits',
            'max_fraction_digits',
            'dec_point',
            'thousands_sep',
            'format_string',
            'createdby',
            'createdon',
            'lastmodifby',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
