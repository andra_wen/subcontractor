<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SysDistricts */

?>
<div class="sys-districts-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
