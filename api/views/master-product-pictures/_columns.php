<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'product_id',
        'value'=>'product.product_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'picture_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'picture_path',
        //'attribute' => 'Image',
        'format' => 'raw',
        'value' => function ($model)
        {
            if ($model->picture_path!='')
                return Html::img("@web/uploads/productpictures/" . $model->picture_path,['width'=>50]); else return 'no image';
        }
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'picture_path_md',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'picture_path_sm',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'picture_path_xs',
    // ],
    //[
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'picture_path_thumbnail',
    // ],
    //[
        //'class'=>'\kartik\grid\DataColumn',
        //'attribute'=>'picture_path_thumbnail',
        //'attribute' => 'Image',
        //'format' => 'raw',
        //'value' => function ($model)
        //{
            //if ($model->picture_path_thumbnail!='')
                //return Html::img("@web/uploads/productpictures_thumbnail/" . $model->picture_path_thumbnail,['width'=>50]); else return 'no image';
        //}
    //],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'picture_alt',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'sort_no',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'featured',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'featured',
        'format'=>'html',
        'value'=>function($model)
        {
            return $model->featured == 10 ? '<p style="color:green;"><i class="fa fa-check"></i> <i>'.Yii::t('language','Yes').'</i></p>' : '<p style="color:red;"><i class="fa fa-close"></i> <i>'.Yii::t('language','No').'</i></p>';
        },
        'filter' => [
            0 => 'No',
            10 => 'Yes'
        ],
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'createdby',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'createdon',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'lastmodifby',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'lastmodif',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'format'=>'html',
        'value'=>function($model)
        {
            return $model->status == 0 ? '<span class="label label-danger">Inactive</span>' : '<span class="label label-primary">Active</span>';
        },
        'filter' => [
            0 => 'Inactive',
            10 => 'Active'
        ],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   