<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CustUsersProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cust-users-profile-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cust_user_id')->textInput() ?>

    <?= $form->field($model, 'user_firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_middlename')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_picture')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'placeofbirth')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dateofbirth')->textInput() ?>

    <?= $form->field($model, 'gender')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'marriagestatus')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bloodtype')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
