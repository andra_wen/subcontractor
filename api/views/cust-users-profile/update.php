<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CustUsersProfile */
?>
<div class="cust-users-profile-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
