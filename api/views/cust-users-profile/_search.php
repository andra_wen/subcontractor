<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CustUserProfilesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cust-users-profile-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'cust_user_id') ?>

    <?= $form->field($model, 'user_firstname') ?>

    <?= $form->field($model, 'user_middlename') ?>

    <?= $form->field($model, 'user_lastname') ?>

    <?php // echo $form->field($model, 'user_picture') ?>

    <?php // echo $form->field($model, 'placeofbirth') ?>

    <?php // echo $form->field($model, 'dateofbirth') ?>

    <?php // echo $form->field($model, 'gender') ?>

    <?php // echo $form->field($model, 'marriagestatus') ?>

    <?php // echo $form->field($model, 'createdby') ?>

    <?php // echo $form->field($model, 'createdon') ?>

    <?php // echo $form->field($model, 'lastmodifby') ?>

    <?php // echo $form->field($model, 'lastmodif') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
