<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'customer_id',
        'value'=>'customer.customer_name'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'bank_shortname',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'bank_longname',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'bank_logo',
        'format' => 'raw',
        'value' => function ($model) {
            if ($model->bank_logo!='')
                return Html::img("@web/uploads/banks/" . $model->bank_logo,['width'=>50]); else return 'no image';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'bank_picture',
        'format' => 'raw',
        'value' => function ($model) {
            if ($model->bank_picture!='')
                return Html::img("@web/uploads/banks/" . $model->bank_picture,['width'=>50]); else return 'no image';
        }
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'createdby',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'createdon',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'lastmodifby',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'lastmodif',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'status',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   