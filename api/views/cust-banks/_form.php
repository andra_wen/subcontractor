<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use vova07\select2\Widget;
use yii\web\JsExpression;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\CustBanks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cust-banks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $dataCustomer = ArrayHelper::map(\common\models\MasterCustomer::find()->where(['status'=>10])->all(), 'id', 'customer_name'); ?>
    <?=
    $form->field($model, 'customer_id')->widget(Widget::classname(),[
        'items'=> $dataCustomer,
        'options'=>[
            'placeholder'=> 'Select Customer...'
        ],
        'events' => [
            'select2-open' => 'function (e) { log("select2:open", e); }',
            'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
        ],
        'settings' => [
            'width' => '100%',
        ],
    ]);
    ?>

    <?= $form->field($model, 'bank_shortname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank_longname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank_logo')->widget(FileInput::classname(), [
    'options' => ['accept' => 'image/*'],
    'pluginOptions' => [
        'showCaption' => false,
        'showRemove' => false,
        'showUpload' => false,
        'browseClass' => 'btn btn-primary btn-block',
        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
        'browseLabel' =>  'Select Photo',
        'initialPreview'=>[
            Html::img("@web/uploads/banks/" . $model->bank_logo,['width'=>100])
        ],
        'overwriteInitial'=>true
    ],
]); ?>

    <?= $form->field($model, 'bank_picture')->widget(FileInput::classname(), [
    'options' => ['accept' => 'image/*'],
    'pluginOptions' => [
        'showCaption' => false,
        'showRemove' => false,
        'showUpload' => false,
        'browseClass' => 'btn btn-primary btn-block',
        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
        'browseLabel' =>  'Select Photo',
        'initialPreview'=>[
            Html::img("@web/uploads/banks/" . $model->bank_picture,['width'=>100])
        ],
        'overwriteInitial'=>true
    ],
]); ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
    [
        'pluginOptions'=>[
            'onText'=>'Active',
            'offText'=>'Inactive',
        ],
    ]);
    ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
