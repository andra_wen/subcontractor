<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MailerLog */
?>
<div class="mailer-log-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer_id',
            'mail_to_address',
            'mail_to_name',
            'mail_cc_address',
            'mail_cc_name',
            'mail_to_message',
            'isactive',
            'createdby',
            'createdon',
            'status',
        ],
    ]) ?>

</div>
