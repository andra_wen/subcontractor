<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MailerLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mailer-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'customer_id')->textInput() ?>

    <?= $form->field($model, 'mail_to_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mail_to_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mail_cc_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mail_cc_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mail_to_message')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isactive')->textInput() ?>

    <?= $form->field($model, 'createdby')->textInput() ?>

    <?= $form->field($model, 'createdon')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
