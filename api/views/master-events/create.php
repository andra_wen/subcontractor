<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterEvents */

?>
<div class="master-events-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
