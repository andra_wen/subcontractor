<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterEvents */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Master Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-events-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer_id',
            'page_id',
            'event_name',
            'event_title',
            'event_content',
            'event_pict',
            'event_alt',
            'isactive',
            'sort_no',
            'event_date',
            'effective_from',
            'effective_till',
            'createdby',
            'createdon',
            'lastmodifby',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
