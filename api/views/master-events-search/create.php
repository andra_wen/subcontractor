<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterEvents */

$this->title = 'Create Master Events';
$this->params['breadcrumbs'][] = ['label' => 'Master Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-events-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
