<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;
use frontend\assets\JsCoreAsset;

/**
 * Main frontend application asset bundle.
 */
class ResourceAsset extends AssetBundle
{
    public $sourcePath = '@app/web/resources';
    public $css = [
		'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
		
		//---main---
		'themes/frontend/main.css',
		
		//---helpers---
		'helpers/animate.css',
		'helpers/backgrounds.css',
		'helpers/boilerplate.css',
		'helpers/border-radius.css',
		'helpers/grid.css',
		'helpers/page-transitions.css',
		'helpers/spacing.css',
		'helpers/typography.css',
		'helpers/utils.css',
		'helpers/colors.css',
		
		//---elements---
		'elements/badges.css',
		'elements/buttons.css',
		'elements/content-box.css',
		'elements/dashboard-box.css',
		'elements/forms.css',
		'elements/images.css',
		'elements/info-box.css',
		'elements/invoice.css',
		'elements/loading-indicators.css',
		'elements/menus.css',
		'elements/panel-box.css',
		'elements/response-messages.css',
		'elements/responsive-tables.css',
		'elements/ribbon.css',
		'elements/social-box.css',
		'elements/tables.css',
		'elements/tile-box.css',
		'elements/timeline.css',
		
		//---frontend elements---
		'frontend-elements/blog.css',
		'frontend-elements/cta-box.css',
		'frontend-elements/feature-box.css',
		'frontend-elements/footer.css',
		'frontend-elements/hero-box.css',
		'frontend-elements/icon-box.css',
		'frontend-elements/portfolio-navigation.css',
		'frontend-elements/pricing-table.css',
		'frontend-elements/sliders.css',
		'frontend-elements/testimonial-box.css',
		
		//---icons---
		'icons/fontawesome/fontawesome.css',
		'icons/linecons/linecons.css',
		'icons/spinnericon/spinnericon.css',
		
		//---widgets---
		'widgets/accordion-ui/accordion.css',
		'widgets/calendar/calendar.css',
		'widgets/carousel/carousel.css',

		'widgets/charts/justgage/justgage.css',
		'widgets/charts/morris/morris.css',
		'widgets/charts/piegage/piegage.css',
		'widgets/charts/xcharts/xcharts.css',

		'widgets/chosen/chosen.css',
		'widgets/colorpicker/colorpicker.css',
		'widgets/datatable/datatable.css',
		'widgets/datepicker/datepicker.css',
		'widgets/datepicker-ui/datepicker.css',
		'widgets/daterangepicker/daterangepicker.css',
		'widgets/dialog/dialog.css',
		'widgets/dropdown/dropdown.css',
		'widgets/dropzone/dropzone.css',
		'widgets/file-input/fileinput.css',
		'widgets/input-switch/inputswitch.css',
		'widgets/input-switch/inputswitch-alt.css',
		'widgets/ionrangeslider/ionrangeslider.css',
		'widgets/jcrop/jcrop.css',
		'widgets/jgrowl-notifications/jgrowl.css',
		'widgets/loading-bar/loadingbar.css',
		'widgets/maps/vector-maps/vectormaps.css',
		'widgets/markdown/markdown.css',
		'widgets/modal/modal.css',
		'widgets/multi-select/multiselect.css',
		'widgets/multi-upload/fileupload.css',
		'widgets/nestable/nestable.css',
		'widgets/noty-notifications/noty.css',
		'widgets/popover/popover.css',
		'widgets/pretty-photo/prettyphoto.css',
		'widgets/progressbar/progressbar.css',
		'widgets/range-slider/rangeslider.css',
		'widgets/slider-ui/slider.css',
		'widgets/summernote-wysiwyg/summernote-wysiwyg.css',
		'widgets/tabs-ui/tabs.css',
		'widgets/theme-switcher/themeswitcher.css',
		'widgets/timepicker/timepicker.css',
		'widgets/tocify/tocify.css',
		'widgets/tooltip/tooltip.css',
		'widgets/touchspin/touchspin.css',
		'widgets/uniform/uniform.css',
		'widgets/wizard/wizard.css',
		'widgets/xeditable/xeditable.css',
		
		//---frontend widgets---
		'widgets/layerslider/layerslider.css',
		'widgets/owlcarousel/owlcarousel.css',
		'widgets/fullpage/fullpage.css',
		
		//---snippets---
		'snippets/chat.css',
		'snippets/files-box.css',
		'snippets/login-box.css',
		'snippets/notification-box.css',
		'snippets/progress-box.css',
		'snippets/todo.css',
		'snippets/user-profile.css',
		'snippets/mobile-navigation.css',
		
		//--frontend theme---
		'themes/frontend/layout.css',
		'themes/frontend/color-schemes/default.css',
		
		//---components theme---
		'themes/components/default.css',
		'themes/components/border-radius.css',
		
		//---frontend responsive---
		'helpers/responsive-elements.css',
		'helpers/frontend-responsive.css',
		
    ];
    public $js = [
		
		//--- Skrollr -->

        'widgets/skrollr/skrollr.js',

        //--- Owl carousel -->

        'widgets/owlcarousel/owlcarousel.js',
        'widgets/owlcarousel/owlcarousel-demo.js',

        //--- HG sticky -->

        'widgets/sticky/sticky.js',

        //--- WOW -->

        'widgets/wow/wow.js',

        //--- VideoBG -->

        'widgets/videobg/videobg.js',
        'widgets/videobg/videobg-demo.js',

        //--- Mixitup -->

        'widgets/mixitup/mixitup.js',
        'widgets/mixitup/isotope.js',

        //--- WIDGETS -->

        //--- Bootstrap Dropdown -->

        'widgets/dropdown/dropdown.js',

        //--- Bootstrap Tooltip -->

        'widgets/tooltip/tooltip.js',

        //--- Bootstrap Popover -->

        'widgets/popover/popover.js',

        //--- Bootstrap Progress Bar -->

        'widgets/progressbar/progressbar.js',

        //--- Bootstrap Buttons -->

        'widgets/button/button.js',

        //--- Bootstrap Collapse -->

        'widgets/collapse/collapse.js',

        //--- Superclick -->

        'widgets/superclick/superclick.js',

        //--- Input switch alternate -->

        'widgets/input-switch/inputswitch-alt.js',

        //--- Slim scroll -->

        'widgets/slimscroll/slimscroll.js',

        //--- Content box -->

        'widgets/content-box/contentbox.js',

        //--- Overlay -->

        'widgets/overlay/overlay.js',

        //--- Widgets init for demo -->

        'js-init/widgets-init.js',
        'js-init/frontend-init.js',

        //--- Theme layout -->

        'themes/frontend/layout.js',

        //--- Theme switcher -->

        'widgets/theme-switcher/themeswitcher.js',
		
    ];
	
	/**
     * @var array
     */
    public $jsOptions = [
    ];
	
    public $depends = [
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
		JsCoreAsset::class,
    ];
}
