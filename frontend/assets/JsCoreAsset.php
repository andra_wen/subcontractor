<?php
namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class JsCoreAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/resources';

    /**
     * @var array
     */
    public $js = [
        'js-core/jquery-core.js',
		'js-core/jquery-ui-core.js',
		'js-core/jquery-ui-widget.js',
		'js-core/jquery-ui-mouse.js',
		'js-core/jquery-ui-position.js',
		'js-core/transition.js',
		'js-core/modernizr.js',
		'js-core/jquery-cookie.js',
    ];

    /**
     * @var array
     */
    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
	
	public $depends = [
        'yii\web\JqueryAsset',
    ];
}
