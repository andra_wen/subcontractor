<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;
use common\models\CustUsers;
use common\models\CustUsersProfile;
use common\models\CustUsersProduct;
use common\models\CustUsersAddress;
use common\models\MasterProducts;

/**
 * Signup form
 */
class UpdateProfile extends Model
{
    public $username;
    public $firstname;
    public $middlename;
    public $lastname;
    public $marriagestatus;
    public $bloodtype;
    public $email;
    public $password;
    public $repeat_password;
    public $contact_number;
    public $gender;
    public $company_name;
    public $company_brn;
    public $country;
    public $state;
    public $city;
    public $province;
    public $company_address;
    public $company_address2;
    public $zip_code;
    public $telephone;
    public $fax;
    public $company_email;
    public $terms;
    public $product;
    public $email_confirmation;
    public $invitation_code;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            [['username'], 'unique', 'targetClass' => '\common\models\CustUsers', 'message' => 'This username has already been taken.', 'targetAttribute' => ['username'=>'username']],
            ['username', 'string', 'min' => 2, 'max' => 255],

            [['email','company_email'], 'trim'],
            [['email','company_email'], 'required'],
            [['email','company_email'], 'email'],
            [['email','company_email'], 'string', 'max' => 255],
            [['email'], 'unique', 'targetClass' => '\common\models\CustUsers', 'message' => 'This email address has already been taken.', 'targetAttribute' => ['email'=>'email']],

            [['password','repeat_password'], 'required'],
            [['password','repeat_password'], 'string', 'min' => 6],
			['repeat_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match"],
			
			[['contact_number','company_name', 'company_brn', 'country', 'state', 'city', 'province','company_address', 'company_address2', 'zip_code', 'telephone', 'fax', 'terms', 'email_confirmation', 'invitation_code', 'firstname', 'middlename', 'lastname', 'marriagestatus'],'string', 'max' => 255],
			
			[['contact_number','company_name', 'company_brn', 'country', 'state', 'city', 'province','company_address', 'company_address2', 'zip_code', 'telephone', 'fax', 'email_confirmation', 'invitation_code', 'firstname', 'middlename', 'lastname', 'marriagestatus'],'trim'],
			
			[['contact_number','company_name', 'company_brn', 'country', 'state', 'city', 'province','company_address', 'zip_code', 'telephone', 'fax', 'terms', 'email_confirmation', 'invitation_code', 'gender', 'product', 'firstname', 'middlename', 'lastname'],'required'],
			
			[['gender', 'bloodtype'], 'string', 'max' => 1],
			// [['product'], 'integer', 'max' => 50],
			[['company_address2'], 'string', 'max' => 255],
			
        ];
    }
	
	public function getGenderOptions() {
		return array(
				'M'=>'Male',
				'F'=>'Female',
		);
	}
	
	public function getMarriage() {
		return array(
				'Single'=>'Single',
				'Married'=>'Married',
				'Separated'=>'Separated',
		);
	}
	
	public function getBlood() {
		return array(
				'A'=>'A',
				'B'=>'B',
				'AB'=>'AB',
				'O'=>'O',
		);
	}

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function edit()
    {
        // if (!$this->validate()) {
            // return null;
        // }
        
        // $user = new User();
        // $user->username = $this->username;
        // $user->email = $this->email;
        // $user->setPassword($this->password);
        // $user->generateAuthKey();
		
		$today = date('Y-m-d H:i:s');
		$custUser = new CustUsers();
		$custUser->customer_id = 3;
		$custUser->status = 10;
		$custUser->effective_from = $today;
		$custUser->effective_till = date('Y-m-d H:i:s',strtotime('+365 days',strtotime($today)));
		$custUser->username = $this->username;
		$custUser->email = $this->email;
        $custUser->password_hash = $this->password;
		$custu = $custUser->save();
		
		if($custu){
			$custAddress = new CustUsersAddress();
			$custAddress->cust_user_id = $custUser->id;
			$custAddress->address_line1 = $this->company_address;
			$custAddress->address_name = $this->company_name;
			$custAddress->address_remarks = $this->company_brn;
			$custAddress->address_line2 = $this->company_address2;
			$custAddress->address_phonenumber = $this->contact_number;
			$custAddress->address_faxnumber = $this->fax;
			$custAddress->address_country_id = $this->country;
			$custAddress->address_province_id = $this->province;
			$custAddress->address_countytown_id = $this->city;
			$custAddress->address_districts_id = $this->state;
			$custAddress->address_postalzip = $this->zip_code;
			$custAddress->address_email = $this->company_email;
			$custAddress->status = 10;
			$custAddress->save();
			
			$custProfiles = new CustUsersProfile();
			$custProfiles->cust_user_id = $custUser->id;
			$custProfiles->gender = $this->gender;
			$custProfiles->bloodtype = 'A';
			$custProfiles->user_firstname = $this->firstname;
			$custProfiles->user_middlename = $this->middlename;
			$custProfiles->user_lastname = $this->lastname;
			$custProfiles->marriagestatus = 'Single';
			$custProfiles->createdby = $custUser->id;
			$custProfiles->lastmodifby = $custUser->id;
			$custProfiles->status = 0;
			$custProfiles->save();
			
			foreach($this->product as $data){
				$products = new CustUsersProduct();
				$products->cust_user_id = $custUser->id;
				$products->product_id = $data;
				$products->createdby = $custUser->id;
				$products->lastmodifby = $custUser->id;
				$products->save();
			}
			
		}
		
        return ( $custu ) ? $custUser : null;
    }
	
	public function loadData($id)
	{
		
	}
}
