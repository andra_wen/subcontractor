<?php

/* @var $this yii\web\View */

$this->title = 'Tender List';
?>

<div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
        <div class="container">
            <h2 class="hero-heading wow fadeInDown text-center font-bold" data-wow-duration="0.6s">Your Tender List</h2>
            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-4">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                        <select class="form-control">
                            <option>10</option>
                            <option>25</option>
                            <option>50</option>
                            <option>100</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-8 control-label">Records per Page</div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 pull-right">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 no-padding">
                        <input type="text" class="form-control" id="" placeholder="Search...">
                    </div>
                </div>
            </div>
            <div class="clearfix col-lg-12 col-md-12 col-sm-12 hidden-xs space-10"></div>

            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding border-spacegrey border-l-orange">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        <div class="clearfix col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="font-space-grey font-bold font-40 helvetica">
                                     TND201804011231
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>

                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Tender Status :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    Submitted
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Submit Date : 
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    23 Feb 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Status :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    Public
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Registered :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    10 Feb 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Deadline :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    23 Apr 2018
                                </div>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-orange">
                                    PT Mechanical Everyday
                                </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-bold font-47 line-height-47 helvetica">
                                    PROJECT MILE CONTRACTOR
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-updn-10">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">General Building</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Civil Engineering</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Concrete Repairs</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow"></span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Interior Decoration & Finishing Works</span>
                                </div>
                            </div>
                            
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10 border-b-spacegrey"></div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>

                            <div class="clearfix col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                    <input type="button" onclick="window.location='tender-details.html'" class="btn-general-orange" name="btn-view" id="btn-view"
                                        value="Details">
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                    
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>

                <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding border-spacegrey border-l-orange">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        <div class="clearfix col-lg-5 col-md-5 col-sm-5 col-xs-12">
                
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="font-space-grey font-bold font-40 helvetica">
                                    TND201804011232
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Tender Status :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    <span class="font-orange">
                                        Invited
                                    </span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Submit Date :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    -
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Status :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    Public
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Registered :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    10 Feb 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Deadline :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    23 Apr 2018
                                </div>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-orange">
                                     PT Delta
                                </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-bold font-47 line-height-47 helvetica">
                                     PROJECT VISION BUILDING
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-updn-10">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">General Building</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Civil Engineering</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Concrete Repairs</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow"></span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Interior Decoration & Finishing Works</span>
                                </div>
                            </div>
                
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10 border-b-spacegrey"></div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                
                            <div class="clearfix col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                    <input type="button" onclick="window.location='tender-details.html'" class="btn-general-orange" name="btn-view" id="btn-view"
                                        value="Details">
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                    <input type="button" onclick="window.location='tender-new.html'" class="btn-general-orange" name="btn-create" id="btn-create"
                                        value="Tender">
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding border-spacegrey border-l-orange">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        <div class="clearfix col-lg-5 col-md-5 col-sm-5 col-xs-12">
                
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="font-space-grey font-bold font-40 helvetica">
                                    TND201804011234
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Tender Status :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    Submitted
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Submit Date :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    23 Feb 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Status :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    Public
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Registered :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    10 Feb 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Deadline :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    23 Apr 2018
                                </div>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-orange">
                                    PT Mechanical Everyday
                                </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-bold font-47 line-height-47 helvetica">
                                    PROJECT MILE CONTRACTOR
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-updn-10">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">General Building</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Civil Engineering</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Concrete Repairs</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow"></span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Interior Decoration & Finishing Works</span>
                                </div>
                            </div>
                
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10 border-b-spacegrey"></div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                
                            <div class="clearfix col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                    <input type="button" onclick="window.location='tender-details.html'" class="btn-general-orange" name="btn-view" id="btn-view"
                                        value="Details">
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
                
                <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding border-spacegrey border-l-orange">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        <div class="clearfix col-lg-5 col-md-5 col-sm-5 col-xs-12">
                
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="font-space-grey font-bold font-40 helvetica">
                                    TND201804011231
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Tender Status :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    <span class="font-orange">
                                        Invited
                                    </span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Submit Date :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    -
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Status :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    Public
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Registered :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    10 Feb 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Deadline :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    23 Apr 2018
                                </div>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-orange">
                                    PT Delta
                                </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-bold font-47 line-height-47 helvetica">
                                    PROJECT VISION BUILDING
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-updn-10">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">General Building</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Civil Engineering</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Concrete Repairs</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow"></span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Interior Decoration & Finishing Works</span>
                                </div>
                            </div>
                
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10 border-b-spacegrey"></div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                
                            <div class="clearfix col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                    <input type="button" onclick="window.location='tender-details.html'" class="btn-general-orange" name="btn-view" id="btn-view"
                                        value="Details">
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                    <input type="button" onclick="window.location='tender-new.html'" class="btn-general-orange" name="btn-create" id="btn-create"
                                        value="Tender">
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="text-center">
                        <ul class="pagination pagination-md">
                            <li>
                                <a href="#">«</a>
                            </li>
                            <li class="active">
                                <a href="#">1
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">2</a>
                            </li>
                            <li>
                                <a href="#">3</a>
                            </li>
                            <li>
                                <a href="#">4</a>
                            </li>
                            <li>
                                <a href="#">5</a>
                            </li>
                            <li>
                                <a href="#">»</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>