<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
        <div class="container">
            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding border-spacegrey border-l-orange">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-orange">
                                    PT Mechanical Everyday
                                </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="font-bold font-47 line-height-47 helvetica">
                                    PROJECT MILE CONTRACTOR
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-t-5">
                                <span class="font-12">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                    ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                    sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        </div>
                        <div class="clearfix col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-space-grey font-bold font-40 helvetica">
                                    SUBCON WORKHEAD
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-updn-10">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">General Building</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Civil Engineering</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Concrete Repairs</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow"></span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Interior Decoration & Finishing Works</span>
                                </div>
                            </div>

                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10 border-b-spacegrey"></div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>


                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Status :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    Public
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Registered :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    10 Feb 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Deadline :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    23 Apr 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                        <input type="button" onclick="window.location='tender-new.html'" class="btn-general-orange" name="btn-create" id="btn-create" value="Tender">
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                        <input type="button" onclick="window.location='subcontractor-new.html'" class="btn-general-orange" name="btn-create" id="btn-create"
                                            value="Edit">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                        </div>
                    </div>
                </div>

                <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 border-spacegrey">
                        <div class="row">
                            <div class="col-md-4">
                                <h2 class="invoice-client mrg10T">Project information:</h2>
                                <h5>Institude Building</h5>
                                <address class="invoice-address">
                                    101 Harris Road
                                    <br> Kilmarnock, VA(Virginia) 22482
                                    <br> (804) 435-8000
                                </address>
                            </div>
                            <div class="col-md-4">
                                <h2 class="invoice-client mrg10T">Contractor Info:</h2>
                                <ul class="reset-ul">
                                    <li>
                                        <b>Created by:</b> December 17, 2014</li>
                                    <li>
                                        <b>Status:</b>
                                        <span class="bs-label label-warning">Pending</span>
                                    </li>
                                    <li>
                                        <b>Id:</b> #474356</li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h2 class="invoice-client mrg10T">Requirement Details:</h2>
                                <p>To achieve this, it would be necessary to have uniform grammar, pronunciation and more common words. If several languages
                                    coalesce.</p>
                                <br>
                                <p>Project must be done within 4 Months</p>
                            </div>
                        </div>
                        
                        <table class="table mrg20T table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Product Name</th>
                                    <th class="text-center">Quantity</th>
                                    <th>Budget</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>General Building</td>
                                    <td class="text-center">1</td>
                                    <td>$433.10</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Civil Engineering</td>
                                    <td class="text-center">4</td>
                                    <td>$41.00</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Concrete Repairs</td>
                                    <td class="text-center">1</td>
                                    <td>$389.50</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Interior Decoration & Finishing Works</td>
                                    <td class="text-center">1</td>
                                    <td>$1999.05</td>
                                </tr>
                                <tr class="font-bold font-black">
                                    <td colspan="3" class="text-right">TOTAL:</td>
                                    <td colspan="3" class="font-blue font-size-23">$2710.65</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>

        <div class="main-footer clearfix bg-black">
            <div class="container clearfix">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="header">About us</div>
                    <p class="about-us">
                        Subsing is a company that provide a lot of Tender to Customer, a open provider that give you open solution.
                    </p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <h3 class="header">MENU</h3>
                    <div class="posts-list">
                        <ul>
                            <li>
                                <a href="tender-list.html" title="Portfolio">TENDER</a>
                            </li>
                            <li>
                                <a href="subcontractor-list.html" title="Portfolio">SUB CONTRACTOR</a>
                            </li>
                            <li>
                                <a href="faq.html" title="Portfolio">FAQ</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-3">
                    <h3 class="header">Contact us</h3>
                    <ul class="footer-contact">
                        <li>
                            <i class="glyph-icon icon-home"></i>
                            5804 Quaking Embers Trail, Tiger, Missouri
                        </li>
                        <li>
                            <i class="glyph-icon icon-phone"></i>
                            (636) 517-1243
                        </li>
                        <li>
                            <i class="glyph-icon icon-envelope-o"></i>
                            <a href="#" title="">homepage@singsub.com</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="footer-pane">
                <div class="container clearfix">
                    <div class="logo">&copy; 2018 SingSub. All Rights Reserved.</div>
                </div>
            </div>
        </div>

    </div>