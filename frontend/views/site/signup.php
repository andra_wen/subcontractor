<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;

$this->title = 'Signup';
?>
<div id="page-content" class="col-md-10 center-margin frontend-components mrg25T">
            <div class="row">
                <div class="col-md-12">
                    <!-- Bootstrap Wizard -->

                    <!--<link rel="stylesheet" type="text/css" href="assets/widgets/wizard/wizard.css">-->
                    <script type="text/javascript" src="resources/widgets/wizard/wizard.js"></script>
                    <script type="text/javascript" src="resources/widgets/wizard/wizard-demo.js"></script>

                    <!-- Boostrap Tabs -->

                    <script type="text/javascript" src="resources/widgets/tabs/tabs.js"></script>

                    <div class="panel">
                        <div class="panel-body">
                            <h3 class="title-hero">
                                Alternate style
                            </h3>
                            <div class="example-box-wrapper">
                                <div id="form-wizard-3" class="form-wizard">
                                    <ul>
                                        <li>
                                            <a href="#step-1" data-toggle="tab">
                                                <label class="wizard-step">1</label>
                                                <span class="wizard-description">
                                                    User details
                                                    <small>User Basic Details Information</small>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#step-2" data-toggle="tab">
                                                <label class="wizard-step">2</label>
                                                <span class="wizard-description">
                                                    Company information
                                                    <small>Fill the Company Details</small>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#step-3" data-toggle="tab">
                                                <label class="wizard-step">3</label>
                                                <span class="wizard-description">
                                                    Business Expert
                                                    <small>Choose your Business Type</small>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#step-4" data-toggle="tab">
                                                <label class="wizard-step">4</label>
                                                <span class="wizard-description">
                                                    Final steps
                                                    <small>Finish and send the email</small>
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
									<?php $form = ActiveForm::begin([
										'id' => 'demo-form',
										'enableAjaxValidation'=>true,
										'options' => ['class' => 'form-horizontal bordered-row'],
									]) ?>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="step-1">
                                            <div class="content-box">
                                                <h3 class="content-box-header bg-black">
                                                    User Details
                                                </h3>
                                                <div class="content-box-wrapper">
													
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Username</label>
                                                                    <div class="col-sm-6">
																	<?= $form->field($model, 'username')->textInput(['maxlength' => 255, 'placeholder' => "Username", 'class' => 'form-control'])->label(false) ?>
																	</div>
                                                                </div>
																
																<div class="form-group">
                                                                    <label class="col-sm-3 control-label">Firstname</label>
                                                                    <div class="col-sm-6">
																	<?= $form->field($model, 'firstname')->textInput(['maxlength' => 255, 'placeholder' => "Firstname", 'class' => 'form-control'])->label(false) ?>
																	</div>
                                                                </div>
																
																<div class="form-group">
                                                                    <label class="col-sm-3 control-label">Middlename</label>
                                                                    <div class="col-sm-6">
																	<?= $form->field($model, 'middlename')->textInput(['maxlength' => 255, 'placeholder' => "Middlename", 'class' => 'form-control'])->label(false) ?>
																	</div>
                                                                </div>
																
																<div class="form-group">
                                                                    <label class="col-sm-3 control-label">Lastname</label>
                                                                    <div class="col-sm-6">
																	<?= $form->field($model, 'lastname')->textInput(['maxlength' => 255, 'placeholder' => "Lastname", 'class' => 'form-control'])->label(false) ?>
																	</div>
                                                                </div>
																
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Email</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'email')->textInput(['placeholder' => "Email", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Contact Number</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'contact_number')->textInput(['placeholder' => "Phone", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Gender</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'gender')->inline()->radioList($model->getGenderOptions())->label(false) ?>
                                                                    </div>
                                                                </div>
																
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Password</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'password')->passwordInput()->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Repeat Password</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'repeat_password')->passwordInput()->label(false) ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="step-2">
                                            <div class="content-box">
                                                <h3 class="content-box-header bg-black">
                                                    Company Details
                                                </h3>
                                                <div class="content-box-wrapper">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Company Name</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'company_name')->textInput(['maxlength' => 255, 'placeholder' => "Company Name", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Company BRN</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'company_brn')->textInput(['maxlength' => 255, 'placeholder' => "Company BRN", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Country</label>
                                                                    <div class="col-sm-6">
																	<?=
                                                                        $form->field($model, 'country')->widget(Select2::classname(), [
																			'data' => \common\models\SysCountry::getCountry(),
																			'options' => ['placeholder' => 'Select data ...'],
																			'pluginOptions' => [
																				'allowClear' => true
																			],
																		])->label(false);
																	?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Province</label>
                                                                    <div class="col-sm-6">
                                                                        <?= 
																			$form->field($model, 'province')->widget(DepDrop::classname(), [
																				'data'=> \common\models\SysProvince::getProvince(),
																				'options' => ['placeholder' => 'Select ...'],
																				'type' => DepDrop::TYPE_SELECT2,
																				'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
																				'pluginOptions'=>[
																					'depends'=>['signupform-country'],
																					'url' => Url::to(['/site/province']),
																					'loadingText' => 'Loading child level 1 ...',
																				]
																			])->label(false);
																		?>
                                                                    </div>
                                                                </div>
																<div class="form-group">
                                                                    <label class="col-sm-3 control-label">City</label>
                                                                    <div class="col-sm-6">
                                                                         <?= 
																			$form->field($model, 'city')->widget(DepDrop::classname(), [
																				'data'=> \common\models\SysCountyTown::getTown(),
																				'options' => ['placeholder' => 'Select ...'],
																				'type' => DepDrop::TYPE_SELECT2,
																				'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
																				'pluginOptions'=>[
																					'depends'=>['signupform-country','signupform-province'],
																					// 'initialize' => true,
																					// 'initDepends'=>['signupform-country','signupform-province'],
																					'url' => Url::to(['/site/city']),
																					'loadingText' => 'Loading child level 1 ...',
																				]
																			])->label(false);
																		?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">State</label>
                                                                    <div class="col-sm-6">
                                                                        <?= 
																			$form->field($model, 'state')->widget(DepDrop::classname(), [
																				'data'=> \common\models\SysDistricts::getDistricts(),
																				'options' => ['placeholder' => 'Select ...'],
																				'type' => DepDrop::TYPE_SELECT2,
																				'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
																				'pluginOptions'=>[
																					'depends'=>['signupform-country','signupform-province','signupform-city'],
																					// 'initialize' => true,
																					// 'initDepends'=>['signupform-country','signupform-province','signupform-city'],
																					'url' => Url::to(['/site/state']),
																					'loadingText' => 'Loading child level 1 ...',
																				]
																			])->label(false);
																		?>
                                                                    </div>
                                                                </div>                                                                
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Company Address</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'company_address')->textInput(['maxlength' => 255, 'placeholder' => "Company Address 1", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Company Address 2</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'company_address2')->textInput(['maxlength' => 255, 'placeholder' => "Company Address 2", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Zip Code</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'zip_code')->textInput(['maxlength' => 255, 'placeholder' => "Zip Code", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Telephone</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'telephone')->textInput(['maxlength' => 255, 'placeholder' => "Telephone", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Fax</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'fax')->textInput(['maxlength' => 255, 'placeholder' => "Fax", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Company Email</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'company_email')->textInput(['maxlength' => 255, 'placeholder' => "Company Email", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
																	<div class="col-sm-2">
																	</div>
                                                                    <div class="col-sm-8">
                                                                        <div class="checkbox">
                                                                            <label>
																			<?php echo $form->field($model, 'terms')->checkbox(['uncheck' => 'Disabled', 'value' => 'Active'])->label('Agree Accept Terms &amp; Conditions'); ?>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="step-3">
                                            <div class="content-box">
                                                <h3 class="content-box-header bg-black">
                                                    Business Specialize
                                                </h3>
                                                <div class="content-box-wrapper">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <?php $dataType = ArrayHelper::map(\common\models\MasterProducts::find()->all(), 'id', 'product_name'); ?>
																	<?= $form->field($model, 'product[]')->checkboxList($dataType, [
																		'item'=>function ($index, $label, $name, $checked, $value){
																					return "<label class='col-md-4'><input type='checkbox' {$checked} name='{$name}' value='{$value}'>{$label}</label>";
																				},'unselect'=>NULL])->label(false);?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="step-4">
                                            <div class="content-box">
                                                <h3 class="content-box-header bg-black">
                                                    Your Final Verification
                                                </h3>
                                                <div class="content-box-wrapper">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Your Email Confirmation</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'email_confirmation')->textInput(['placeholder' => "Email Confirmation", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Company Invitation</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'invitation_code')->textInput(['placeholder' => "Invitation Code", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="bg-default content-box text-center pad20A mrg25T">
                                                            <?= Html::submitButton('Finish', ['class' => 'btn btn-primary']) ?>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
										<?php ActiveForm::end() ?>
										<div class="pager">
											<div class="pull-right">
												<button class="btn btn-default button-next">
													<i class="glyph-icon icon-arrow-right"></i>
												</button>
												<button class="btn btn-default button-last">
													<i class="glyph-icon icon-chevron-right"></i>
												</button>
											</div>
											<div class="pull-left">
												<button class="btn btn-default button-first">
													<i class="glyph-icon icon-chevron-left"></i>
												</button>
												<button class="btn btn-default button-previous">
													<i class="glyph-icon icon-arrow-left"></i>
												</button>
											</div>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
