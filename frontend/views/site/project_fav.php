<?php

/* @var $this yii\web\View */

$this->title = 'Project Bookmark';
?>
<div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
        <div class="container">
            <h2 class="hero-heading wow fadeInDown text-center font-bold" data-wow-duration="0.6s">List of Favorite Project</h2>
            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-4">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                        <select class="form-control">
                            <option>10</option>
                            <option>25</option>
                            <option>50</option>
                            <option>100</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-8 control-label">Records per Page</div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 pull-right">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 no-padding">
                        <input type="text" class="form-control" id="" placeholder="Search...">
                    </div>
                </div>
            </div>
            <div class="clearfix col-lg-12 col-md-12 col-sm-12 hidden-xs space-10"></div>

            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding border-spacegrey border-l-orange">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-orange">
                                     PT Mechanical Everyday
                                </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="font-bold font-47 line-height-47 helvetica">
                                     PROJECT MILE CONTRACTOR
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-t-5">
                                <span class="font-12">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                                    laborum.
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        </div>
                        <div class="clearfix col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-space-grey font-bold font-40 helvetica">
                                    SUBCON WORKHEAD
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-updn-10">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">General Building</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Civil Engineering</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Concrete Repairs</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow"></span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Interior Decoration & Finishing Works</span>
                                </div>
                            </div>

                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10 border-b-spacegrey"></div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                            

                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Status : 
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    Open to Public
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Registered :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    10 Feb 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                     Project Deadline :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                     23 Apr 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                        <input type="button" onclick="window.location='project-details.html'" class="btn-general-orange" name="btn-view"id="btn-view" value="Details">
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 font-italic no-padding padding-t-10">
                                        <input type="button" class="btn-general-orange" name="btn-create" id="btn-create"
                                            value="Remove">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                        </div>
                    </div>
                </div>

                <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding border-spacegrey border-l-orange">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-orange">
                                     PT Delta
                                </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="font-bold font-47 line-height-47 helvetica">
                                    PROJECT VISION BUILDING
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-t-5">
                                <span class="font-12">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                                    laborum.
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        </div>
                        <div class="clearfix col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-space-grey font-bold font-40 helvetica">
                                    SUBCON WORKHEAD
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-updn-10">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">General Building</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Civil Engineering</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Concrete Repairs</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow"></span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Interior Decoration & Finishing Works</span>
                                </div>
                            </div>
                
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10 border-b-spacegrey"></div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                
                
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Status :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    Open to Public
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Registered :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    10 Feb 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Deadline :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    23 Apr 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                        <input type="button" onclick="window.location='project-details.html'" class="btn-general-orange" name="btn-view" id="btn-view"
                                            value="Details">
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 font-italic no-padding padding-t-10">
                                        <input type="button" class="btn-general-orange" name="btn-create" id="btn-create" value="Remove">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                        </div>
                    </div>
                </div>

                <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding border-spacegrey border-l-orange">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-orange">
                                     PT Samsung
                                </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="font-bold font-47 line-height-47 helvetica">
                                    PROJECT OLED RESEARCH DEVELOPMENT
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-t-5">
                                <span class="font-12">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                                    laborum.
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        </div>
                        <div class="clearfix col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-space-grey font-bold font-40 helvetica">
                                    SUBCON WORKHEAD
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-updn-10">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">General Building</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Civil Engineering</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Concrete Repairs</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow"></span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Interior Decoration & Finishing Works</span>
                                </div>
                            </div>
                
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10 border-b-spacegrey"></div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                
                
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Status :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    Invited Only
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Registered :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    10 Feb 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Deadline :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    23 Apr 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                        <input type="button" onclick="window.location='project-details.html'" class="btn-general-orange" name="btn-view" id="btn-view"
                                            value="Details">
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 font-italic no-padding padding-t-10">
                                        <input type="button" class="btn-general-orange" name="btn-create" id="btn-create" value="Remove">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                        </div>
                    </div>
                </div>

                <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding border-spacegrey border-l-orange">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-orange">
                                     PT Oracle
                                </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="font-bold font-47 line-height-47 helvetica">
                                    PROJECT DATACENTER SOLUTION
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-t-5">
                                <span class="font-12">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                                    laborum.
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        </div>
                        <div class="clearfix col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-space-grey font-bold font-40 helvetica">
                                    SUBCON WORKHEAD
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-updn-10">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">General Building</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Civil Engineering</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Concrete Repairs</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow"></span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Interior Decoration & Finishing Works</span>
                                </div>
                            </div>
                
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10 border-b-spacegrey"></div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                
                
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Status :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    Invited Only
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Registered :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    10 Feb 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Deadline :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    23 Apr 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                        <input type="button" onclick="window.location='project-details.html'" class="btn-general-orange" name="btn-view" id="btn-view"
                                            value="Details">
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 font-italic no-padding padding-t-10">
                                        <input type="button" class="btn-general-orange" name="btn-create" id="btn-create" value="Remove">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                        </div>
                    </div>
                </div>

                <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding border-spacegrey border-l-orange">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-orange">
                                     PT Sun Micro
                                </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="font-bold font-47 line-height-47 helvetica">
                                    PROECT NEW SUNSYSTEM RESEARCH
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-t-5">
                                <span class="font-12">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                                    laborum.
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                        </div>
                        <div class="clearfix col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-space-grey font-bold font-40 helvetica">
                                    SUBCON WORKHEAD
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-updn-10">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">General Building</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Civil Engineering</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Concrete Repairs</span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow"></span>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="font-yellow">Interior Decoration & Finishing Works</span>
                                </div>
                            </div>
                
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10 border-b-spacegrey"></div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                
                
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Status :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    Invited Only
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Registered :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    10 Feb 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Project Deadline :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    23 Apr 2018
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                        <input type="button" onclick="window.location='project-details.html'" class="btn-general-orange" name="btn-view" id="btn-view"
                                            value="Details">
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 font-italic no-padding padding-t-10">
                                        <input type="button" class="btn-general-orange" name="btn-create" id="btn-create" value="Remove">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-10"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="text-center">
                        <ul class="pagination pagination-md">
                            <li>
                                <a href="#">«</a>
                            </li>
                            <li class="active">
                                <a href="#">1
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">2</a>
                            </li>
                            <li>
                                <a href="#">3</a>
                            </li>
                            <li>
                                <a href="#">4</a>
                            </li>
                            <li>
                                <a href="#">5</a>
                            </li>
                            <li>
                                <a href="#">»</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>