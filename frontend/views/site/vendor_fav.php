<?php

/* @var $this yii\web\View */

$this->title = 'Project Bookmark';
?>
<div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
        <div class="container">
            <h2 class="hero-heading wow fadeInDown text-center font-bold" data-wow-duration="0.6s">Your Favorit Vendor</h2>
            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-4">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                        <select class="form-control">
                            <option>10</option>
                            <option>25</option>
                            <option>50</option>
                            <option>100</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-8 control-label">Records per Page</div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 pull-right">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 no-padding">
                        <input type="text" class="form-control" id="" placeholder="Search...">
                    </div>
                </div>
            </div>
            <div class="clearfix col-lg-12 col-md-12 col-sm-12 hidden-xs space-10"></div>

            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding border-spacegrey border-l-orange">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <br>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-bold font-30 helvetica">
                                    PT. PUTRA PUTRI PEMBANGUNAN PABRIK
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Company BRN : 
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    XXdkdADFDF
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Contact Person :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    John Cipil
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Email Address :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    ada@gmail.co.id
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Contact Number :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    081212122787
                                </div>
                            </div>
                        
                            <div class="clearfix col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                    <input type="button" class="btn-general-orange" name="btn-view" id="btn-view"
                                        value="Remove">
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                    
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="clearfix col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="font-space-grey font-bold font-20 helvetica">
                                    Company Address
                                </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                Jl. Kemayoran Utama, No. 88 <br>
                                Persis depan SDN 001
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <br>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                Province : 
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Kemanggisan
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                City :
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Jakarta Barat
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                State :
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Jakarta
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                Country :
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Indonesia
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding border-spacegrey border-l-orange">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <br>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-bold font-30 helvetica">
                                    PT. PUTRA PUTRI PEMBANGUNAN PABRIK
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Company BRN :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    XXdkdADFDF
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Contact Person :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    John Cipil
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Email Address :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    ada@gmail.co.id
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Contact Number :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    081212122787
                                </div>
                            </div>
                
                            <div class="clearfix col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                    <input type="button" class="btn-general-orange" name="btn-view" id="btn-view" value="Remove">
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="clearfix col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="font-space-grey font-bold font-20 helvetica">
                                    Company Address
                                </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                Jl. Kemayoran Utama, No. 88
                                <br> Persis depan SDN 001
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <br>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                Province :
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Kemanggisan
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                City :
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Jakarta Barat
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                State :
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Jakarta
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                Country :
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Indonesia
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding border-spacegrey border-l-orange">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <br>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-bold font-30 helvetica">
                                    PT. PUTRA PUTRI PEMBANGUNAN PABRIK
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Company BRN :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    XXdkdADFDF
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Contact Person :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    John Cipil
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Email Address :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    ada@gmail.co.id
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Contact Number :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    081212122787
                                </div>
                            </div>
                
                            <div class="clearfix col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                    <input type="button" class="btn-general-orange" name="btn-view" id="btn-view" value="Remove">
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="clearfix col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="font-space-grey font-bold font-20 helvetica">
                                    Company Address
                                </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                Jl. Kemayoran Utama, No. 88
                                <br> Persis depan SDN 001
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <br>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                Province :
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Kemanggisan
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                City :
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Jakarta Barat
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                State :
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Jakarta
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                Country :
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Indonesia
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding border-spacegrey border-l-orange">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <br>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-b-1-5">
                                <span class="font-bold font-30 helvetica">
                                    PT. PUTRA PUTRI PEMBANGUNAN PABRIK
                                </span>
                            </div>
                            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Company BRN :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    XXdkdADFDF
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Contact Person :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    John Cipil
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Email Address :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    ada@gmail.co.id
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold no-padding">
                                    Contact Number :
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic no-padding">
                                    081212122787
                                </div>
                            </div>
                
                            <div class="clearfix col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                                    <input type="button" class="btn-general-orange" name="btn-view" id="btn-view" value="Remove">
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 font-italic no-padding padding-t-10">
                
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="clearfix col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="font-space-grey font-bold font-20 helvetica">
                                    Company Address
                                </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                Jl. Kemayoran Utama, No. 88
                                <br> Persis depan SDN 001
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <br>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                Province :
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Kemanggisan
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                City :
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Jakarta Barat
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                State :
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Jakarta
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-bold">
                                Country :
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 font-italic">
                                Indonesia
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                
            </div>
        </div>

        <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>