<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="container">
            <div class="hero-box hero-box-smaller bg-gradient-5 font-inverse">
                <div class="container">
                    <h1 class="pad0A hero-heading font-size-28 font-bold font-yellow wow fadeInDown" data-wow-duration="0.6s">Contact Us</h1>
                    <p class="mrg15B text-center">Please drop us an Email, tell us what we can do</p>
                </div>
                <div class="hero-overlay bg-black"></div>
            </div>

            <div class="mrg25T pad25T pad25B">
                <div class="container mrg25T mrg25B row">
                    <div class="col-md-8">
                        <form class="form-horizontal bordered-row" id="demo-form" data-parsley-validate="">
                            <div class="content-box">
                                <h3 class="content-box-header bg-default">
                                    Send us an email
                                </h3>
                                <div class="content-box-wrapper">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Name:</label>
                                            <div class="col-sm-6">
                                                <input type="text" placeholder="Required Field" required class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Email:</label>
                                            <div class="col-sm-6">
                                                <input type="text" data-parsley-type="email" placeholder="Email address" required class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Subject:</label>
                                            <div class="col-sm-6">
                                                <input type="text" data-parsley-type="url" placeholder="Subject" required class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Message:</label>
                                            <div class="col-sm-6">
                                                <textarea id="" name="" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"></label>
                                            <div class="col-sm-6">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" required name="terms"> Accept Terms &amp; Conditions
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="button-pane">
                                    <button class="btn btn-warning">Send</button>
                                    <button class="btn btn-link font-gray-dark">Cancel</button>
                                </div>
                            </div>
                        </form>
            
                    </div>
                    <div class="col-md-4">
                        <div class="row mrg15T">
                            <div class="col-md-4">
                                <a href="#" title="Follow us on Facebook" class="btn btn-block tooltip-button bg-facebook">
                                    <i class="glyph-icon icon-facebook"></i>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="#" title="Follow us on Twitter" class="btn btn-block tooltip-button bg-twitter">
                                    <i class="glyph-icon icon-twitter"></i>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="#" title="Follow us on Google+" class="btn btn-block tooltip-button bg-google">
                                    <i class="glyph-icon icon-google-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="divider mrg25T mrg25B"></div>
                        <h3>Get in touch</h3>
                        <ul class="contact-list mrg15T mrg25B reset-ul">
                            <li>
                                <i class="glyph-icon icon-home"></i>
                                5804 Quaking Embers Trail, Tiger, Missouri
                            </li>
                            <li>
                                <i class="glyph-icon icon-phone"></i>
                                (636) 517-1243
                            </li>
                            <li>
                                <i class="glyph-icon icon-envelope-o"></i>
                                <a href="#" title="">homepage@example.com</a>
                            </li>
                        </ul>
                        <div class="divider mrg25T mrg25B"></div>
                        <h3 class="mrg15B">About us</h3>
                        <p class="font-gray-dark">For science, music, sport. The languages only differ in their grammar, their pronunciation and their most common
                            words.</p>
                    </div>
                </div>
            </div>
        </div>