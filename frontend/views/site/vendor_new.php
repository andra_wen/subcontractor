<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="container">
            <div class="hero-box hero-box-smaller bg-gradient-5 font-inverse">
                <div class="container">
                    <h1 class="pad0A hero-heading font-size-28 font-bold font-yellow wow fadeInDown" data-wow-duration="0.6s">CREATE NEW SUBCONTRACTOR</h1>
                    <p class="mrg15B text-center">Take no more than 5 minutes to submit everythings</p>
                </div>
                <div class="hero-overlay bg-black"></div>
            </div>

            <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel">
                    <div class="panel-body">
                        <h3 class="title-hero text-center font-orange font-bold">
                            Please fill below Information for Subcontractore Requirements
                        </h3>
                        <div class="example-box-wrapper">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Your Company</label>
                                    <div class="col-lg-6">
                                        <textarea name="" placeholder="Tell us why we should choose you" rows="3" class="form-control textarea-md"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Please tell us your Portofolio</label>
                                    <div class="col-lg-6">
                                        <textarea name="" placeholder="Tell us about your experiences" rows="3" class="form-control textarea-md"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Tell us your main Requirement</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="" placeholder="For Example Workhead...">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">When this project deadline?</label>
                                    <div class="col-lg-6">
                                        <div class="input-prepend input-group">
                                            <span class="add-on input-group-addon">
                                                <i class="glyph-icon icon-calendar"></i>
                                            </span>
                                            <input type="text" class="bootstrap-datepicker form-control" value="02/16/12" data-date-format="mm/dd/yy">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Project Information - Building or Area</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="" placeholder="Specific Landmark">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Project Information - Detail Address</label>
                                    <div class="col-lg-6">
                                        <textarea name="" placeholder="Tell us the detail area" rows="3" class="form-control textarea-md"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Please tell us your Detail Requirements</label>
                                    <div class="col-lg-6">
                                        <textarea name="" placeholder="What is the minimum requirements, duration time, expectation, etc..." rows="3" class="form-control textarea-md"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">What type of this project?</label>
                                    <div class="col-lg-6">
                                        <div class=" radio-warning">
                                            <label>
                                                <input type="radio" id="project_public" name="Project_type" class="custom-radio"> Public
                                            </label>
                                        </div>
                                        <div class=" radio-warning">
                                            <label>
                                                <input type="radio" id="project_invited" name="Project_type" class="custom-radio"> Invited
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Proposal Document</label>
                                    <div class="col-sm-6">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <span class="btn btn-primary btn-file">
                                                <span class="fileinput-new">Select file</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="...">
                                            </span>
                                            <span class="fileinput-filename"></span>
                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Is below price negotiable?</label>
                                    <div class="col-lg-6">
                                        <input type="checkbox" data-on-color="warning" name="checkbox-4" class="input-switch" checked data-size="medium" data-on-text="Yes"
                                            data-off-text="No">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                        <input type="button" href="#" title="" class="btn btn-warning add-author" value="Add Item">
                    </div>
                    
                    <table class="table myTable mrg20T table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th class="text-center">Quantity</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td>
                                    <div class="selector" style="width: 290px;">
                                        <span style="width: 268px; user-select: none;">Civil Engineering</span>
                                        <select name="product-list" class="custom-select">
                                            <option>Civil Engineering</option>
                                            <option>Concrete Repairs</option>
                                            <option>Civil Engineering</option>
                                            <option>Interior Decoration &amp; Finishing Works</option>
                                        </select>
                                        <i class="glyph-icon icon-caret-down"></i>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="col-sm-8">
                                        <input id="touchspin-0" class="form-control" type="text" value="" name="quantity">
                                    </div>
                                </td>
                                <td>
                                    <div class="col-sm-8">
                                        <input id="touchspin-0" class="form-control" type="text" value="" name="price">
                                    </div>
                                    <input type="button" class="btn btn-danger remove" value="Remove">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="clearfix col-sm-12 font-italic no-padding">
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-5 font-italic no-padding padding-t-10 pull-right">
                            <input type="button" class="btn-general-white" name="btn-cancel" id="btn-create" value="Cancel">
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-5 font-italic no-padding padding-t-10 pull-right">
                            <input type="button" class="btn-general-orange" name="btn-create" id="btn-create" value="Create">
                        </div>
                    </div>
                    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
                </div>

                <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 space-20"></div>
            </div>
        </div>