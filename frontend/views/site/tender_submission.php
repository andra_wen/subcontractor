<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
	<!-- Form Radio-->

    <script type="text/javascript" src="resources/widgets/uniform/uniform.js"></script>
    <script type="text/javascript" src="resources/widgets/uniform/uniform-demo.js"></script>

    <!-- PieGage charts -->
    
    <script type="text/javascript" src="resources/widgets/charts/piegage/piegage.js"></script>
    <script type="text/javascript" src="resources/widgets/charts/piegage/piegage-demo.js"></script>

<div id="page-content" class="col-md-10 center-margin frontend-components mrg25T">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 no-padding">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 content-box">
                            <div class="login-box clearfix">
                                <div class="user-img">
                                    <?php
										$hasil = \common\models\CustUsersProfile::getCustprofilebyuserid(Yii::$app->user->identity->id);
										$url = Yii::$app->urlBackendImage->baseUrl;
										echo Html::img(Yii::$app->urlBackendImage->baseUrl.'/'.$hasil['user_picture'],['alt'=>"User Image",'width'=>'45px','height'=>'45px']);
									?>
                                </div>
								<?php 
									$names = \common\models\CustUsersProfile::getCustprofilebyuserid(Yii::$app->user->identity->id);
									$usernames = $names['user_firstname'].' '.$names['user_lastname'];
								?>
                                <div class="user-info">
                                    <span>
                                        <?= $usernames ?>
										<?php $roles = Yii::$app->user->identity->roles_name; ?>
                                        <i><?= $roles == 10 ? 'Backend' : ($roles == 20 ? 'Subcontractor' : 'Vendor') ?></i>
                                    </span>
                                </div>
								<?= Html::a('Edit Profile', ['edit-profile','id'=>Yii::$app->user->identity->id], []) ?><br>
								<?= Html::a('Change Password', ['change-password','id'=>Yii::$app->user->identity->id], []) ?>
                            </div>
                        </div>
                        <div class="list-group">
							<?= Html::a('<i class="glyph-icon icon-dashboard"></i>
                                Project Dashboard
                                <i class="glyph-icon icon-chevron-right"></i>', ['project-dashboard','id'=>Yii::$app->user->identity->id], ['class' => 'list-group-item']) 
							?>
							<?= Html::a('<i class="glyph-icon icon-dashboard"></i>
                                My Invitation
                                <i class="glyph-icon icon-chevron-right"></i>', ['project-invitation','id'=>Yii::$app->user->identity->id], ['class' => 'list-group-item']) 
							?>
							<?= Html::a('<i class="glyph-icon icon-dashboard"></i>
                                My Submission
                                <i class="glyph-icon icon-chevron-right"></i>', ['tender-submission','id'=>Yii::$app->user->identity->id], ['class' => 'list-group-item active']) 
							?>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 no-padding border-grey">
                        <div class="mrg20T text-center pad10A ">
                            <a href="#" class="btn center-div btn-black updateEasyPieChart">
                                <i class="glyph-icon icon-refresh"></i>
                                Refresh all charts
                            </a>
                        </div>
                        <div class="panel">
                            <div class="panel-body">
                                <h3 class="title-hero">
                                    Submission Dashboard
                                </h3>
                                <div class="example-box-wrapper">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="dashboard-box bg-blue-alt">
                                                <div class="content-wrapper">
                                                    <div class="header">
                                                        Last Month
                                                        <span>July - December 2018</span>
                                                    </div>
                                                    <div class="sparkline-big">183,579,180,311,405,342,579,405,450,311,180,311,450,302,370,210</div>
                                                </div>
                                                <div class="button-pane">
                                                    <div class="size-md float-left heading center-margin">
                                                        Total Invitation
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#" title="Example tile shortcut" class="tile-box tile-box-alt btn-black">
                                                <div class="tile-header">
                                                    Total Invitation
                                                </div>
                                                <div class="tile-content-wrapper">
                                                    <div class="chart-alt-10" data-percent="67">
                                                        <span>67</span>%</div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#" title="Example tile shortcut" class="tile-box tile-box-alt btn-danger">
                                                <div class="tile-header">
                                                     Invitation Responded
                                                </div>
                                                <div class="tile-content-wrapper">
                                                    <div class="chart-alt-10" data-percent="55">
                                                        <span>55</span>%</div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#" title="Example tile shortcut" class="tile-box tile-box-alt btn-success">
                                                <div class="tile-header">
                                                    Invitation Pending
                                                </div>
                                                <div class="tile-content-wrapper">
                                                    <div class="chart-alt-10" data-percent="43">
                                                        <span>43</span>%</div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#" title="Example tile shortcut" class="tile-box tile-box-alt btn-info">
                                                <div class="tile-header">
                                                    Invitation Expired
                                                </div>
                                                <div class="tile-content-wrapper">
                                                    <div class="chart-alt-10" data-percent="76">
                                                        <span>76</span>%</div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <br>