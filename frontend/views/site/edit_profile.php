<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use kartik\widgets\FileInput;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div id="page-content" class="col-md-10 center-margin frontend-components mrg25T">
            <div class="row">
                <div class="col-md-12">
                    <!-- Bootstrap Wizard -->

                    <!--<link rel="stylesheet" type="text/css" href="assets/widgets/wizard/wizard.css">-->
                    <script type="text/javascript" src="resources/widgets/wizard/wizard.js"></script>
                    <script type="text/javascript" src="resources/widgets/wizard/wizard-demo.js"></script>

                    <!-- Boostrap Tabs -->

                    <script type="text/javascript" src="resources/widgets/tabs/tabs.js"></script>

                    <div class="panel">
                        <div class="panel-body">
                            <h3 class="title-hero">
                                Alternate style
                            </h3>
                            <div class="example-box-wrapper">
                                <div id="form-wizard-3" class="form-wizard">
                                    <ul>
                                        <li>
                                            <a href="#step-1" data-toggle="tab">
                                                <label class="wizard-step">1</label>
                                                <span class="wizard-description">
                                                    User details
                                                    <small>User Basic Details Information</small>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#step-2" data-toggle="tab">
                                                <label class="wizard-step">2</label>
                                                <span class="wizard-description">
                                                    Company information
                                                    <small>Update the Company Details</small>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#step-3" data-toggle="tab">
                                                <label class="wizard-step">3</label>
                                                <span class="wizard-description">
                                                    Business Expert
                                                    <small>Update your Business Type</small>
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
									<?php $form = ActiveForm::begin([
										'id' => 'edit-form',
										// 'enableAjaxValidation'=>true,
										'options' => ['enctype'=>'multipart/form-data','class' => 'form-horizontal bordered-row'],
									]) ?>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="step-1">
                                            <div class="content-box">
                                                <h3 class="content-box-header bg-black">
                                                    User Details
                                                </h3>
                                                <div class="content-box-wrapper">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Username</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($modelcUser, 'username')->textInput(['maxlength' => 255, 'placeholder' => "Username", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Email</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($modelcUser, 'email')->textInput(['maxlength' => 255, 'placeholder' => "Email", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Contact Number</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($modelcUserAddress, 'address_phonenumber')->textInput(['maxlength' => 255, 'placeholder' => "Contact Number", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Gender</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($modelcUserProfile, 'gender')->inline()->radioList($modelcUserProfile->getGenderOptions())->label(false) ?>
                                                                    </div>
                                                                </div>
																<div class="form-group">
                                                                    <label class="col-sm-3 control-label">Marriage Status</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($modelcUserProfile, 'marriagestatus')->inline()->dropdownList($modelcUserProfile->getMarriage())->label(false) ?>
                                                                    </div>
                                                                </div>
																<div class="form-group">
                                                                    <label class="col-sm-3 control-label">Blood Type</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($modelcUserProfile, 'bloodtype')->inline()->dropdownList($modelcUserProfile->getBlood())->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">User Image</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($modelcUserProfile, 'user_picture')->widget(FileInput::classname(), [
																			'options' => ['accept' => 'image/*'],
																			'pluginOptions' => [
																				'showCaption' => false,
																				'showRemove' => false,
																				'showUpload' => false,
																				'browseClass' => 'btn btn-primary btn-block',
																				'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
																				'browseLabel' =>  'Select Photo',
																				'initialPreview'=>[
																					Html::img(Yii::$app->urlBackendImage->baseUrl.'/'.$modelcUserProfile->user_picture,['width'=>100])
																				],
																				'overwriteInitial'=>true
																			],
																			])->label(false); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="step-2">
                                            <div class="content-box">
                                                <h3 class="content-box-header bg-black">
                                                    Company Details
                                                </h3>
                                                <div class="content-box-wrapper">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Company Name</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($modelcUserAddress, 'address_name')->textInput(['maxlength' => 255, 'placeholder' => "Company Name", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Company BRN</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($modelcUserAddress, 'address_remarks')->textInput(['maxlength' => 255, 'placeholder' => "Company BRN", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Country</label>
                                                                    <div class="col-sm-6">
																	<?=
                                                                        $form->field($modelcUserAddress, 'address_country_id')->widget(Select2::classname(), [
																			'data' => \common\models\SysCountry::getCountry(),
																			'options' => ['placeholder' => 'Select data ...'],
																			'pluginOptions' => [
																				'allowClear' => true
																			],
																		])->label(false);
																	?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Province</label>
                                                                    <div class="col-sm-6">
                                                                        <?= 
																			$form->field($modelcUserAddress, 'address_province_id')->widget(DepDrop::classname(), [
																				'data'=> \common\models\SysProvince::getProvince(),
																				'options' => ['placeholder' => 'Select ...'],
																				'type' => DepDrop::TYPE_SELECT2,
																				'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
																				'pluginOptions'=>[
																					'depends'=>['custusersaddress-address_country_id'],
																					// 'initialize' => true,
																					// 'initDepends'=>['custusersaddress-address_country_id'],
																					'url' => Url::to(['/site/province']),
																					'loadingText' => 'Loading child level 1 ...',
																				]
																			])->label(false);
																		?>
                                                                    </div>
                                                                </div>
																<div class="form-group">
                                                                    <label class="col-sm-3 control-label">City</label>
                                                                    <div class="col-sm-6">
                                                                         <?= 
																			$form->field($modelcUserAddress, 'address_countytown_id')->widget(DepDrop::classname(), [
																				'data'=> \common\models\SysCountyTown::getTown(),
																				'options' => ['placeholder' => 'Select ...'],
																				'type' => DepDrop::TYPE_SELECT2,
																				'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
																				'pluginOptions'=>[
																					'depends'=>['custusersaddress-address_country_id','custusersaddress-address_province_id'],
																					// 'initialize' => true,
																					// 'initDepends'=>['custusersaddress-address_country_id','custusersaddress-address_province_id'],
																					'url' => Url::to(['/site/city']),
																					'loadingText' => 'Loading child level 1 ...',
																				]
																			])->label(false);
																		?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">State</label>
                                                                    <div class="col-sm-6">
                                                                        <?= 
																			$form->field($modelcUserAddress, 'address_districts_id')->widget(DepDrop::classname(), [
																				'data'=> \common\models\SysDistricts::getDistricts(),
																				'options' => ['placeholder' => 'Select ...'],
																				'type' => DepDrop::TYPE_SELECT2,
																				'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
																				'pluginOptions'=>[
																					'depends'=>['custusersaddress-address_country_id','custusersaddress-address_province_id','custusersaddress-address_countytown_id'],
																					// 'initialize' => true,
																					// 'initDepends'=>['custusersaddress-address_country_id','custusersaddress-address_province_id','custusersaddress-address_countytown_id'],
																					'url' => Url::to(['/site/state']),
																					'loadingText' => 'Loading child level 1 ...',
																				]
																			])->label(false);
																		?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Company Address</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($modelcUserAddress, 'address_line1')->textInput(['maxlength' => 255, 'placeholder' => "Company Address", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Company Address 2</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($modelcUserAddress, 'address_line2')->textInput(['maxlength' => 255, 'placeholder' => "Company Address 2", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Zip Code</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($modelcUserAddress, 'address_postalzip')->textInput(['maxlength' => 255, 'placeholder' => "Zip Code", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Telephone</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($modelcUserAddress, 'address_phonenumber')->textInput(['maxlength' => 255, 'placeholder' => "Telephone", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Fax</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($modelcUserAddress, 'address_faxnumber')->textInput(['maxlength' => 255, 'placeholder' => "Fax", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Company Email</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($modelcUserAddress, 'address_email')->textInput(['maxlength' => 255, 'placeholder' => "Company Email", 'class' => 'form-control'])->label(false) ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="step-3">
                                            <div class="content-box">
                                                <h3 class="content-box-header bg-black">
                                                    Business Specialize
                                                </h3>
                                                <div class="content-box-wrapper">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
																	<div class="col-md-12">
																		<?php 
																			$dataType = ArrayHelper::map(\common\models\MasterProducts::find()->all(), 'id', 'product_name');
																			if(!$modelcUserProduct->isNewRecord) {
																				$checkeds = [];
																				foreach($checkedList as $data => $value){
																					$checkeds[]=$value['product_id'];
																				}
																					$modelcUserProduct->product_id = $checkeds;
																			 }
																		?>
																		<?= $form->field($modelcUserProduct, 'product_id')->checkboxList($dataType, [
																			'item'=>function ($index, $label, $name, $checked, $value){
																						$checked = $checked ? 'checked' : '';
																						return "<label class='col-md-4'><input type='checkbox' {$checked} name='{$name}' value='{$value}'>{$label}</label>";
																					}])->label(false);?>
																	</div>
                                                                </div>
                                                            </div>
                                                        </div>
														<div class="bg-default content-box text-center pad20A mrg25T">
                                                            <?= Html::submitButton('Finish', ['class' => 'btn btn-primary']) ?>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
									<?php ActiveForm::end() ?>
									<div class="pager">
										<div class="pull-right">
											<button class="btn btn-default button-next">
												<i class="glyph-icon icon-arrow-right"></i>
											</button>
											<button class="btn btn-default button-last">
												<i class="glyph-icon icon-chevron-right"></i>
											</button>
										</div>
										<div class="pull-left">
											<button class="btn btn-default button-first">
												<i class="glyph-icon icon-chevron-left"></i>
											</button>
											<button class="btn btn-default button-previous">
												<i class="glyph-icon icon-arrow-left"></i>
											</button>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>