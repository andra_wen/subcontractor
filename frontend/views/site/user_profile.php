<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div id="page-content" class="col-md-10 center-margin frontend-components mrg25T">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 no-padding">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 content-box">
                            <div class="login-box clearfix">
                                <div class="user-img">
                                    <?php
										$hasil = \common\models\CustUsersProfile::getCustprofilebyuserid(Yii::$app->user->identity->id);
										$url = Yii::$app->urlBackendImage->baseUrl;
										echo Html::img(Yii::$app->urlBackendImage->baseUrl.'/'.$hasil['user_picture'],['alt'=>"User Image",'width'=>'45px','height'=>'45px']);
									?>
                                </div>
								<?php 
									$names = \common\models\CustUsersProfile::getCustprofilebyuserid(Yii::$app->user->identity->id);
									$usernames = $names['user_firstname'].' '.$names['user_lastname'];
								?>
                                <div class="user-info">
                                    <span>
                                        <?= $usernames ?>
										<?php $roles = Yii::$app->user->identity->roles_name; ?>
                                        <i><?= $roles == 10 ? 'Backend' : ($roles == 20 ? 'Subcontractor' : 'Vendor') ?></i>
                                    </span>
                                </div>
								<?= Html::a('Edit Profile', ['edit-profile','id'=>Yii::$app->user->identity->id], []) ?><br>
								<?= Html::a('Change Password', ['change-password','id'=>Yii::$app->user->identity->id], []) ?>
                            </div>
                        </div>
                        <div class="list-group">
							<?= Html::a('<i class="glyph-icon icon-dashboard"></i>
                                Project Dashboard
                                <i class="glyph-icon icon-chevron-right"></i>', ['project-dashboard','id'=>Yii::$app->user->identity->id], ['class' => 'list-group-item']) 
							?>
							<?= Html::a('<i class="glyph-icon icon-dashboard"></i>
                                My Invitation
                                <i class="glyph-icon icon-chevron-right"></i>', ['project-invitation','id'=>Yii::$app->user->identity->id], ['class' => 'list-group-item']) 
							?>
							<?= Html::a('<i class="glyph-icon icon-dashboard"></i>
                                My Submission
                                <i class="glyph-icon icon-chevron-right"></i>', ['tender-submission','id'=>Yii::$app->user->identity->id], ['class' => 'list-group-item']) 
							?>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 no-padding border-grey">
                        <div class="panel">
                            <div class="panel-body">
                                <h3 class="title-hero">
                                    User Basic Information
                                </h3>
                                <div class="example-box-wrapper">
                                    <div class="row">
                                        <div class="col-md-6">
                                            Username : 
                                        </div>
                                        <div class="col-md-6">
                                            <?= $modelcUser->username; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            Email :
                                        </div>
                                        <div class="col-md-6">
                                            <?= $modelcUser->email; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            Contact Number :
                                        </div>
                                        <div class="col-md-6">
                                            <?= $modelcUserAddress->address_phonenumber; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            Gender : 
                                        </div>
                                        <div class="col-md-6">
                                            <?= $modelcUserProfile->gender == 'M' ? 'Male' : 'Female'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-body">
                                <h3 class="title-hero">
                                    Company Information
                                </h3>
                                <div class="example-box-wrapper">
                                    <div class="row">
                                        <div class="col-md-6">
                                            Company Name :
                                        </div>
                                        <div class="col-md-6">
                                            <?= $modelcUserAddress->address_name; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            Company BRN :
                                        </div>
                                        <div class="col-md-6">
                                            <?= $modelcUserAddress->address_remarks; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            Company Email :
                                        </div>
                                        <div class="col-md-6">
                                            <?= $modelcUserAddress->address_email; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            Telephone :
                                        </div>
                                        <div class="col-md-6">
                                            <?= $modelcUserAddress->address_phonenumber; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            Fax Number :
                                        </div>
                                        <div class="col-md-6">
                                            <?= $modelcUserAddress->address_faxnumber; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            Company Email :
                                        </div>
                                        <div class="col-md-6">
                                            <?= $modelcUserAddress->address_email; ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <br>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3 class="title-hero">
                                                Address
                                            </h3>
                                        </div>
                                        <div class="col-md-6">
                                            Country : 
                                        </div>
                                        <div class="col-md-6">
                                            <?php
												$country = \common\models\SysCountry::findOne($modelcUserAddress->address_country_id);
												echo $country->short_name;
											?>
                                        </div>
                                        <div class="col-md-6">
                                            State : 
                                        </div>
                                        <div class="col-md-6">
                                            <?php
												$districts = \common\models\SysDistricts::findOne($modelcUserAddress->address_districts_id);
												echo $districts->name;
											?>
                                        </div>
                                        <div class="col-md-6">
                                            City : 
                                        </div>
                                        <div class="col-md-6">
                                            <?php
												$countytown = \common\models\SysCountyTown::findOne($modelcUserAddress->address_countytown_id);
												echo $countytown->name;
											?>
                                        </div>
                                        <div class="col-md-6">
                                            Province : 
                                        </div>
                                        <div class="col-md-6">
                                            <?php
												$province = \common\models\SysProvince::findOne($modelcUserAddress->address_province_id);
												echo $province->name;
											?>
                                        </div>
                                        <div class="col-md-6">
                                            Address : 
                                        </div>
                                        <div class="col-md-6">
                                            <?= $modelcUserAddress->address_line1; ?>
                                        </div>
                                        <div class="col-md-6">
                                            Address 2 : 
                                        </div>
                                        <div class="col-md-6">
                                            <?= $modelcUserAddress->address_line2; ?>
                                        </div>
                                        <div class="col-md-6">
                                            Zip Code : 
                                        </div>
                                        <div class="col-md-6">
                                            <?= $modelcUserAddress->address_postalzip; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-body">
                                <h3 class="title-hero">
                                    Business Expert
                                </h3>
                                <div class="example-box-wrapper">
                                    <div class="row">
										<?php
											$products = \common\models\CustUsersProduct::find()->where(['cust_user_id' => Yii::$app->user->identity->id])->all();
											foreach($products as $data => $value){
												$name = \common\models\MasterProducts::find()->where(['id' => $value['product_id']])->one();
											
										?>
											<div class="col-md-6">
												<?= $name['product_name'] ?>
											</div>
										<?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <br>