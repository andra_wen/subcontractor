<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use kartik\widgets\FileInput;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div id="page-content" class="col-md-10 center-margin frontend-components mrg25T">
            <div class="row">
                <div class="col-md-12">
                    <!-- Bootstrap Wizard -->

                    <!--<link rel="stylesheet" type="text/css" href="assets/widgets/wizard/wizard.css">-->
                    <script type="text/javascript" src="resources/widgets/wizard/wizard.js"></script>
                    <script type="text/javascript" src="resources/widgets/wizard/wizard-demo.js"></script>

                    <!-- Boostrap Tabs -->

                    <script type="text/javascript" src="resources/widgets/tabs/tabs.js"></script>

                    <div class="panel">
                        <div class="panel-body">
                            <h3 class="title-hero">
                                Alternate style
                            </h3>
                            <div class="example-box-wrapper">
                                <div id="form-wizard-3" class="form-wizard">
                                    <ul>
                                        <li>
                                            <a href="#step-4" data-toggle="tab">
                                                <label class="wizard-step"></label>
                                                <span class="wizard-description">
                                                    Change Password
                                                    <small>Change password</small>
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
									<?php $form = ActiveForm::begin([
										'id' => 'change-password',
										// 'enableAjaxValidation'=>true,
										'options' => ['class' => 'form-horizontal bordered-row'],
									]) ?>
                                    <div class="tab-content">
                                        <div class="tab-pane" id="step-4">
                                            <div class="content-box">
                                                <h3 class="content-box-header bg-black">
                                                    Change Password
                                                </h3>
                                                <div class="content-box-wrapper">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Old Password</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'oldPassword')->passwordInput()->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">New Password</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'newPassword')->passwordInput()->label(false) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Repeat Password</label>
                                                                    <div class="col-sm-6">
                                                                        <?= $form->field($model, 'retypePassword')->passwordInput()->label(false) ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
														<div class="bg-default content-box text-center pad20A mrg25T">
                                                            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
									<?php ActiveForm::end() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>