<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="owl-slider-1 slider-wrapper">
	<div>
		<img src="resources/image-resources/slides-bg/slider-cat1.jpg" class="img-full" alt="Example alternate text">
	</div>
	<div>
		<img src="resources/image-resources/slides-bg/slider-cat1.jpg" class="img-full" alt="Example alternate text">
	</div>
</div>
<br>
        <br>
        <br>

        <div class="cta-box-btn bg-yellow">
            <div class="container">
                <a href="#" class="btn btn-black" title="">
                    Join us as Subcontractor
                    <span>It takes less than 5 minutes to get everything set up.</span>
                </a>
            </div>
        </div>
        <div class="owl-carousel-1 xlarge-padding col-md-11 center-margin slider-wrapper carousel-wrapper carousel-wrapper-alt carousel-container">
            <div>
                <div class="testimonial-box">
                    <div class="popover top mrg10A">
                        <div class="arrow float-right"></div>
                        <div class="popover-content">
                            <i class="glyph-icon icon-quote-left"></i>
                            <p>If several languages coalesce, the grammar of the resulting language is more simple and regular
                                than that of the individual languages. The regular than the existing European languages.</p>
                        </div>
                    </div>
                    <div class="testimonial-author-wrapper">
                        <img class="img-circle float-right" src="assets/image-resources/people/testimonial1.jpg" alt="">
                        <div class="testimonial-author text-right">
                            <b>John Wayne</b>
                            <span>Manager, ACME Inc.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="testimonial-box">
                    <div class="popover top mrg10A">
                        <div class="arrow float-right"></div>
                        <div class="popover-content">
                            <i class="glyph-icon icon-quote-left"></i>
                            <p>If several languages coalesce, the grammar of the resulting language is more simple and regular
                                than that of the individual languages. The regular than the existing European languages.</p>
                        </div>
                    </div>
                    <div class="testimonial-author-wrapper">
                        <img class="img-circle float-right" src="assets/image-resources/people/testimonial2.jpg" alt="">
                        <div class="testimonial-author text-right">
                            <b>John Wayne</b>
                            <span>Manager, ACME Inc.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="testimonial-box">
                    <div class="popover top mrg10A">
                        <div class="arrow float-right"></div>
                        <div class="popover-content">
                            <i class="glyph-icon icon-quote-left"></i>
                            <p>If several languages coalesce, the grammar of the resulting language is more simple and regular
                                than that of the individual languages. The regular than the existing European languages.</p>
                        </div>
                    </div>
                    <div class="testimonial-author-wrapper">
                        <img class="img-rounded float-right" src="assets/image-resources/people/testimonial3.jpg" alt="">
                        <div class="testimonial-author text-right">
                            <b>John Wayne</b>
                            <span>Manager, ACME Inc.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="testimonial-box">
                    <div class="popover top mrg10A">
                        <div class="arrow float-right"></div>
                        <div class="popover-content">
                            <i class="glyph-icon icon-quote-left"></i>
                            <p>If several languages coalesce, the grammar of the resulting language is more simple and regular
                                than that of the individual languages. The regular than the existing European languages.</p>
                        </div>
                    </div>
                    <div class="testimonial-author-wrapper">
                        <img class="img-rounded float-right" src="assets/image-resources/people/testimonial4.jpg" alt="">
                        <div class="testimonial-author text-right">
                            <b>John Wayne</b>
                            <span>Manager, ACME Inc.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="testimonial-box">
                    <div class="popover top mrg10A">
                        <div class="arrow float-right"></div>
                        <div class="popover-content">
                            <i class="glyph-icon icon-quote-left"></i>
                            <p>If several languages coalesce, the grammar of the resulting language is more simple and regular
                                than that of the individual languages. The regular than the existing European languages.</p>
                        </div>
                    </div>
                    <div class="testimonial-author-wrapper">
                        <img class="img-rounded float-right" src="assets/image-resources/people/testimonial5.jpg" alt="">
                        <div class="testimonial-author text-right">
                            <b>John Wayne</b>
                            <span>Manager, ACME Inc.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="testimonial-box">
                    <div class="popover top mrg10A">
                        <div class="arrow float-right"></div>
                        <div class="popover-content">
                            <i class="glyph-icon icon-quote-left"></i>
                            <p>If several languages coalesce, the grammar of the resulting language is more simple and regular
                                than that of the individual languages. The regular than the existing European languages.</p>
                        </div>
                    </div>
                    <div class="testimonial-author-wrapper">
                        <img class="img-rounded float-right" src="assets/image-resources/people/testimonial6.jpg" alt="">
                        <div class="testimonial-author text-right">
                            <b>John Wayne</b>
                            <span>Manager, ACME Inc.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="cta-box-btn bg-yellow">
            <div class="container">
                <a href="#" class="btn btn-black" title="">
                    Join our Tender
                    <span>It takes less than 5 minutes to set everything set up</span>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-1 col-md-1 hidden-sm hidden-xs"></div>
                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 no-padding box-offering">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding">
                        <img class="img img-responsive img-offering" src="assets/image-resources/main-images/icon-offeringservice.png">
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding box-offeringlogo">
                            <img class="img img-logo" src="assets/image-resources/logo.png">
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="service-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                labore</div>
                        </div>

                        <form id="form-offering" action="offering-new.html">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-lg-10 col-md-11 col-sm-10 col-xs-12 padding-10">
                                        <input type="text" class="input-productname padding-10 border-spacegrey" name="input-productname" id="input-productname"
                                            placeholder="isi nama produk yang anda minta. contoh : helm">
                                    </div>
                                    <div class="col-lg-2 col-md-1 col-sm-2 hidden-xs padding-10"></div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-10">
                                        <div class="input-group spinner">
                                            <input type="text" class="form-control input-number" value="12">
                                            <div class="input-group-btn-vertical">
                                                <button class="btn btn-default" type="button">
                                                    <i class="fa fa-caret-up"></i>
                                                </button>
                                                <button class="btn btn-default" type="button">
                                                    <i class="fa fa-caret-down"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-10">
                                        <select class="product-uom">
                                            <option>Piece</option>
                                            <option>KG</option>
                                            <option>Unit</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs padding-10"></div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                <div class="form-group">
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding">
                                        <input type="submit" class="btn-offering" name="btn-login" id="btn-login" value="CREATE TENDER">
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 hidden-xs no-padding"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-1 col-md-1 hidden-sm hidden-xs"></div>
            </div>
        </div>
