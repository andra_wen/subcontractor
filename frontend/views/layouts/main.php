<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\assets\ResourceAsset;
use common\widgets\Alert;
use common\models\MasterProducts;
use yii\helpers\ArrayHelper;

ResourceAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
	<script type="text/javascript">
        $(window).load(function () {
            setTimeout(function () {
                $('#loading').fadeOut(400, "linear");
            }, 300);
        });
    </script>
	<div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
	<div class="top-bar bg-black font-inverse">
            <div class="container">
                <div class="float-left">
                    <!--<a href="#" class="btn btn-sm bg-facebook tooltip-button" data-placement="bottom" title="Follow us on Facebook">
                        <i class="glyph-icon icon-facebook"></i>
                    </a>
                    <a href="#" class="btn btn-sm bg-google tooltip-button" data-placement="bottom" title="Follow us on Google+">
                        <i class="glyph-icon icon-google-plus"></i>
                    </a>
                    <a href="#" class="btn btn-sm bg-twitter tooltip-button" data-placement="bottom" title="Follow us on Twitter">
                        <i class="glyph-icon icon-twitter"></i>
                    </a>-->

                    <a href="#" class="btn btn-top btn-sm" title="Give us a call">
                        <i class="glyph-icon icon-phone"></i>
                        +1-541-754-3010
                    </a>
                </div>
                <div class="float-right user-account-btn dropdown">
                    <a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown" aria-expanded="false">
                        <?php
						if (!Yii::$app->user->isGuest){
								$names = \common\models\CustUsersProfile::getCustprofilebyuserid(Yii::$app->user->identity->id);
								$usernames = $names['user_firstname'].' '.$names['user_lastname'];
								$hasil = \common\models\CustUsersProfile::getCustprofilebyuserid(Yii::$app->user->identity->id);
								$url = Yii::$app->urlBackendImage->baseUrl;
								echo Html::img(Yii::$app->urlBackendImage->baseUrl.'/'.$hasil['user_picture'],['class'=>'glyph-icon img-circle','alt'=>"User Image",'width'=>'45px','height'=>'45px']);
							echo '<span>'.$usernames.'</span>';?>
							<i class="glyph-icon icon-angle-down"></i>
						<?php
						}else{						
						?>
						<span>Login</span>
						<i class="glyph-icon icon-angle-down"></i>
						<?php } ?>                        
                    </a>
                    <div class="dropdown-menu pad0B float-right">
                        <div class="box-sm">
						<div class="login-box clearfix">
							<?php if (!Yii::$app->user->isGuest){ ?>
							<?php $form = ActiveForm::begin([]); ?>						
									<div class="user-img">
											<?php
												$hasil = \common\models\CustUsersProfile::getCustprofilebyuserid(Yii::$app->user->identity->id);
												$url = Yii::$app->urlBackendImage->baseUrl;
												echo Html::img(Yii::$app->urlBackendImage->baseUrl.'/'.$hasil['user_picture'],['alt'=>"User Image",'width'=>'45px','height'=>'45px']);
											?>
									</div>
									<div class="user-info">
										<span>
											<?php 
												echo $usernames;
											?>
										</span>
										<?= Html::a('My Profile', ['user-profile','id'=>Yii::$app->user->identity->id], []) ?>
									</div>
								<div class="divider"></div>
								<ul class="reset-ul mrg5B">
									<li>
										<?= Html::a('My Project', ['project-dashboard','id'=>Yii::$app->user->identity->id])?>
									</li>
									<li>
										<?= Html::a('My Invitation', ['project-invitation','id'=>Yii::$app->user->identity->id])?>
									</li>
									<li>
										<?= Html::a('My Submission', ['tender-submission','id'=>Yii::$app->user->identity->id])?>
									</li>
								</ul>
								<?php echo Html::a('<i class="glyph-icon icon-power-off"></i> Logout',['/site/logout'],['class'=>'btn display-block font-normal btn-danger','data-method' => 'post']) ?>
								<?php ActiveForm::end(); ?>
						</div>
							<?php }else{?>
                            
								<?php $form = ActiveForm::begin(['action' => ['site/login'], 'id' => 'login-form', 'enableClientValidation'=> true]); ?>
                                    <div class="content-box">
                                        <h3 class="content-box-header content-box-header-alt bg-default">
                                            <span class="icon-separator">
                                                <i class="glyph-icon icon-cog"></i>
                                            </span>
                                            <span class="header-wrapper">
                                                Members area
                                                <small>Use the form below to login to your account.</small>
                                            </span>
                                        </h3>
                                        <div class="content-box-wrapper">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="loginform-username" name="LoginForm[username]" placeholder="Enter Username" required>
                                                    <span class="input-group-addon bg-blue">
                                                        <i class="glyph-icon icon-envelope-o"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="password" class="form-control" id="loginform-password" name="LoginForm[password]" placeholder="Password" required>
                                                    <span class="input-group-addon bg-blue">
                                                        <i class="glyph-icon icon-unlock-alt"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <a href="" title="Recover password">Forgot Your Password?</a>
                                            </div>
                                            <button class="btn btn-success btn-block">Sign In</button>
                                        </div>
                                    </div>
                                <?php ActiveForm::end(); ?>
                                <button onclick="location.href = '<?=Url::to(['site/signup']);?>';" class="btn btn-sm btn-warning btn-block">Sign Up</button>
                            </div>
							<?php }?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
		<!-- .top-bar -->
        <div class="main-header animated wow fadeInDown bg-black font-inverse">
            <div class="container">
                <a href="index.php" class="header-logo" title="Monarch - Create perfect presentation websites"></a>
                <!-- .header-logo -->
                <div class="right-header-btn">
                    <div id="mobile-navigation">
                        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target=".header-nav">
                            <span></span>
                        </button>
                    </div>
                    <div class="search-btn">
                        <a href="#" class="popover-button" title="Search" data-placement="bottom" data-id="#popover-search">
                            <i class="glyph-icon icon-search"></i>
                        </a>
                        <div class="hide" id="popover-search">
                            <div class="pad5A box-md">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search terms here ...">
                                    <span class="input-group-btn">
                                        <a class="btn btn-primary" href="#">Search</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .header-logo -->
                <ul class="header-nav collapse">
                    <li>
                        <a href="index.php" title="HOME">
                            HOMEPAGE
                        </a>
                    </li>
					<li>
                        <a href="#" title="SUBCONTRACTOR">
                            SUBCONTRACTOR
                            <i class="glyph-icon icon-angle-down"></i>
                        </a>
                        <ul class="footer-nav">
                            <li>
                                <a href="<?=Url::to(['site/subcontractor-new']);?>" title="Hero with pattern backgrounds">
                                    <span>PROJECT LIST</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['site/tender-list']);?>" title="Hero with gradient backgrounds">
                                    <span>MY TENDER</span>
                                </a>
                            </li>
							<li>
                                <a href="<?=Url::to(['site/project-fav']);?>" title="PROJECT BOOKMARK BY YOU">
                                    <span>PROJECT BOOKMARK</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" title="VENDOR">
                            VENDOR
                            <i class="glyph-icon icon-angle-down"></i>
                        </a>
                        <ul class="footer-nav">
                            <li>
								<a href="<?=Url::to(['site/vendor-new']);?>" title="CREATE NEW PROJECT">
                                    <span>NEW PROJECT</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['site/project-list']);?>" title="LIST OF PROJECT">
                                    <span>PROJECT LIST</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['site/vendor-fav']);?>" title="VENDOR BOOKMARK BY YOU">
                                    <span>VENDOR BOOKMARK</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?=Url::to(['site/contact-us']);?>" title="CONTACT US">
                            CONTACT US
                        </a>
                    </li>
                    <li>
                        <a href="<?=Url::to(['site/faq']);?>" title="FAQ">
                            FAQ
                        </a>
                    </li>
                </ul>
                <!-- .header-nav -->
            </div>
            <!-- .container -->
        </div>
        <!-- .main-header -->
	
		<div id="page-wrapper">
			<?= Breadcrumbs::widget([
				'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
			]) ?>
			<?= Alert::widget() ?>
			<?= $content ?>
		</div>

<div class="main-footer clearfix bg-black">
	<div class="container clearfix">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
			<div class="header">About us</div>
			<p class="about-us">
				Subsing is a company that provide a lot of Tender to Customer, a open provider that give you open solution.
			</p>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
			<h3 class="header">MENU</h3>
			<div class="posts-list">
				<ul>
					<li>
						<a href="<?=Url::to(['site/tender-list']);?>" title="Portfolio">TENDER</a>
					</li>
					<li>
						<a href="<?=Url::to(['site/subcontractor']);?>" title="Portfolio">SUB CONTRACTOR</a>
					</li>
					<li>
						<a href="<?=Url::to(['site/faq']);?>" title="Portfolio">FAQ</a>
					</li>
				</ul>
			</div>
		</div>

		<div class="col-md-3">
			<h3 class="header">Contact us</h3>
			<ul class="footer-contact">
				<li>
					<i class="glyph-icon icon-home"></i>
					5804 Quaking Embers Trail, Tiger, Missouri
				</li>
				<li>
					<i class="glyph-icon icon-phone"></i>
					(636) 517-1243
				</li>
				<li>
					<i class="glyph-icon icon-envelope-o"></i>
					<a href="#" title="">homepage@singsub.com</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="footer-pane">
		<div class="container clearfix">
			<div class="logo">&copy; 2018 SingSub. All Rights Reserved.</div>
		</div>
	</div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
