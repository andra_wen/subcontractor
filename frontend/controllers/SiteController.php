<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\LoginForm;
use common\models\SysCountry;
use common\models\SysProvince;
use common\models\SysCountyTown;
use common\models\SysDistricts;
use common\models\CustUsers;
use common\models\CustUsersSearch;
use common\models\CustUsersProfile;
use common\models\CustUsersProfileSearch;
use common\models\CustUsersCulture;
use common\models\CustUsersCultureSearch;
use common\models\CustUsersAddress;
use common\models\CustUsersAddressSearch;
use common\models\CustUsersProduct;
use common\models\CustUsersProductSearch;

use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\ChangePassword;
use yii\helpers\Json;
use yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
USE yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$login = new LoginForm();
        return $this->render('index', ['model'=>$login]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
		if (!Yii::$app->user->isGuest)
        {
            return $this->goHome();
        }
		
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }else{
			return $this->redirect(['site/index']);
		}
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionFaq()
    {
        return $this->render('faq',
			[
				
			]
		);
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionContactUs()
    {
        return $this->render('contact_us',
			[
				
			]
		);
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionUserProfile($id)
    {
		$cUserModel = CustUsers::findOne($id);
		$cUserProfileModel = $this->findUserprofile($id);		
		$cUserAddressModel = $this->findUseraddress($id);
		$cUserCultureModel = $this->findUserculture($id);
		$cUserProductModel = $this->findUserproduct($id);
		$checked = \common\models\CustUsersProduct::find()->select(['product_id'])->where(['cust_user_id' => $id])->asArray()->all();
		
        return $this->render('user_profile',
			[
				'modelcUser' => $cUserModel,
				'modelcUserProfile' => $cUserProfileModel,
				'modelcUserAddress' => $cUserAddressModel,
				'modelcUserCulture' => $cUserCultureModel,
				'modelcUserProduct' => $cUserProductModel,
				'checkedList' => $checked
			]
		);
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionEditProfile($id)
    {
		$cUserModel = CustUsers::findOne($id);
		$cUserProfileModel = $this->findUserprofile($id);		
		$cUserAddressModel = $this->findUseraddress($id);
		$cUserCultureModel = $this->findUserculture($id);
		$cUserProductModel = $this->findUserproduct($id);
		$checked = \common\models\CustUsersProduct::find()->select(['product_id'])->where(['cust_user_id' => $id])->asArray()->all();
		
		$postData = Yii::$app->request->post();
		$cUserProfileModel->user_picture = $cUserProfileModel->user_picture=='' ? $cUserProfileModel->oldAttributes['user_picture'] : $cUserProfileModel->user_picture;
		if($cUserModel->load($postData) && $cUserProfileModel->load($postData) && $cUserAddressModel->load($postData) && $cUserProductModel->load($postData) && Model::validateMultiple([$cUserModel, $cUserProfileModel, $cUserAddressModel])) {
			
			if( $cUserProfileModel->validate()){
				$image = UploadedFile::getInstance($cUserProfileModel, 'user_picture');
				if (!is_null($image)) {
					$filenames = $image->name;
					$tmp = explode(".", $filenames);
					$ext = end($tmp);
					// generate a unique file name to prevent duplicate filenames
					$cUserProfileModel->user_picture = Yii::$app->security->generateRandomString().".{$ext}";
					// the path to save file, you can set an uploadPath
					// in Yii::$app->params (as used in example below)
					Yii::$app->params['uploadPath'] = Yii::$app->urlBackendImages->baseUrl. '/';
					$path = Yii::$app->params['uploadPath'] . $cUserProfileModel->user_picture;
					// $cUserProfileModel->user_picture = $filenames;
					$image->saveAs($path);
				}
			}
			
			
			foreach($cUserProductModel->product_id as $data){
				$usersProduct = CustUsersProduct::find()->where(['cust_user_id'=>$id,'product_id'=>$data])->one();
				if($usersProduct === null){
					$products = new CustUsersProduct();
					$products->cust_user_id = $id;
					$products->product_id = $data;
					$products->createdby = $id;
					$products->lastmodifby = $id;
					$products->save();
				}
			}
			
			if( $cUserModel->save() && $cUserProfileModel->save() && $cUserAddressModel->save())
			{
				return $this->redirect(['user-profile','id'=>$id]);
			}
			
        } else {
			return $this->render('edit_profile',
				[
					'modelcUser' => $cUserModel,
					'modelcUserProfile' => $cUserProfileModel,
					'modelcUserAddress' => $cUserAddressModel,
					'modelcUserCulture' => $cUserCultureModel,
					'modelcUserProduct' => $cUserProductModel,
					'checkedList' => $checked
				]
			);
		}
    }
	
	/**
     * Finds the CustUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUserprofile($id)
    {
        if (($model = CustUsersProfile::find()->where(['cust_user_id' => $id])->one()) !== null) {
            return $model;
        } else {
            return new CustUsersProfile();
        }
    }
	
	/**
     * Finds the CustUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUseraddress($id)
    {
        if (($model = CustUsersAddress::find()->where(['cust_user_id' => $id])->one()) !== null) {
            return $model;
        } else {
            return new CustUsersAddress();
        }
    }
	
	/**
     * Finds the CustUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUserculture($id)
    {
        if (($model = CustUsersCulture::find()->where(['cust_user_id' => $id])->one()) !== null) {
            return $model;
        } else {
            return new CustUsersCulture();
        }
    }
	
	/**
     * Finds the CustUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUserproduct($id)
    {
        if (($model = CustUsersProduct::find()->where(['cust_user_id' => $id])->one()) !== null) {
            return $model;
        } else {
            return new CustUsersProduct();
        }
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionRegister()
    {
        return $this->render('register',
			[
				
			]
		);
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionProjectDashboard()
    {
        return $this->render('project_dashboard');
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionProjectInvitation()
    {
        return $this->render('project_invitation');
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionTenderSubmission()
    {
        return $this->render('tender_submission');
    }
	
    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionSubcontractorDetails()
    {
        return $this->render('subcontractor_details');
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionSubcontractorNew()
    {
        return $this->render('subcontractor_new');
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionVendorNew()
    {
        return $this->render('vendor_new');
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionSubcontractor()
    {
        return $this->render('subcontractor');
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionTenderDetails()
    {
        return $this->render('tender_details');
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionTenderList()
    {
        return $this->render('tender_list');
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionProjectFav()
    {
        return $this->render('project_fav');
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionVendorFav()
    {
        return $this->render('vendor_fav');
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionProjectList()
    {
        return $this->render('project_list');
    }
	
	/**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionTenderNew()
    {
        return $this->render('tender_new');
    }
	
	public function actionProvince() {
		$out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $country_id = $parents[0];
                $out = SysProvince::getProvinceByCountry($country_id);

                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        } echo Json::encode(['output' => '', 'selected' => '']);
	}
	
	public function actionCity() {
		$out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $country_id = empty($ids[0]) ? null : $ids[0];
            $province_id = empty($ids[1]) ? null : $ids[1];
            if ($country_id !== null) {
                $data = \common\models\SysCountytown::getCountytownByCountryProvince($country_id, $province_id);

                echo Json::encode(['output' => $data['output'], 'selected' => $data['selected']]);
                return;
            }
        } echo Json::encode(['output' => '', 'selected' => '']);
	}
	
	public function actionState() {
		$out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $country_id = empty($ids[0]) ? null : $ids[0];
            $province_id = empty($ids[1]) ? null : $ids[1];
            $countytown_id = empty($ids[2]) ? null : $ids[2];
            if ($country_id !== null) {
                $data = \common\models\SysDistricts::getDistrictsByCountryProvinceCountytown($country_id, $province_id, $countytown_id);

                echo Json::encode(['output' => $data['output'], 'selected' => $data['selected']]);
                return;
            }
        } echo Json::encode(['output' => '', 'selected' => '']);
	}

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
		
		 if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()))
		  {
		   Yii::$app->response->format = 'json';
		   return ActiveForm::validate($model);
		  }
		
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
				// return var_dump($user);
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
	
	    /**
     * Reset password
     * @return string
     */
    public function actionChangePassword()
    {
        $model = new ChangePassword();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->change()) {
            return $this->goHome();
        }
        return $this->render('change_password', [
                'model' => $model,
        ]);
    }

	
}
