<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
	'defaultRoute' => 'site/index',
    'controllerNamespace' => 'frontend\controllers',
	'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            // add wildcard allowed action here!
            '*',
            //'admin/*',
        ],
    ],
	'modules' => [
        /*'mimin' => [
            'class' => '\hscstudio\mimin\Module',
        ],
        */
        'admin' => [
            'class' => 'mdm\admin\Module',
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ],
        'translatemanager' => [
            'class' => 'lajax\translatemanager\Module',
            'allowedIPs' => ['*'],
            'layout' => '@backend/views/layouts/main',
        ],
        /*'redactor' => [
            'class' => 'yii\redactor\RedactorModule',
            'uploadDir' => '@webroot/path/to/uploadfolder',
            'uploadUrl' => '@web/path/to/uploadfolder',
            'imageAllowExtensions'=>['jpg','png','gif']
        ]*/
        'redactor' => 'yii\redactor\RedactorModule',
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\CustUsers',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
			// 'loginUrl' => ['site/index'],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
		'authManager' => [
            'class' => 'yii\rbac\DbManager', // only support DbManager //or using 'yii\rbac\PhpManager'
            // 'defaultRoles'=>['guest']
        ],
		'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'db' => 'db',
                    'sourceLanguage' => 'en_US', // Developer language
                    'sourceMessageTable' => '{{%language_source}}',
                    'messageTable' => '{{%language_translate}}',
                    /*'cachingDuration' => 86400,
                    'enableCaching' => true,*/
                    'forceTranslation'=>true,
                ],
            ],
        ],
        'languagepicker' => [
            'class' => 'lajax\languagepicker\Component',
            'languages' => function () {                        // List of available languages (icons and text)
                return \lajax\translatemanager\models\Language::getLanguageNames(true);
            },
            //'cookieName' => 'language',                         // Name of the cookie.
            //'cookieDomain' => 'example.com',                    // Domain of the cookie.
            //'expireDays' => 64,                                 // The expiration time of the cookie is 64 days.
            /*'callback' => function() {
                if (!\Yii::$app->user->isGuest) {
                    $user = \Yii::$app->user->identity;
                    $user->language = \Yii::$app->language;
                    $user->save();
                }
            }*/
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
		'urlBackendImage'=>[
			'enablePrettyUrl' => false,
			'class' => 'yii\web\UrlManager',
			'baseUrl' => '/subcontractor/backend/web/uploads/userpictures/',
		],
		'urlBackendImages'=>[
			'enablePrettyUrl' => false,
			'class' => 'yii\web\UrlManager',
			'baseUrl' => '@backend/web/uploads/userpictures/',
		],
    ],
    'params' => $params,
];
