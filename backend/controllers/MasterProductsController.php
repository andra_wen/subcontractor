<?php

namespace backend\controllers;

use Yii;
use common\models\MasterProducts;
use common\models\MasterProductsSearch;
use common\models\MasterProductProfiles;
use common\models\MasterProductProfilesSearch;
use common\models\MasterProductPictures;
use common\models\MasterProductPicturesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * MasterProductsController implements the CRUD actions for MasterProducts model.
 */
class MasterProductsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterProducts models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new MasterProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single MasterProducts model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
            $request = Yii::$app->request;
            if($request->isAjax){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'title'=> "MasterProducts #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                        'model3'=> MasterProductPictures::find()->where(['product_id'=>$id])->all(),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                return $this->render('view', [
                    'model' => $this->findModel($id),
                    'model3' => MasterProductPictures::find()->where(['product_id'=>$id])->all(),
                ]);
            }

    }

    public function actionUpload($id)
    {
        $model = new MasterProducts();

        $file = UploadedFile::getInstances($model, 'getfile');

        if ($file !== false) {
            $sortno = 0;
            foreach ($file as $files) {
                $upload = new MasterProductPictures();

                $upload->product_id = $id;
                $upload->sort_no = $sortno++;
                $upload->featured = $sortno==1?10:0;
                $upload->picture_name = $files->name;
                $ext = end((explode(".", $files->name)));
                // generate a unique file name to prevent duplicate filenames
                $upload->picture_path = Yii::$app->security->generateRandomString().".{$ext}";
                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/productpictures/';
                $path = Yii::$app->params['uploadPath'] . $upload->picture_path;
                $files->saveAs($path);

                $upload->picture_path_thumbnail = $upload->picture_path;
                $path2 = Yii::$app->basePath . '/web/uploads/productpictures_thumbnail/' . $upload->picture_path;
                Image::thumbnail($path, 300, 300)->save($path2,['quality' => 100]);

                $upload->save(false);

            }
        }
        else {
            $upload = new MasterProductPictures();
            var_dump ($upload->getErrors()); die();
        }

        $this->redirect(['index']);
    }

    public function actionDeletepicture($id)
    {
        //$file_key = (int)\Yii::$app->request->post('id');
        $model3 = MasterProductPictures::findOne($id);

        //$model2->delete();
        $path1 = Yii::$app->basePath . '/web/uploads/productpictures/'.$model3->picture_path;
        $path2 = Yii::$app->basePath . '/web/uploads/productpictures_thumbnail/' . $model3->picture_path;

        if($model3->delete())
        {
            //for deleting picture from folder
            unlink($path1);
            unlink($path2);

            //for result
            return true;
        }
        else
        {
            return false;
        }

        //return false;
    }

    /**
     * Creates a new MasterProducts model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new MasterProducts();
        $model2 = new MasterProductProfiles();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new MasterProducts",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model, 'model2' => $model2,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post())){
                $model->status = $model->status==1?10:0;
                $model->custome = $model->custome==1?10:0;
                $model2->status = $model->status==1?10:0;

                if($model->validate())
                {
                    $model2->product_id = 1;
                    if($model2->load($request->post()))
                    {
                        if($model2->validate() && $model->save() && ($model2->product_id = $model->id) && $model2->save())
                        {
                            return [
                                'forceReload'=>'#crud-datatable-pjax',
                                'title'=> "Create new MasterProducts",
                                'content'=>'<span class="text-success">Create MasterProducts success</span>',
                                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                            ];
                        }
                        else
                        {
                            return [
                                'title'=> "Create new MasterProducts",
                                'content'=>$this->renderAjax('create', [
                                    'model' => $model, 'model2' => $model2,
                                ]),
                                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                            ];
                        }
                    }
                    else
                    {
                        return [
                            'title'=> "Create new MasterProducts",
                            'content'=>$this->renderAjax('create', [
                                'model' => $model, 'model2' => $model2,
                            ]),
                            'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                        ];
                    }

                }
                else
                {
                    return [
                        'title'=> "Create new MasterProducts",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model, 'model2' => $model2,
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                    ];
                }
            }else{           
                return [
                    'title'=> "Create new MasterProducts",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model, 'model2' => $model2,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model, 'model2' => $model2,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing MasterProducts model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if(MasterProductProfiles::find()->where(['product_id' => $id])->one())
        {
            $model2 = MasterProductProfiles::find()->where(['product_id' => $id])->one();
        }
        else
        {
            $model2 = new MasterProductProfiles();
            $model2->product_id = $id;
            if($model2->validate() && $model2->save())
            {
                $model2 = MasterProductProfiles::find()->where(['product_id' => $id])->one();
            }
        }

        $model->status = $model->status==10?1:0;
        $model->custome = $model->custome==10?1:0;
        $model2->status = $model2->status==10?1:0;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update MasterProducts #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model, 'model2' => $model2,
                        'model3' => MasterProductPictures::find()->where(['product_id'=>$id])->all(),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post())){
                $model->status = $model->status==1?10:0;
                $model->custome = $model->custome==1?10:0;

                if($model->validate())
                {
                    $model2->product_id = $id;
                    if($model2->load($request->post()))
                    {
                        $model2->status = $model2->status==1?10:0;
                        if($model2->validate() && $model->save() && ($model2->product_id = $model->id) && $model2->save())
                        {
                            return [
                                'forceReload'=>'#crud-datatable-pjax',
                                'title'=> "MasterProducts #".$id,
                                'content'=>$this->renderAjax('view', [
                                    'model' => $model, 'model2' => $model2,
                                    'model3' => MasterProductPictures::find()->where(['product_id'=>$id])->all(),
                                ]),
                                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                            ];
                        }
                        else
                        {
                            return [
                                'title'=> "Update MasterProducts #".$id,
                                'content'=>$this->renderAjax('update', [
                                    'model' => $model, 'model2' => $model2,
                                    'model3' => MasterProductPictures::find()->where(['product_id'=>$id])->all(),
                                ]),
                                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                            ];
                        }
                    }
                    else
                    {
                        return [
                            'title'=> "Update MasterProducts #".$id,
                            'content'=>$this->renderAjax('update', [
                                'model' => $model, 'model2' => $model2,
                                'model3' => MasterProductPictures::find()->where(['product_id'=>$id])->all(),
                            ]),
                            'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                        ];
                    }
                }
                else
                {
                    return [
                        'title'=> "Update MasterProducts #".$id,
                        'content'=>$this->renderAjax('update', [
                            'model' => $model, 'model2' => $model2,
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                    ];
                }
            }else{
                 return [
                    'title'=> "Update MasterProducts #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model, 'model2' => $model2,
                        'model3' => MasterProductPictures::find()->where(['product_id'=>$id])->all(),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model, 'model2' => $model2,
                    'model3' => MasterProductPictures::find()->where(['product_id'=>$id])->all(),
                ]);
            }
        }
    }

    /**
     * Delete an existing MasterProducts model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing MasterProducts model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the MasterProducts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterProducts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterProducts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
