<?php

namespace backend\controllers;

use Yii;
use common\models\MasterNews;
use common\models\MasterNewsSearch;
use common\models\MasterNewsPictures;
use common\models\MasterNewsPicturesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * MasterNewsController implements the CRUD actions for MasterNews model.
 */
class MasterNewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterNews models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new MasterNewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single MasterNews model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "MasterNews #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                        'model3'=> MasterNewsPictures::find()->where(['news_id'=>$id])->all(),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
                'model3' => MasterNewsPictures::find()->where(['news_id'=>$id])->all(),
            ]);
        }
    }

    public function actionUpload($id)
    {
        $model = new MasterNews();

        $file = UploadedFile::getInstances($model, 'getfile');

        if ($file !== false) {
            $sortno = 0;
            foreach ($file as $files) {
                $upload = new MasterNewsPictures();

                $upload->news_id = $id;
                $upload->sort_no = $sortno++;
                $upload->status = $sortno=1;
                $upload->picture_name = $files->name;
                $ext = end((explode(".", $files->name)));
                // generate a unique file name to prevent duplicate filenames
                $upload->picture_path = Yii::$app->security->generateRandomString().".{$ext}";
                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/news/';
                $path = Yii::$app->params['uploadPath'] . $upload->picture_path;
                $files->saveAs($path);

                $upload->save(false);

            }
        }
        else {
            $upload = new MasterNewsPictures();
            var_dump ($upload->getErrors()); die();
        }

        $this->redirect(['index']);
    }

    public function actionDeletepicture($id)
    {
        //$file_key = (int)\Yii::$app->request->post('id');
        $model3 = MasterNewsPictures::findOne($id);

        //$model2->delete();
        $path1 = Yii::$app->basePath . '/web/uploads/news/'.$model3->picture_path;

        if($model3->delete())
        {
            //for deleting picture from folder
            unlink($path1);

            //for result
            return true;
        }
        else
        {
            return false;
        }

        //return false;
    }

    /**
     * Creates a new MasterNews model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new MasterNews();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new MasterNews",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post())){
                $model->status = $model->status==1?10:0;
                $model->isactive = $model->isactive==1?10:0;

                $image = UploadedFile::getInstance($model, 'news_pict');
                if (!is_null($image)) {
                    $model->news_pict = $image->name;
                    $ext = end((explode(".", $image->name)));
                    // generate a unique file name to prevent duplicate filenames
                    $model->news_pict = Yii::$app->security->generateRandomString().".{$ext}";
                    // the path to save file, you can set an uploadPath
                    // in Yii::$app->params (as used in example below)
                    Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/news/';
                    $path = Yii::$app->params['uploadPath'] . $model->news_pict;
                    $image->saveAs($path);
                }

                if($model->validate() && $model->save())
                {
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Create new MasterNews",
                        'content'=>'<span class="text-success">Create MasterNews success</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                    ];
                }
                else
                {
                    return [
                        'title'=> "Create new MasterNews",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                    ];
                }
            }else{           
                return [
                    'title'=> "Create new MasterNews",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing MasterNews model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->status = $model->status==10?1:0;
        $model->isactive = $model->isactive==10?1:0;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update MasterNews #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post())){
                $model->status = $model->status==1?10:0;
                $model->isactive = $model->isactive==1?10:0;

                if($model->validate())
                {
                    $model->news_pict = $model->news_pict=='' ? $model->oldAttributes['news_pict'] : $model->news_pict;
                    $image = UploadedFile::getInstance($model, 'news_pict');
                    if (!is_null($image)) {
                        $model->news_pict = $image->name;
                        $ext = end((explode(".", $image->name)));
                        // generate a unique file name to prevent duplicate filenames
                        $model->news_pict = Yii::$app->security->generateRandomString().".{$ext}";
                        // the path to save file, you can set an uploadPath
                        // in Yii::$app->params (as used in example below)
                        Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/news/';
                        $path = Yii::$app->params['uploadPath'] . $model->news_pict;
                        $image->saveAs($path);
                    }
                    if($model->save())
                    {
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "MasterNews #".$id,
                            'content'=>$this->renderAjax('view', [
                                'model' => $model,
                            ]),
                            'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                        ];
                    }
                    else
                    {
                        return [
                            'title'=> "Update MasterNews #".$id,
                            'content'=>$this->renderAjax('update', [
                                'model' => $model,
                            ]),
                            'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                        ];
                    }
                }
                else
                {
                    return [
                        'title'=> "Update MasterNews #".$id,
                        'content'=>$this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                    ];
                }
            }else{
                 return [
                    'title'=> "Update MasterNews #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing MasterNews model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing MasterNews model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the MasterNews model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterNews the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterNews::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
