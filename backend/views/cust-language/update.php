<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CustLanguage */
?>
<div class="cust-language-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
