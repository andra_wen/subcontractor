<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CustLanguage */
?>
<div class="cust-language-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer_id',
            'language_id',
            'createdby',
            'createdon',
            'lastmodifby',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
