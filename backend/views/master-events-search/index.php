<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterEventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-events-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Master Events', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'customer_id',
            'page_id',
            'event_name',
            'event_title',
            // 'event_content',
            // 'event_pict',
            // 'event_alt',
            // 'isactive',
            // 'sort_no',
            // 'event_date',
            // 'effective_from',
            // 'effective_till',
            // 'createdby',
            // 'createdon',
            // 'lastmodifby',
            // 'lastmodif',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
