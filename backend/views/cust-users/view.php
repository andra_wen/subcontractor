<?php

use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\web\JsExpression;
use kartik\widgets\DepDrop;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\CustUsers */

$js = '

jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {

    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {

        jQuery(this).html("Address: " + (index + 1))

    });

});


jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {

    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {

        jQuery(this).html("Address: " + (index + 1))

    });

});

';


$this->registerJs($js);

?>

<div class="cust-users-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer.customer_name',
            'username',
            'auth_key',
            'password_hash',
            'password_reset_token',
            'email:email',
            'effective_from',
            'effective_till',
            'createdon',
            [
                'label' => 'Status',
                'format' => 'html',
                'attribute' => function($model)
                {
                    if($model->status == 10)
                    {
                        return '<span class="label label-primary">'.'Active'.'</span>';
                    }
                    else
                    {
                        return "<span class='label label-danger'>"."Inactive"."</span>";
                    }
                }
            ],
			[
                'label' => 'Roles',
                'format' => 'html',
                'attribute' => function($model)
                {
                    if($model->roles_name == 10)
                    {
                        return '<span class="label label-primary">'.'Backend'.'</span>';
                    }
                    elseif($model->roles_name == 20)
                    {
                        return "<span class='label label-danger'>"."Subcontractor"."</span>";
                    }
					else
                    {
                        return "<span class='label label-danger'>"."Vendor"."</span>";
                    }
                }
            ]
        ],
    ]) ?>

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <div class="panel panel-default">
        <div class="panel-body">

            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 0, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelAddress[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'address_name',
                    'address_line1',
                    'address_line2',
                    'address_phonenumber',
                    'address_country_id',
                    'address_province_id',
                    'address_countytown_id',
                    'address_districts_id',
                    'address_postalzip',
                    'address_default',
                    'address_remarks',
                ],
            ]); ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>
                        <i class="glyphicon glyphicon-envelope"></i> Addresses
                        <!--<button type="button" class="add-item btn btn-success btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> Add</button>-->
                    </h4>
                </div>
                <div class="panel-body">
                    <div class="container-items"><!-- widgetBody -->
                        <?php foreach ($modelAddress as $i => $modelsAddress): ?>
                            <div class="item panel panel-default"><!-- widgetItem -->
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left">Address</h3>
                                    <div class="pull-right">
                                        <!--<button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>-->
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                    // necessary for update action.
                                    if (!$modelsAddress->isNewRecord) {
                                        echo Html::activeHiddenInput($modelsAddress, "[{$i}]id");
                                    }
                                    ?>
                                    <?= $form->field($modelsAddress, "[{$i}]address_name")->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <?= $form->field($modelsAddress, "[{$i}]address_line1")->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                        </div>
                                        <div class="col-sm-12">
                                            <?= $form->field($modelsAddress, "[{$i}]address_line2")->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                        </div>
                                        <div class="col-sm-12">
                                            <?= $form->field($modelsAddress, "[{$i}]address_phonenumber")->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                        </div>
                                    </div><!-- .row -->
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php
                                            $dataCountry = \common\models\SysCountry::find()->orderBy('short_name')->all();
                                            $listDataCountry = \yii\helpers\ArrayHelper::map($dataCountry, 'id', 'short_name');
                                            ?>
                                            <?= $form->field($modelsAddress, "[{$i}]address_country_id")->dropDownList($listDataCountry,
                                            [
                                                'prompt' => Yii::t('language', 'Select Country...'),
                                                'id' => "custusersaddress-$i-address_country_id",
                                                'disabled'=>"disabled",
                                            ]); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php
                                            $dataProvince = \common\models\SysProvince::find()->orderBy('name')->all();
                                            $listDataProvince = \yii\helpers\ArrayHelper::map($dataProvince, 'id', 'name');
                                            ?>
                                            <?= $form->field($modelsAddress, "[{$i}]address_province_id")->widget(DepDrop::classname(), [
                                            'options' => ['id' => "custusersaddress-$i-address_province_id"],
                                            'data'=>$listDataProvince,
                                            'pluginOptions' => [
                                                'depends' => ["custusersaddress-$i-address_country_id"],
                                                'placeholder' => Yii::t('language', 'Select Province...'),
                                                'url' => Url::to(['/sys-province/provincelist'])
                                            ],
                                            'disabled'=>"disabled",
                                        ]); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php
                                            $dataCountyTown = \common\models\SysCountytown::find()->orderBy('name')->all();
                                            $listDataCountyTown = \yii\helpers\ArrayHelper::map($dataCountyTown, 'id', 'name');
                                            ?>

                                            <?= $form->field($modelsAddress, "[{$i}]address_countytown_id")->widget(DepDrop::classname(), [
                                            'options' => ['id' => "custusersaddress-$i-address_countytown_id"],
                                            'data'=>$listDataCountyTown,
                                            'pluginOptions' => [
                                                'depends' => ["custusersaddress-$i-address_country_id", "custusersaddress-$i-address_province_id"],
                                                'placeholder' => Yii::t('language', 'Select Countytown..'),
                                                'url' => Url::to(['/sys-countytown/countylist'])
                                            ],
                                            'disabled'=>"disabled",
                                        ]); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php
                                            $dataDistricts = \common\models\SysDistricts::find()->orderBy('name')->all();
                                            $listDataDistricts = \yii\helpers\ArrayHelper::map($dataDistricts, 'id', 'name');
                                            ?>
                                            <?= $form->field($modelsAddress, "[{$i}]address_districts_id")->widget(DepDrop::classname(), [
                                            'options' => ['id' => "custusersaddress-$i-address_districts_id"],
                                            'data'=>$listDataDistricts,
                                            'pluginOptions' => [
                                                'depends' => ["custusersaddress-$i-address_country_id", "custusersaddress-$i-address_province_id", "custusersaddress-$i-address_countytown_id"],
                                                'placeholder' => Yii::t('language', 'Select Districts..'),
                                                'url' => Url::to(['/sys-districts/districtslist'])
                                            ],
                                            'disabled'=>"disabled",
                                        ]);
                                            ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $form->field($modelsAddress, "[{$i}]address_postalzip")->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                        </div>
                                        <div>
                                            <?= $form->field($modelsAddress, "[{$i}]address_default")->widget(SwitchInput::classname(),
                                            [
                                                'pluginOptions'=>[
                                                    'onText'=>'Yes',
                                                    'offText'=>'No',
                                                ],
                                            ]);
                                            ?>
                                        </div>
                                    </div><!-- .row -->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <?= $form->field($modelsAddress, "[{$i}]address_remarks")->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                        </div>
                                    </div><!-- .row -->
                                </div>
                            </div>
                            <?php endforeach; ?>
                    </div>
                </div>
            </div><!-- .panel -->

            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
