<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CustUsers */

$this->title = Yii::t('language', 'Update Profile : ', ['modelClass' => 'Cust Users']) . $model->username;
$this->params['breadcrumbs'][] = Yii::t('language', 'Update Profile');

?>
<div class="cust-users-edit">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_formedit',
    [
        'model' => $model,'model2'=>$model2, 'model3'=>$model3, 'modelAddress' => $modelAddress
    ]) ?>

</div>
