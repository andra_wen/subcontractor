<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SysCountytown */

?>
<div class="sys-countytown-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
