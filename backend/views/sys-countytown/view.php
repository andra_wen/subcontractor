<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SysCountytown */
?>
<div class="sys-countytown-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'country_id',
            'province_id',
            'name',
            'iso_code',
            'createdby',
            'createdon',
            'lastmodifby',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
