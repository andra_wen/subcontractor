<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterCustomer */

?>
<div class="master-customer-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
