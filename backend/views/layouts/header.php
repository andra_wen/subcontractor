<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$formatter = Yii::$app->formatter;
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <li class="navbar-text">
                    <?= \lajax\languagepicker\widgets\LanguagePicker::widget([
                    'skin' => \lajax\languagepicker\widgets\LanguagePicker::SKIN_DROPDOWN,
                    'size' => \lajax\languagepicker\widgets\LanguagePicker::SIZE_LARGE
                ]); ?>
                </li>

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php
                        $hasil = \common\models\CustUsersProfile::getCustprofilebyuserid(Yii::$app->user->identity->id);
                        echo Html::img('@web/uploads/userpictures/'.$hasil->user_picture,['class'=>'img-circle','alt'=>"User Image",'width'=>17,'height'=>17]);
                        ?>
                        <span class="hidden-xs"><?php echo Yii::$app->user->identity->username;?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <?php
                                echo Html::img('@web/uploads/userpictures/'.$hasil->user_picture,['class'=>'img-circle','alt'=>"User Image",'width'=>17,'height'=>17]);
                            ?>
                            <p>
                                <?php echo Yii::$app->user->identity->username;?>
                                <small>Member since <?php echo $formatter->asDate(Yii::$app->user->identity->createdon, 'long');?></small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <?=
                                Html::a(
                                    'Profile',
                                    \yii\helpers\Url::toRoute(['/cust-users/edit','id'=>1]),
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                )
                                ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
