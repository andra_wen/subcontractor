<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\MasterProductPictures */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-product-pictures-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'picture_name')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'picture_path')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'picture_path_md')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'picture_path_sm')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'picture_path_xs')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'picture_path_thumbnail')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'picture_alt')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'sort_no')->hiddenInput()->label(false) ?>

    <center>
        <div class="panel panel-default">
            <div class="panel-body">
                <?php echo Html::img(Yii::$app->request->baseUrl.'/uploads/productpictures/'.$model->picture_path, ['width'=>200, 'height'=>200]); ?>
            </div>
            <div class="panel-footer">
                <?php echo $model->picture_name; ?>
            </div>
        </div>

        <?= $form->field($model, 'featured')->widget(SwitchInput::classname(),
        [
            'pluginOptions'=>[
                'onText'=>'Yes',
                'offText'=>'No',
            ],
        ]);
        ?>

    </center>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
