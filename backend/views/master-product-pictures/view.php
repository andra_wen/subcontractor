<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterProductPictures */
?>
<div class="master-product-pictures-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'product.product_name',
            'picture_name',
            [
                'attribute'=>'picture_path',
                'value'=>"@web/uploads/productpictures/".$model->picture_path,
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
            'picture_path_md',
            'picture_path_sm',
            'picture_path_xs',
            [
                'attribute'=>'picture_path_thumbnail',
                'value'=>"@web/uploads/productpictures_thumbnail/".$model->picture_path_thumbnail,
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
            'picture_alt',
            'sort_no',
            [
                'label'=>'featured',
                'format'=>'html',
                'attribute'=>function($model)
                {
                    if($model->featured == 10)
                    {
                        return '<p style="color:green;"><i class="fa fa-check"></i> <i>'.Yii::t('language','Yes').'</i></p>';
                    }
                    else
                    {
                        return '<p style="color:red;"><i class="fa fa-close"></i> <i>'.Yii::t('language','No').'</i></p>';
                    }
                }
            ],
            'createdby0.username',
            'createdon',
            'lastmodifby0.username',
            'lastmodif',
            [
                'label'=>'status',
                'format'=>'html',
                'attribute'=>function($model)
                {
                    if($model->status == 10)
                    {
                        return '<span class="label label-primary">'.'Active'.'</span>';
                    }
                    else
                    {
                        return "<span class='label label-danger'>"."Inactive"."</span>";
                    }
                }
            ],
        ],
    ]) ?>

</div>
