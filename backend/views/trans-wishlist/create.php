<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TransWishlist */

?>
<div class="trans-wishlist-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
