<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CustAddress */

?>
<div class="cust-address-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
