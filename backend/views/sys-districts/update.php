<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SysDistricts */
?>
<div class="sys-districts-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
