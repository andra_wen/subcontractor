<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterProductVarians */
?>
<div class="master-product-varians-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
