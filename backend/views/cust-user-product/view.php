<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CustUserProduct */
?>
<div class="cust-user-product-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cust_user_id',
            'product_id',
            'createdon',
            'lastmodifon',
            'createdby',
            'lastmodifby',
        ],
    ]) ?>

</div>
