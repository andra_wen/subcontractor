<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CustUserProduct */

?>
<div class="cust-user-product-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
