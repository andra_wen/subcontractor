<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'customer_id',
        'value'=>'customer.customer_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'paymentmethod_id',
        'value'=>'paymentmethod.payment_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cust_bank_account_id',
        'value'=>'custBankAccount.bank_accountno',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'isactive',
        'format'=>'html',
        'value'=>function($model)
        {
            return $model->isactive == 0 ? '<span class="label label-danger">Inactive</span>' : '<span class="label label-primary">Active</span>';
        },
        'filter' => [
            0 => 'Inactive',
            10 => 'Active'
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'format'=>'html',
        'value'=>function($model)
        {
            return $model->status == 0 ? '<span class="label label-danger">Inactive</span>' : '<span class="label label-primary">Active</span>';
        },
        'filter' => [
            0 => 'Inactive',
            10 => 'Active'
        ],
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'createdon',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'lastmodifby',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'lastmodif',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'status',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   