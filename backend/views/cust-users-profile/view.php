<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CustUsersProfile */
?>
<div class="cust-users-profile-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cust_user_id',
            'user_firstname',
            'user_middlename',
            'user_lastname',
            'user_picture',
            'placeofbirth',
            'dateofbirth',
            'gender',
            'marriagestatus',
            'bloodtype',
            'createdby',
            'createdon',
            'lastmodifby',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
