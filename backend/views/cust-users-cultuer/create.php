<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CustUsersCulture */

?>
<div class="cust-users-culture-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
