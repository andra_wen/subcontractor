<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CustUsersCulture */
?>
<div class="cust-users-culture-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cust_user_id',
            'themes_color',
            'language',
            'country',
            'dateFormat',
            'user_currency',
            'user_picture',
            'createdby',
            'createdon',
            'lastmodifby',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
