<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CustUsersCulture */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cust-users-culture-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cust_user_id')->textInput() ?>

    <?= $form->field($model, 'themes_color')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'language')->textInput() ?>

    <?= $form->field($model, 'country')->textInput() ?>

    <?= $form->field($model, 'dateFormat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_currency')->textInput() ?>

    <?= $form->field($model, 'user_picture')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'createdby')->textInput() ?>

    <?= $form->field($model, 'createdon')->textInput() ?>

    <?= $form->field($model, 'lastmodifby')->textInput() ?>

    <?= $form->field($model, 'lastmodif')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
