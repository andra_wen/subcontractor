<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SysProvince */

?>
<div class="sys-province-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
