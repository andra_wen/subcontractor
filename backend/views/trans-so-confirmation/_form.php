<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TransSoConfirmation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trans-so-confirmation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'so_id')->textInput() ?>

    <?= $form->field($model, 'so_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'paymentmethod_id')->textInput() ?>

    <?= $form->field($model, 'paymentmethod_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'account_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'account_holder')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_amount')->textInput() ?>

    <?= $form->field($model, 'payment_date')->textInput() ?>

    <?= $form->field($model, 'confirm_remarks')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'confirm_attachment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'createdby')->textInput() ?>

    <?= $form->field($model, 'createdon')->textInput() ?>

    <?= $form->field($model, 'lastmodifby')->textInput() ?>

    <?= $form->field($model, 'lastmodif')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
