<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TransSoConfirmation */
?>
<div class="trans-so-confirmation-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'so_id',
            'so_no',
            'paymentmethod_id',
            'paymentmethod_name',
            'account_no',
            'account_holder',
            'phone_number',
            'total_amount',
            'payment_date',
            'confirm_remarks',
            'confirm_attachment',
            'createdby',
            'createdon',
            'lastmodifby',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
