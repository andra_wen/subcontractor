<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MasterNewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-news-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'customer_id') ?>

    <?= $form->field($model, 'page_id') ?>

    <?= $form->field($model, 'news_name') ?>

    <?= $form->field($model, 'news_title') ?>

    <?php // echo $form->field($model, 'news_content') ?>

    <?php // echo $form->field($model, 'news_pict') ?>

    <?php // echo $form->field($model, 'news_alt') ?>

    <?php // echo $form->field($model, 'isactive') ?>

    <?php // echo $form->field($model, 'sort_no') ?>

    <?php // echo $form->field($model, 'effective_from') ?>

    <?php // echo $form->field($model, 'effective_till') ?>

    <?php // echo $form->field($model, 'createdby') ?>

    <?php // echo $form->field($model, 'createdon') ?>

    <?php // echo $form->field($model, 'lastmodifby') ?>

    <?php // echo $form->field($model, 'lastmodif') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
