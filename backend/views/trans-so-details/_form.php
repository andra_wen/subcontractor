<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TransSoDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trans-so-details-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'so_id')->textInput() ?>

    <?= $form->field($model, 'product_id')->textInput() ?>

    <?= $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_varians_id')->textInput() ?>

    <?= $form->field($model, 'product_varians_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_price')->textInput() ?>

    <?= $form->field($model, 'product_quantity')->textInput() ?>

    <?= $form->field($model, 'provider_id')->textInput() ?>

    <?= $form->field($model, 'provider_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shippingcost_id')->textInput() ?>

    <?= $form->field($model, 'shippingcost')->textInput() ?>

    <?= $form->field($model, 'user_address_id')->textInput() ?>

    <?= $form->field($model, 'address_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_line1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_line2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_province')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_countytown')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_districts')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_postalzip')->textInput() ?>

    <?= $form->field($model, 'cart_remarks')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'details_total')->textInput() ?>

    <?= $form->field($model, 'details_statuscode')->textInput() ?>

    <?= $form->field($model, 'createdby')->textInput() ?>

    <?= $form->field($model, 'createdon')->textInput() ?>

    <?= $form->field($model, 'lastmodifby')->textInput() ?>

    <?= $form->field($model, 'lastmodif')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
