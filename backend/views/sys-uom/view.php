<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SysUom */
?>
<div class="sys-uom-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            'uom_name',
            'uom_description',
            'createdby',
            'createdon',
            'lastmodifby',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
