<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SysCountry */
?>
<div class="sys-country-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
