<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TransCarts */
?>
<div class="trans-carts-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
