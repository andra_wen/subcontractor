<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use vova07\select2\Widget;
use yii\web\JsExpression;
use kartik\widgets\SwitchInput;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model common\models\TransShippingcost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trans-shippingcost-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $dataCustomer = ArrayHelper::map(\common\models\MasterCustomer::find()->where(['status'=>10])->all(), 'id', 'customer_name'); ?>
    <?=
    $form->field($model, 'customer_id')->widget(Widget::classname(),[
        'items'=> $dataCustomer,
        'options'=>[
            'placeholder'=> 'Select Customer...'
        ],
        'events' => [
            'select2-open' => 'function (e) { log("select2:open", e); }',
            'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
        ],
        'settings' => [
            'width' => '100%',
        ],
    ]);
    ?>

    <?php $dataShippingprovider = ArrayHelper::map(\common\models\TransShippingprovider::find()->where(['status'=>10])->all(), 'id', 'shipping_name'); ?>
    <?=
    $form->field($model, 'shippingprovider_id')->widget(Widget::classname(),[
        'items'=> $dataShippingprovider,
        'options'=>[
            'placeholder'=> 'Select Provider...'
        ],
        'events' => [
            'select2-open' => 'function (e) { log("select2:open", e); }',
            'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
        ],
        'settings' => [
            'width' => '100%',
        ],
    ]);
    ?>

    <?php
        $dataCountry = \common\models\SysCountry::find()->orderBy('short_name')->all();
        $listDataCountry = \yii\helpers\ArrayHelper::map($dataCountry, 'id', 'short_name');
    ?>
    <?= $form->field($model, 'shipping_country')->dropDownList($listDataCountry, [
        'prompt' => Yii::t('language', 'Select Country...'),
        'id' => 'country-id'
]); ?>

    <?php
        $dataProvince = \common\models\SysProvince::find()->orderBy('name')->all();
        $listDataProvince = \yii\helpers\ArrayHelper::map($dataProvince, 'id', 'name');
    ?>
    <?= $form->field($model, 'shipping_province')->widget(DepDrop::classname(), [
        'options' => ['id' => 'province-id'],
        'data'=> $listDataProvince,
        'pluginOptions' => [
            'depends' => ['country-id'],
            'placeholder' => Yii::t('language', 'Select Province...'),
            'url' => Url::to(['/sys-province/provincelist'])
        ]
]); ?>

    <?php
        $dataCountyTown = \common\models\SysCountytown::find()->orderBy('name')->all();
        $listDataCountyTown = \yii\helpers\ArrayHelper::map($dataCountyTown, 'id', 'name');
    ?>
    <?= $form->field($model, 'shipping_countytown')->widget(DepDrop::classname(), [
        'options' => ['id' => 'countytown-id'],
        'data'=>$listDataCountyTown,
        'pluginOptions' => [
            'depends' => ['country-id', 'province-id'],
            'placeholder' => Yii::t('language', 'Select Countytown..'),
            'url' => Url::to(['/sys-countytown/countylist'])
    ]
]); ?>

    <?php
        $dataDistricts = \common\models\SysDistricts::find()->orderBy('name')->all();
        $listDataDistricts = \yii\helpers\ArrayHelper::map($dataDistricts, 'id', 'name');
    ?>
    <?= $form->field($model, 'shipping_districts')->widget(DepDrop::classname(), [
        'options' => ['id' => 'districts-id'],
        'data'=>$listDataDistricts,
        'pluginOptions' => [
            'depends' => ['country-id', 'province-id', 'countytown-id'],
            'placeholder' => Yii::t('language', 'Select Districts..'),
            'url' => Url::to(['/sys-districts/districtslist'])
    ]
]); ?>

    <?= $form->field($model, 'shipping_note')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shipping_prices')->textInput() ?>

    <?= $form->field($model, 'shipping_minweight')->textInput() ?>

    <?= $form->field($model, 'shipping_minweightprices')->textInput() ?>

    <?= $form->field($model, 'shipping_minwegithnextprice')->textInput() ?>

    <?= $form->field($model, 'shipping_maxweight')->textInput() ?>

    <?= $form->field($model, 'isactive')->widget(SwitchInput::classname(),
    [
        'pluginOptions'=>[
            'onText'=>'Active',
            'offText'=>'Inactive',
        ],
    ]);
    ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
    [
        'pluginOptions'=>[
            'onText'=>'Active',
            'offText'=>'Inactive',
        ],
    ]);
    ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
