<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TransSoPictures */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trans-so-pictures-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_so_details')->textInput() ?>

    <?= $form->field($model, 'picture_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'picture_path')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
