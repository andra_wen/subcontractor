<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TransSoPictures */

$this->title = Yii::t('language', 'Update {modelClass}: ', [
    'modelClass' => 'Trans So Pictures',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('language', 'Trans So Pictures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('language', 'Update');
?>
<div class="trans-so-pictures-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
