<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CustBanks */
?>
<div class="cust-banks-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
