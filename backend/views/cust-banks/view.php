<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CustBanks */
?>
<div class="cust-banks-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer.customer_name',
            'bank_shortname',
            'bank_longname',
            [
                'attribute'=>'bank_logo',
                'value'=>"@web/uploads/banks/".$model->bank_logo,
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
            [
                'attribute'=>'bank_picture',
                'value'=>"@web/uploads/banks_thumbnail/".$model->bank_picture,
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
            'createdby0.username',
            'createdon',
            'lastmodifby0.username',
            'lastmodif',
            [
                'label'=>'status',
                'format'=>'html',
                'attribute'=>function($model)
                {
                    if($model->status == 10)
                    {
                        return '<span class="label label-primary">'.'Active'.'</span>';
                    }
                    else
                    {
                        return "<span class='label label-danger'>"."Inactive"."</span>";
                    }
                }
            ],
        ],
    ]) ?>

</div>
