<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterFiles */
?>
<div class="master-files-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
