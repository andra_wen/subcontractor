<?php
/**
 * Created by JetBrains PhpStorm.
 * Framwork: Yii2
 * User: andrawen
 * Date: 1/27/18
 * Time: 7:33 PM
 * To change this template use File | Settings | File Templates.
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\redactor\widgets\Redactor;

use kartik\widgets\Select2;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <p>You have entered the following information:</p>

    <?php
    ?>
    <?php
    $email=common\models\CustUsers::find()->select(['email'])->where(['status'=>10])->orderBy('username ASC')->all();
    $emailto=\yii\helpers\ArrayHelper::map($email,'email','email');

    $username=common\models\CustUsers::find()->select(['username'])->where(['status'=>10])->orderBy('username ASC')->all();
    $usernameto=\yii\helpers\ArrayHelper::map($username,'username','username');
    ?>

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->widget(Select2::classname(),
    [
        'data' => $usernameto,
        'options' => ['placeholder' => 'Select data ...','multiple'=>true],
        'pluginOptions' =>
        [
            'allowClear' => true
        ],
    ])->label('Email To');
    ?>

    <?= $form->field($model, 'email')->widget(Select2::classname(),
    [
        'data' => $emailto,
        'options' => ['placeholder' => 'Select data ...','multiple'=>true],
        'pluginOptions' =>
        [
            'allowClear' => true
        ],
    ])->label('CC To');

    /*echo var_dump($result);*/
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Subject') ?>

    <?= $form->field($model, 'mail_to_address')->widget(Redactor::className(), [
    'clientOptions' => [
        'lang' => 'en-US',
        'limiter'=> 5000,
        'plugins' => ['fontcolor','limiter','textdirection','properties']
    ]
])?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
