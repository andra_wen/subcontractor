<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterSliders */
?>
<div class="master-sliders-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer.customer_name',
            'page.page_name',
            'slider_name',
            'slider_title',
            [
            'attribute'=>'slider_pict',
            'value'=>"@web/uploads/sliders/".$model->slider_pict,
            'format' => ['image',['width'=>'100','height'=>'100']],
            ],
            'slider_alt',
            [
                'label'=>'isactive',
                'format'=>'html',
                'attribute'=>function($model)
                {
                    if($model->isactive == 10)
                    {
                        return '<span class="label label-primary">'.'Active'.'</span>';
                    }
                    else
                    {
                        return "<span class='label label-danger'>"."Inactive"."</span>";
                    }
                }
            ],
            'sort_no',
            'createdby0.username',
            'createdon',
            'lastmodifby0.username',
            'lastmodif',
            [
                'label'=>'status',
                'format'=>'html',
                'attribute'=>function($model)
                {
                    if($model->status == 10)
                    {
                        return '<span class="label label-primary">'.'Active'.'</span>';
                    }
                    else
                    {
                        return "<span class='label label-danger'>"."Inactive"."</span>";
                    }
                }
            ],
        ],
    ]) ?>

</div>
