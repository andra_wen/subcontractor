<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SysSeqPattern */

?>
<div class="sys-seq-pattern-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
