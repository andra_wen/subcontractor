<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SysSeqPattern */
?>
<div class="sys-seq-pattern-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer_id',
            'pattern_name',
            'pattern_desc',
            'pattern_fortable',
            'pattern_format',
            'pattern_lastsequences',
            'createdby',
            'createdon',
            'lastmodifby',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
