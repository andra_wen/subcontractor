<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterNews */

?>
<div class="master-news-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
