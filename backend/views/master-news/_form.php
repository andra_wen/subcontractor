<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use vova07\select2\Widget;
use yii\web\JsExpression;
use kartik\widgets\SwitchInput;
use kartik\widgets\DateTimePicker;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use yii\redactor\widgets\Redactor;

/* @var $this yii\web\View */
/* @var $model common\models\MasterNews */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $dataCustomer = ArrayHelper::map(\common\models\MasterCustomer::find()->where(['status'=>10])->all(), 'id', 'customer_name'); ?>
    <?=
    $form->field($model, 'customer_id')->widget(Widget::classname(),[
        'items'=> $dataCustomer,
        'options'=>[
            'placeholder'=> 'Select Customer...'
        ],
        'events' => [
            'select2-open' => 'function (e) { log("select2:open", e); }',
            'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
        ],
        'settings' => [
            'width' => '100%',
        ],
    ]);
    ?>

    <?php $dataPage = ArrayHelper::map(\common\models\MasterPages::find()->where(['status'=>10])->all(), 'id', 'page_name'); ?>
    <?=
    $form->field($model, 'page_id')->widget(Widget::classname(),[
        'items'=> $dataPage,
        'options'=>['placeholder'=> 'Select Page...'],
        'events' => [
            'select2-open' => 'function (e) { log("select2:open", e); }',
            'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
        ],
        'settings' => [
            'width' => '100%',
        ],
    ]);
    ?>

    <?= $form->field($model, 'news_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'news_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'news_content')->widget(Redactor::className(), [
    'clientOptions' => [
        'lang' => 'en-US',
        'limiter'=> 5000,
        'plugins' => ['fontcolor','limiter','textdirection','properties']
    ]
])?>


    <?= $form->field($model, 'news_pict')->widget(FileInput::classname(), [
    'options' => ['accept' => 'image/*'],
    'pluginOptions' => [
        'showCaption' => false,
        'showRemove' => false,
        'showUpload' => false,
        'browseClass' => 'btn btn-primary btn-block',
        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
        'browseLabel' =>  'Select Photo',
        'initialPreview'=>[
            Html::img("@web/uploads/news/" . $model->news_pict,['width'=>100])
        ],
        'overwriteInitial'=>true
    ],
]); ?>

    <?= $form->field($model, 'news_alt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isactive')->widget(SwitchInput::classname(),
    [
        'pluginOptions'=>[
            'onText'=>'Active',
            'offText'=>'Inactive',
        ],
    ]);
    ?>

    <?= $form->field($model, 'sort_no')->textInput() ?>

    <?= $form->field($model, 'news_date')->widget(DatePicker::classname(),
    [
        'name'=>'news_date',
        'value'=> date('yyyy-mm-dd'),
        'options'=>['placeholder'=>'Select a date...'],
        'pluginOptions'=>
        [
            'format'=>'yyyy-mm-dd',
            'todayHighlight'=>true,
            'autoclose'=>true,
        ]
    ]);
    ?>

    <?= $form->field($model, 'effective_from')->widget(DatetimePicker::classname(),
    [
        'name'=>'effective_from',
        'value'=> date('yyyy-mm-dd H:i:s'),
        'options'=>['placeholder'=>'Select a date...'],
        'pluginOptions'=>
        [
            'format'=>'yyyy-mm-dd H:i:s',
            'todayHighlight'=>true,
            'autoclose'=>true,
        ]
    ]);
    ?>

    <?= $form->field($model, 'effective_till')->widget(DatetimePicker::classname(),
    [
        'name'=>'effective_till',
        'value'=> date('yyyy-mm-dd H:i:s'),
        'options'=>['placeholder'=>'Select a date...'],
        'pluginOptions'=>
        [
            'format'=>'yyyy-mm-dd H:i:s',
            'todayHighlight'=>true,
            'autoclose'=>true,
        ]
    ]);
    ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
    [
        'pluginOptions'=>[
            'onText'=>'Active',
            'offText'=>'Inactive',
        ],
    ]);
    ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
