<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TransSo */
?>
<div class="trans-so-update">

    <?= $this->render('_form', [
        'model' => $model, 'model2'=> $model2,
        'modelDetails'=> $modelDetails,
    ]) ?>

</div>
