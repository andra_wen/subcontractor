<?php

use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\web\JsExpression;
use kartik\widgets\DepDrop;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\TransSo */

$js = '

jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {

    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {

        jQuery(this).html("Address: " + (index + 1))

    });

});


jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {

    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {

        jQuery(this).html("Address: " + (index + 1))

    });

});

';


$this->registerJs($js);

?>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">SO Information</a></li>
        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">SO Details</a></li>
        <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Confirmation Details</a></li>
    </ul>
</div>
<div class="tab-content">
    <div class="tab-pane active" id="tab_1">
        <div class="trans-so-view">

            <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'customer.customer_name',
                'custUser.username',
                'so_no',
                'paymentmethod.payment_name',
                'custBankAccount.bank_accountname',
                [
                    'label'=>'shipping_totalcost',
                    'format'=>'html',
                    'attribute'=> function($model)
                    {
                        $currency = "IDR. ";
                        return $currency.number_format($model->shipping_totalcost,'0',',','.');
                    }
                ],
                [
                    'label'=>'product_totalfee',
                    'format'=>'html',
                    'attribute'=> function($model)
                    {
                        $currency = "IDR. ";
                        return $currency.number_format($model->product_totalfee,'0',',','.');
                    }
                ],
                [
                    'label'=>'so_total',
                    'format'=>'html',
                    'attribute'=> function($model)
                    {
                        $currency = "IDR. ";
                        return $currency.number_format($model->so_total,'0',',','.');
                    }
                ],
                'transStatusCode.trans_code',
                'effective_from',
                'effective_till',
                'createdby0.username',
                'createdon',
                'lastmodifby0.username',
                'lastmodif',
                [
                    'label'=>'status',
                    'format'=>'html',
                    'attribute'=>function($model)
                    {
                        if($model->status == 10)
                        {
                            return '<span class="label label-primary">'.'Active'.'</span>';
                        }
                        else
                        {
                            return "<span class='label label-danger'>"."Inactive"."</span>";
                        }
                    }
                ],
            ],
        ]) ?>

        </div>
    </div>

    <div class="tab-pane" id="tab_2">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>SO Details</th>
                <th>SO Picture</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($modelDetails as $indexDetails => $modelDetail): ?>
            <tr>
                <td>
                    <?= yii\widgets\DetailView::widget([
                    'model' => $modelDetail,
                    'attributes' => [
                        'product_id',
                        'product_name',
                        'product_varians_id',
                        'product_varians_name',
                        'product_price',
                        'product_quantity',
                        'provider_id',
                        'provider_name',
                        'shippingcost_id',
                        'shippingcost',
                        'user_address_id',
                        'address_name',
                        'address_line1',
                        'address_line2',
                        'address_country',
                        'address_province',
                        'address_countytown',
                        'address_districts',
                        'address_postalzip',
                        'cart_remarks',
                        'details_total',
                        'details_statuscode',
                        'status',
                    ],
                    'options'=>['class' => 'table table-bordered']
                ]) ?>
                </td>
                <?php
                $modelsPic = \common\models\TransSoPictures::find()->where(['id_so_details'=>$modelDetail->id])->all();
                ?>
                <td>
                    <table>
                        <tbody>
                            <?php foreach ($modelsPic as $indexPic => $modelPic): ?>
                        <tr>
                            <td>
                                <?= yii\widgets\DetailView::widget([
                                'model' => $modelPic,
                                'attributes' => [
                                    'picture_name',
                                    [
                                        'attribute'=>'picture_path',
                                        'value'=>"@web/uploads/custome/png/".$modelPic->picture_path.".png",
                                        'format' => ['image',['width'=>'100','height'=>'100']],
                                    ],
                                ],
                                'options'=>['class' => 'table table-bordered']
                            ]) ?>
                            </td>
                        </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </td>
            </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <div class="tab-pane" id="tab_3">
        <?= DetailView::widget([
        'model' => $model2,
        'attributes' => [
            'so_no',
            'paymentmethod_name',
            'account_no',
            'account_holder',
            'phone_number',
            'total_amount',
            'payment_date',
            'confirm_remarks',
            'confirm_attachment',
            ],
        ]) ?>
    </div>
</div>

