<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use vova07\select2\Widget;
use kartik\widgets\SwitchInput;
use kartik\widgets\DateTimePicker;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use lajax\translatemanager\models\Language;
use yii\web\JsExpression;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model common\models\TransSo */
/* @var $form yii\widgets\ActiveForm */

$js = 'jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {

    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {

        jQuery(this).html("Address: " + (index + 1))

    });

});


jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {

    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {

        jQuery(this).html("Address: " + (index + 1))

    });

});';


$this->registerJs($js);

?>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">SO Information</a></li>
        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">SO Details</a></li>
        <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Confirmation Details</a></li>
    </ul>

    <?php $form = ActiveForm::begin(['id'=>'dynamic-form']); ?>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <div class="trans-so-form">

                <?php $dataCustomer = ArrayHelper::map(\common\models\MasterCustomer::find()->where(['status'=>10])->all(), 'id', 'customer_name'); ?>
                <?=
                $form->field($model, 'customer_id')->widget(Widget::classname(),[
                    'items'=> $dataCustomer,
                    'options'=>[
                        'placeholder'=> 'Select Customer...'
                    ],
                    'events' => [
                        'select2-open' => 'function (e) { log("select2:open", e); }',
                        'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
                    ],
                    'settings' => [
                        'width' => '100%',
                    ],
                ]);
                ?>

                <?php $dataUser = ArrayHelper::map(\common\models\CustUsers::find()->where(['status'=>10])->all(), 'id', 'username'); ?>
                <?=
                $form->field($model, 'cust_user_id')->widget(Widget::classname(),[
                    'items'=> $dataUser,
                    'options'=>[
                        'placeholder'=> 'Select Customer User...'
                    ],
                    'events' => [
                        'select2-open' => 'function (e) { log("select2:open", e); }',
                        'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
                    ],
                    'settings' => [
                        'width' => '100%',
                    ],
                ]);
                ?>

                <?= $form->field($model, 'so_no')->textInput(['maxlength' => true]) ?>

                <?php
                $dataPayment = \common\models\TransPaymentmethod::find()->orderBy('payment_name')->all();
                $listDataPayment = \yii\helpers\ArrayHelper::map($dataPayment, 'id', 'payment_name');
                ?>
                <?= $form->field($model, 'paymentmethod_id')->dropDownList($listDataPayment, [
                'prompt' => Yii::t('language', 'Select Payment method...'),
                'id' => 'paymentmethod-id'
            ]); ?>

                <?php
                $dataBank = \common\models\CustBankAccounts::find()->where(['status'=>10])->orderBy('bank_accountname')->all();
                $listDataBank = \yii\helpers\ArrayHelper::map($dataBank, 'id', 'bank_accountname');
                ?>
                <?= $form->field($model, 'cust_bank_account_id')->widget(DepDrop::classname(), [
                'options' => ['id' => 'custbank-id'],
                'data' => $listDataBank,
                'pluginOptions' => [
                    'depends' => ['paymentmethod-id'],
                    'placeholder' => Yii::t('language', 'Select Bank Account...'),
                    'url' => Url::to(['/trans-payment-method-bank/paymentmethodbanklist'])
                ]
            ]); ?>

                <?= $form->field($model, 'shipping_totalcost')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'product_totalfee')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'so_total')->textInput() ?>

                <?php $dataTransStatus = ArrayHelper::map(\common\models\TransStatuscode::find()->where(['status'=>10])->all(), 'id', 'trans_name'); ?>
                <?=
                $form->field($model, 'so_statuscode')->widget(Widget::classname(),[
                    'items'=> $dataTransStatus,
                    'options'=>[
                        'placeholder'=> 'Select Trans Status code...'
                    ],
                    'events' => [
                        'select2-open' => 'function (e) { log("select2:open", e); }',
                        'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
                    ],
                    'settings' => [
                        'width' => '100%',
                    ],
                ]);
                ?>

                <?= $form->field($model, 'effective_from')->widget(DatePicker::classname(),
                [
                    'name'=>'effective_from',
                    'value'=> date('yyyy-mm-dd'),
                    'options'=>['placeholder'=>'Select a date...'],
                    'pluginOptions'=>
                    [
                        'format'=>'yyyy-mm-dd',
                        'todayHighlight'=>true,
                        'autoclose'=>true,
                    ]
                ]);
                ?>

                <?= $form->field($model, 'effective_till')->widget(DatePicker::classname(),
                [
                    'name'=>'effective_till',
                    'value'=> date('yyyy-mm-dd'),
                    'options'=>['placeholder'=>'Select a date...'],
                    'pluginOptions'=>
                    [
                        'format'=>'yyyy-mm-dd',
                        'todayHighlight'=>true,
                        'autoclose'=>true,
                    ]
                ]);
                ?>

                <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
                [
                    'pluginOptions'=>[
                        'onText'=>'Active',
                        'offText'=>'Inactive',
                    ],
                ]);
                ?>
            </div>
        </div>
        <!-- /.tab-panel -->

        <div class="tab-pane" id="tab_2">
            <div class="cust-address-form">
                <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 0, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelDetails[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'product_id',
                    'product_name',
                    'product_varians_id',
                    'product_varians_name',
                    'product_price',
                    'product_quantity',
                    'provider_id',
                    'provider_name',
                    'shippingcost_id',
                    'shippingcost',
                    'user_address_id',
                    'address_name',
                    'address_line1',
                    'address_line2',
                    'address_country',
                    'address_province',
                    'address_countytown',
                    'address_districts',
                    'address_postalzip',
                    'cart_remarks',
                    'details_total',
                    'details_statuscode',
                    'status',
                ],
            ]); ?>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <i class="glyphicon glyphicon-envelope"></i>SO Details
                            <button type="button" class="add-item btn btn-success btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> Add</button>
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="container-items"><!-- widgetBody -->
                            <?php foreach ($modelDetails as $i => $modelsDetails): ?>
                                <div class="item panel panel-default"><!-- widgetItem -->
                                    <div class="panel-heading">
                                        <h3 class="panel-title pull-left">Product Details</h3>
                                        <div class="pull-right">
                                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                        // necessary for update action.
                                        if (!$modelsDetails->isNewRecord) {
                                            echo Html::activeHiddenInput($modelsDetails, "[{$i}]id");
                                        }
                                        ?>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <?php
                                                $dataProduct = \common\models\MasterProducts::find()->where(['status'=>10])->orderBy('product_name')->all();
                                                $listDataProduct = \yii\helpers\ArrayHelper::map($dataProduct, 'id', 'product_name');
                                                ?>
                                                <?= $form->field($modelsDetails, "[{$i}]product_id")->dropDownList($listDataProduct,
                                                [
                                                    'prompt' => Yii::t('language', 'Select Product...'),
                                                    'id' => "sodetail-$i-product_id",
                                                ]); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $form->field($modelsDetails, "[{$i}]product_name")->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div><!-- .row -->
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <?php
                                                $dataVarian = \common\models\MasterProductVarians::find()->orderBy('varian_name')->all();
                                                $listDataVarian = \yii\helpers\ArrayHelper::map($dataVarian, 'id', 'varian_name');
                                                ?>
                                                <?= $form->field($modelsDetails, "[{$i}]product_varians_id")->widget(DepDrop::classname(), [
                                                'options' => ['id' => "sodetail-$i-product_varian_id"],
                                                'data'=>$listDataVarian,
                                                'pluginOptions' => [
                                                    'depends' => ["sodetail-$i-product_id"],
                                                    'placeholder' => Yii::t('language', 'Select Product Varian...'),
                                                    'url' => Url::to(['/master-product-varians/varianlist'])
                                                ]
                                            ]); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $form->field($modelsDetails, "[{$i}]product_varians_name")->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?= $form->field($modelsDetails, "[{$i}]product_price")->textInput() ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $form->field($modelsDetails, "[{$i}]product_quantity")->textInput() ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?php
                                                $dataProvider = \common\models\TransShippingprovider::find()->orderBy('shipping_name')->all();
                                                $listDataProvider = \yii\helpers\ArrayHelper::map($dataProvider, 'id', 'shipping_name');
                                                ?>
                                                <?= $form->field($modelsDetails, "[{$i}]provider_id")->dropDownList($listDataProvider,
                                                [
                                                    'prompt' => Yii::t('language', 'Select Provider...'),
                                                    'id' => "sodetail-$i-provider_id",
                                                ]); ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $form->field($modelsDetails, "[{$i}]provider_name")->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?php
                                                $dataShipping = \common\models\TransShippingcost::find()->where(['status'=>10])->orderBy('id')->all();
                                                $listDataShipping = \yii\helpers\ArrayHelper::map($dataShipping, 'id', 'id');
                                                ?>
                                                <?= $form->field($modelsDetails, "[{$i}]shippingcost_id")->dropDownList($listDataShipping,
                                                [
                                                    'prompt' => Yii::t('language', 'Select Shipping Cost...'),
                                                    'id' => "sodetail-$i-shipping_id",
                                                ]); ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $form->field($modelsDetails, "[{$i}]shippingcost")->textInput() ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?php
                                                $dataAddress = \common\models\CustUsersAddress::find()->orderBy('address_name')->all();
                                                $listDataAddress = \yii\helpers\ArrayHelper::map($dataAddress, 'id', 'address_name');
                                                ?>
                                                <?= $form->field($modelsDetails, "[{$i}]user_address_id")->dropDownList($listDataAddress,
                                                [
                                                    'prompt' => Yii::t('language', 'Select Address...'),
                                                    'id' => "sodetail-$i-address_id",
                                                ]); ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $form->field($modelsDetails, "[{$i}]address_name")->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?= $form->field($modelsDetails, "[{$i}]address_line1")->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $form->field($modelsDetails, "[{$i}]address_line2")->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                        </div>
                    </div>
                </div><!-- .panel -->

                <?php DynamicFormWidget::end(); ?>
            </div>
        </div>
        <!-- /.tab-panel -->


        <div class="tab-pane" id="tab_3">
            <?= $form->field($model2, 'so_no')->textInput(['maxlength' => true]) ?>

            <?php $dataPaymentMethod = ArrayHelper::map(\common\models\TransPaymentmethod::find()->all(), 'id', 'payment_name'); ?>
            <?=
            $form->field($model2, 'paymentmethod_id')->widget(Widget::classname(),[
                'items'=> $dataPaymentMethod,
                'options'=>[
                    'placeholder'=> 'Select Payment Method...'
                ],
                'events' => [
                    'select2-open' => 'function (e) { log("select2:open", e); }',
                    'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
                ],
                'settings' => [
                    'width' => '100%',
                ],
            ]);
            ?>

            <?= $form->field($model2, 'paymentmethod_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model2, 'account_no')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model2, 'account_holder')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model2, 'phone_number')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model2, 'total_amount')->textInput() ?>

            <?= $form->field($model2, 'payment_date')->widget(DatePicker::classname(),
            [
                'name'=>'effective_till',
                'value'=> date('yyyy-mm-dd'),
                'options'=>['placeholder'=>'Select a date...'],
                'pluginOptions'=>
                [
                    'format'=>'yyyy-mm-dd',
                    'todayHighlight'=>true,
                    'autoclose'=>true,
                ]
            ]);
            ?>

            <?= $form->field($model2, 'confirm_remarks')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model2, 'confirm_attachment')->hiddenInput()->label(false) ?>

            <?php  $form->field($model2, 'status')->widget(SwitchInput::classname(),
            [
                'pluginOptions'=>[
                    'onText'=>'Active',
                    'offText'=>'Inactive',
                ],
            ]);
            ?>
        </div>
        <!-- /.tab-panel -->

        <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        <?php } ?>
    </div>
    <!-- /.tab-content -->
    <?php ActiveForm::end(); ?>
</div>
