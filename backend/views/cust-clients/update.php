<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CustClients */
?>
<div class="cust-clients-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
