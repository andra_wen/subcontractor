<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CustClients */
?>
<div class="cust-clients-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer_id',
            'username',
            'auth_key',
            'password_hash',
            'password_reset_token',
            'email:email',
            'effective_from',
            'effective_till',
            'createdon',
            'status',
        ],
    ]) ?>

</div>
