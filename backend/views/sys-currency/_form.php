<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SysCurrency */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sys-currency-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iso_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'convert_nomimal')->textInput() ?>

    <?= $form->field($model, 'convert_rate')->textInput() ?>

    <?= $form->field($model, 'intl_formatting')->textInput() ?>

    <?= $form->field($model, 'min_fraction_digits')->textInput() ?>

    <?= $form->field($model, 'max_fraction_digits')->textInput() ?>

    <?= $form->field($model, 'dec_point')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'thousands_sep')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'format_string')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'createdby')->textInput() ?>

    <?= $form->field($model, 'createdon')->textInput() ?>

    <?= $form->field($model, 'lastmodifby')->textInput() ?>

    <?= $form->field($model, 'lastmodif')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
