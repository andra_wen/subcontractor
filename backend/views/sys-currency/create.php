<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SysCurrency */

?>
<div class="sys-currency-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
