<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterCustomerProfile */
?>
<div class="master-customer-profile-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer_id',
            'customer_name',
            'customer_desc',
            'customer_country',
            'customer_contactperson',
            'customer_contactno',
            'createdby',
            'createdon',
            'lastmodifby',
            'lastmodif',
            'status',
        ],
    ]) ?>

</div>
