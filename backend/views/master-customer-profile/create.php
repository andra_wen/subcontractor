<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterCustomerProfile */

?>
<div class="master-customer-profile-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
