<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CustBankAccounts */
?>
<div class="cust-bank-accounts-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
