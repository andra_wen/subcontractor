<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use vova07\select2\Widget;
use yii\web\JsExpression;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\CustBankAccounts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cust-bank-accounts-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $dataBanks = ArrayHelper::map(\common\models\CustBanks::find()->where(['status'=>10])->all(), 'id', 'bank_shortname'); ?>
    <?=
    $form->field($model, 'bank_id')->widget(Widget::classname(),[
        'items'=> $dataBanks,
        'options'=>[
            'placeholder'=> 'Select Bank...'
        ],
        'events' => [
            'select2-open' => 'function (e) { log("select2:open", e); }',
            'select2-close' => new JsExpression('function (e) { log("select2:close", e); }')
        ],
        'settings' => [
            'width' => '100%',
        ],
    ]);
    ?>

    <?= $form->field($model, 'bank_accountname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank_accountno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank_info')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank_remarks')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isactive')->widget(SwitchInput::classname(),
    [
        'pluginOptions'=>[
            'onText'=>'Active',
            'offText'=>'Inactive',
        ],
    ]);
    ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(),
    [
        'pluginOptions'=>[
            'onText'=>'Active',
            'offText'=>'Inactive',
        ],
    ]);
    ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('language', 'Create') : Yii::t('language', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
