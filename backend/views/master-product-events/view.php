<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterProductEvents */
?>
<div class="master-product-events-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'product.product_name',
            'event_name',
            'event_caption',
            'event_label',
            'event_stringvalue',
            'event_numbervalue',
            'effective_from',
            'effective_till',
            [
                'label'=>'isactive',
                'format'=>'html',
                'attribute'=>function($model)
                {
                    if($model->isactive == 10)
                    {
                        return '<span class="label label-primary">'.'Active'.'</span>';
                    }
                    else
                    {
                        return "<span class='label label-danger'>"."Inactive"."</span>";
                    }
                }
            ],
            'createdby0.username',
            'createdon',
            'lastmodifby0.username',
            'lastmodif',
            [
                'label'=>'status',
                'format'=>'html',
                'attribute'=>function($model)
                {
                    if($model->status == 10)
                    {
                        return '<span class="label label-primary">'.'Active'.'</span>';
                    }
                    else
                    {
                        return "<span class='label label-danger'>"."Inactive"."</span>";
                    }
                }
            ],
        ],
    ]) ?>

</div>
