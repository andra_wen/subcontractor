<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterProductEvents */
?>
<div class="master-product-events-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
